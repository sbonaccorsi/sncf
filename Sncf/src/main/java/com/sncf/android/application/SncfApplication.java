package com.sncf.android.application;

import android.app.Application;

import com.sncf.android.R;
import com.sncf.android.utils.UtilApplication;

/**
 * Created by zzzwist on 11/01/14.
 */
public class SncfApplication extends Application {

    public static boolean               IS_UI_TABLET;
    public static boolean               IS_DEBUG_MODE;

    @Override
    public void onCreate() {
        super.onCreate();
        IS_UI_TABLET = getResources().getBoolean(R.bool.is_UI_tablet);
        IS_DEBUG_MODE = UtilApplication.isDebugMode(getBaseContext());
    }
}
