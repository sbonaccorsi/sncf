package com.sncf.android.fragment;

import android.app.DatePickerDialog;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.widget.SimpleCursorAdapter;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.sncf.android.R;
import com.sncf.android.data.CRIEdition;
import com.sncf.android.entity.CompoEntity;
import com.sncf.android.entity.InterventionEntity;
import com.sncf.android.entity.LinkSeriesEntity;
import com.sncf.android.entity.LinkSpecialtyEntity;
import com.sncf.android.entity.RameEntity;
import com.sncf.android.entity.STFEntity;
import com.sncf.android.entity.SeriesEntity;
import com.sncf.android.entity.SiteEntity;
import com.sncf.android.entity.SpecialtyEntity;
import com.sncf.android.entity.SubSeriesEntity;
import com.sncf.android.entity.UOEntity;
import com.sncf.android.orm.Entity;
import com.sncf.android.orm.criteria.ICriteria;
import com.sncf.android.orm.criteria.SQLCriteria;
import com.sncf.android.orm.criteria.SQLPredicate;
import com.sncf.android.orm.criteria.aggregators.AndAggregator;
import com.sncf.android.orm.criteria.operators.EqualSqlPredicate;
import com.sncf.android.orm.dao.LinkSpecialtyDAO;
import com.sncf.android.orm.dao.SeriesDAO;
import com.sncf.android.orm.dao.SiteDAO;
import com.sncf.android.orm.dao.SpecialtyDAO;
import com.sncf.android.orm.dao.UODAO;
import com.sncf.android.provider.SncfContent;
import com.sncf.android.utils.EntityCreator;
import com.sncf.android.utils.LogSncf;

import java.util.Calendar;
import java.util.Collection;

/**
 * Created by zzzwist on 18/01/14.
 */
public class FragmentAddCRIIdent extends FragmentAddCRISpinnerManagment implements View.OnClickListener, AdapterView.OnItemSelectedListener, TextWatcher {

    public static final String                      SAVE_INST_INTERVENANT = "saveinstintervenant";

    private Spinner                                 mSpinnerSTF;
    private Spinner                                 mSpinnerUO;
    private Spinner                                 mSpinnerSite;
    private Spinner                                 mSpinnerSpeciality;
    private Spinner                                 mSpinnerSeries;
    private Spinner                                 mSpinnerSubSeries;
    private Spinner                                 mSpinnerRames;
    private Spinner                                 mSpinnerCaisses;
    private Spinner                                 mSpinnerIntervention;
    private Spinner                                 mSpinnerUM;
    private EditText                                mEditTextSpeciality;
    private EditText                                mEditTextDateSign;
    private EditText                                mEditTextDateInt;
    private EditText                                mEditTextIntervenant;

    private CRIEdition                              mEditionData;

    @Override
    protected int getContentLayout() {
        return R.layout.fragment_add_cri_identification;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.restoreInstanceState(savedInstanceState);

        mEditionData                = CRIEdition.getInstance();

        mSpinnerSTF                 = (Spinner) view.findViewById(R.id.spinner_select_stf);
        mSpinnerUO                  = (Spinner) view.findViewById(R.id.spinner_select_technicenter);
        mSpinnerSite                = (Spinner) view.findViewById(R.id.spinner_select_sites);
        mSpinnerSpeciality          = (Spinner) view.findViewById(R.id.spinner_select_speciality);
        mSpinnerSeries              = (Spinner) view.findViewById(R.id.spinner_select_series);
        mSpinnerRames               = (Spinner) view.findViewById(R.id.spinner_select_rame);
        mSpinnerCaisses             = (Spinner) view.findViewById(R.id.spinner_select_caisse);
        mSpinnerIntervention        = (Spinner) view.findViewById(R.id.spinner_select_interventions);
        mSpinnerUM                  = (Spinner) view.findViewById(R.id.spinner_select_um);
        mEditTextSpeciality         = (EditText) view.findViewById(R.id.editText_show_speciality);
        mEditTextDateSign           = (EditText) view.findViewById(R.id.editText_add_cri_signalement);
        mEditTextDateInt            = (EditText) view.findViewById(R.id.editText_add_cri_intervention);
        mEditTextIntervenant        = (EditText) view.findViewById(R.id.editText_select_intervenant);
        mSpinnerSubSeries           = (Spinner) view.findViewById(R.id.spinner_select_subseries);

        this.initializeSpinner(mSpinnerSTF, SncfContent.t_sncf_stf.CONTENT_URI, null, null, null, SncfContent.t_sncf_stf.Columns.STF.getName());
        this.initializeSpinner(mSpinnerSeries, SncfContent.t_sncf_series.CONTENT_URI, SncfContent.t_sncf_series.Columns.ID_SERIE.getName() + "=?",
                new String[]{Integer.toString(1)}, null, SncfContent.t_sncf_series.Columns.SERIE.getName());
        this.initializeSpinner(mSpinnerIntervention, SncfContent.t_gr_interventions.CONTENT_URI, null, null, null,
                SncfContent.t_gr_interventions.Columns.INTERVENTION.getName());

        ((TextView) view.findViewById(R.id.textView_title_sub_header))
                .setText(getString(R.string.title_header_add_cri_identification));

        view.findViewById(R.id.imageView_calendar_signa).setOnClickListener(this);
        view.findViewById(R.id.imageView_calendar_inter).setOnClickListener(this);
        mSpinnerSTF.setOnItemSelectedListener(this);
        mSpinnerUO.setOnItemSelectedListener(this);
        mSpinnerSite.setOnItemSelectedListener(this);
        mSpinnerSpeciality.setOnItemSelectedListener(this);
        mSpinnerSeries.setOnItemSelectedListener(this);
        mSpinnerRames.setOnItemSelectedListener(this);
        mSpinnerCaisses.setOnItemSelectedListener(this);
        mSpinnerIntervention.setOnItemSelectedListener(this);
        mSpinnerUM.setOnItemSelectedListener(this);
        mSpinnerSubSeries.setOnItemSelectedListener(this);
        ((EditText)view.findViewById(R.id.editText_select_intervenant)).addTextChangedListener(this);

        this.initDateField((EditText)view.findViewById(R.id.editText_datepicker_signalement));
        this.initDateField((EditText)view.findViewById(R.id.editText_datepicker_inter));
    }

    private void restoreInstanceState(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            String intervenant = savedInstanceState.getString(SAVE_INST_INTERVENANT);
            ((EditText)getView().findViewById(R.id.editText_select_intervenant)).setText(intervenant);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        String intervenant = ((EditText)getView().findViewById(R.id.editText_select_intervenant)).getText().toString();
        outState.putString(SAVE_INST_INTERVENANT, intervenant);
    }

    private void initDateField(EditText editText) {
        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int year = calendar.get(Calendar.YEAR);
        editText.setText(String.format("%ta %d %tB %d", calendar, day, calendar, year));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LogSncf.e("FragmentAddCRIIdent", "onDestroy");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imageView_calendar_signa:
                this.selectDate((EditText)getView().findViewById(R.id.editText_datepicker_signalement));
                break;
            case R.id.imageView_calendar_inter:
                this.selectDate((EditText)getView().findViewById(R.id.editText_datepicker_inter));
                break;
        }
    }

    private void selectDate(EditText editText) {
        Calendar calendar = Calendar.getInstance();
        DatePickerDialog dialog = new DatePickerDialog(getActivity(), new SelectManager(editText), calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        dialog.show();
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {

        SimpleCursorAdapter adapter                     = (SimpleCursorAdapter) adapterView.getAdapter();
        Cursor cursor                                   = adapter.getCursor();
        if (adapterView == mSpinnerSTF)                 this.manageSelectionSpinnerSTF(cursor, position);
        else if (adapterView == mSpinnerUO)             this.manageSelectionSpinnerUO(cursor, position);
        else if (adapterView == mSpinnerSite)           this.manageSelectionSpinnerSite(cursor, position);
        else if (adapterView == mSpinnerSpeciality)     this.manageSelectionSpinnerSpeciality(cursor, position);
        else if (adapterView == mSpinnerSeries)         this.manageSelectionSpinnerSerie(cursor, position);
        else if (adapterView == mSpinnerRames)          this.manageSelectionSpinnerRames(cursor, position);
        else if (adapterView == mSpinnerCaisses)        this.manageSelectionSpinnerCaisses(cursor, position);
        else if (adapterView == mSpinnerIntervention)   this.manageSelectionSpinnerIntervention(cursor, position);
        else if (adapterView == mSpinnerUM)             this.manageSelectionSpinnerUM(cursor, position);
        else if (adapterView == mSpinnerSubSeries)      this.manageSelectionSpinnerSubSeries(cursor, position);
    }

    private void manageSelectionSpinnerSTF(Cursor cursor, int position) {

        if (position == 0) {
            mEditionData.setId_stf(null);

            mSpinnerUO.setSelection(0);
            mSpinnerRames.setSelection(0);
            mSpinnerUM.setSelection(0);

            this.manageSelectionSpinnerUO(null, 0);
            mSpinnerUO.setAdapter(null);

            this.manageSelectionSpinnerRames(null, 0);
            mSpinnerRames.setAdapter(null);

            this.manageSelectionSpinnerUM(null, 0);
            mSpinnerUM.setAdapter(null);

            return;
        }
        if (cursor.moveToPosition(position - 1)) {

            STFEntity stfEntity                     = new STFEntity(cursor);
            mEditionData.setId_stf(stfEntity.getPrimaryKeyValue());

            this.initializeSpinner(mSpinnerUO, SncfContent.t_sncf_uo.CONTENT_URI, SncfContent.t_sncf_uo.Columns.ID_STF.getName() + "=?",
                    new String[]{Integer.toString(stfEntity.idSTF)}, null, SncfContent.t_sncf_uo.Columns.UO_LONG.getName());

            this.initializeSpinner(mSpinnerUM, SncfContent.t_sncf_rames.CONTENT_URI, SncfContent.t_sncf_rames.Columns.ID_SERIE.getName() + "=? AND "
                    + SncfContent.t_sncf_rames.Columns.ID_RAME.getName() + ">? AND " + SncfContent.t_sncf_rames.Columns.ID_STF.getName() + "=?",
                    new String[]{Integer.toString(1), Integer.toString(0), Integer.toString(stfEntity.idSTF)}, null, SncfContent.t_sncf_rames.Columns.EAB.getName());
        }
    }

    private void manageSelectionSpinnerUO(Cursor cursor, int position) {
        if (position == 0) {
            mEditionData.setId_uo(null);

            mSpinnerSite.setSelection(0);
            this.manageSelectionSpinnerSite(null, 0);
            mSpinnerSite.setAdapter(null);

            return;
        }
        if (cursor.moveToPosition(position - 1)) {
            UOEntity uoEntity                       = new UOEntity(cursor);
            mEditionData.setId_uo(uoEntity.getPrimaryKeyValue());

            this.initializeSpinner(mSpinnerSite, SncfContent.t_sncf_sites.CONTENT_URI, SncfContent.t_sncf_sites.Columns.ID_UO.getName() + "=?",
                    new String[]{Integer.toString(uoEntity.idUO)}, null, SncfContent.t_sncf_sites.Columns.SITE.getName());
        }
    }

    private void manageSelectionSpinnerSite(Cursor cursor, int position) {
        if (position == 0) {
            mEditionData.setId_site(null);

            mSpinnerSpeciality.setSelection(0);
            this.manageSelectionSpinnerSpeciality(null, 0);
            mSpinnerSpeciality.setAdapter(null);

            return;
        }

        if (cursor.moveToPosition(position - 1)) {
            SiteEntity siteEntity                   = new SiteEntity(cursor);
            mEditionData.setId_site(siteEntity.getPrimaryKeyValue());

            if (!siteEntity.isSpecialize) {
                mSpinnerSpeciality.setEnabled(false);
                mSpinnerSpeciality.setAdapter(null);
                mEditionData.setId_speciality(null);
            }
            else {
                this.initializeSpinner(mSpinnerSpeciality, SncfContent.t_gr_specialities.CONTENT_URI,
                        null, null, null, SncfContent.t_gr_specialities.Columns.SPECIALITY.getName());
                mSpinnerSpeciality.setEnabled(true);
            }
        }
    }

    private void manageSelectionSpinnerSpeciality(Cursor cursor, int position) {
        if (position == 0) {
            mEditionData.setId_speciality(null);
            mEditionData.setName_speciality(null);
            return;
        }

        if (cursor.moveToPosition(position - 1)) {
            SpecialtyEntity specialtyEntity         = new SpecialtyEntity(cursor);
            mEditionData.setId_speciality(specialtyEntity.getPrimaryKeyValue());

            UOEntity uoEntity       = EntityCreator.buildEntity(new UODAO(getActivity()),
                    new SQLPredicate(SncfContent.t_sncf_uo.Columns.ID.getName(), new EqualSqlPredicate(mEditionData.getId_uo())));

            SiteEntity siteEntity   = EntityCreator.buildEntity(new SiteDAO(getActivity()),
                    new SQLPredicate(SncfContent.t_sncf_sites.Columns.ID.getName(), new EqualSqlPredicate(mEditionData.getId_site())));

            ICriteria criteria                      = new SQLCriteria();
            LinkSpecialtyDAO linkSpecialtyDAO       = new LinkSpecialtyDAO(getActivity());
            AndAggregator andAggregator             = new AndAggregator(
                    new SQLPredicate(SncfContent.t_sncf_link_specialities.Columns.ID_UO.getName(), new EqualSqlPredicate(uoEntity.idUO)),
                    new SQLPredicate(SncfContent.t_sncf_link_specialities.Columns.ID_SPECIALITY.getName(), new EqualSqlPredicate(specialtyEntity.idSpeciality)),
                    new SQLPredicate(SncfContent.t_sncf_link_specialities.Columns.ID_SITE.getName(), new EqualSqlPredicate(siteEntity.idSite)));
            criteria.add(andAggregator);
            Collection<LinkSpecialtyEntity> collection = linkSpecialtyDAO.read(criteria);
            if (collection == null)                 return;
            if (collection.size() == 0)             return;

            LinkSpecialtyEntity linkSpecialtyEntity = (LinkSpecialtyEntity) collection.toArray()[0];
            mEditTextSpeciality.setText(linkSpecialtyEntity.griffeSpecialty);
            mEditionData.setName_speciality(linkSpecialtyEntity.griffeSpecialty);
        }
    }

    private void manageSelectionSpinnerSerie(Cursor cursor, int position) {
        if (position == 0) {
            mEditionData.setId_series(null);
            mSpinnerSubSeries.setSelection(0);
            this.manageSelectionSpinnerSubSeries(null, 0);
            mSpinnerSubSeries.setAdapter(null);
            return;
        }
        if (cursor.moveToPosition(position - 1)) {

            SeriesEntity seriesEntity               = new SeriesEntity(cursor);
            mEditionData.setId_series(seriesEntity.getPrimaryKeyValue());
            this.initializeSpinner(mSpinnerSubSeries, SncfContent.t_sncf_subseries.CONTENT_URI, SncfContent.t_sncf_subseries.Columns.ID_SERIE.getName() + "=?",
                    new String[]{Integer.toString(seriesEntity.idSeries)}, null, SncfContent.t_sncf_subseries.Columns.SUBSERIE.getName());
        }
    }

    private void manageSelectionSpinnerSubSeries(Cursor cursor, int position) {
        if (position == 0) {
            mEditionData.setId_subserie(null);
            mSpinnerRames.setSelection(0);
            this.manageSelectionSpinnerRames(null, 0);
            mSpinnerRames.setAdapter(null);
            return;
        }
        if (cursor.moveToPosition(position)) {

            SubSeriesEntity subSeriesEntity         = new SubSeriesEntity(cursor);
            mEditionData.setId_subserie(subSeriesEntity.getPrimaryKeyValue());

            this.initializeSpinner(mSpinnerRames, SncfContent.t_sncf_rames.CONTENT_URI,
                    SncfContent.t_sncf_rames.Columns.ID_UNDER_SERIE.getName() + "=? AND " + SncfContent.t_sncf_rames.Columns.ID_SERIE.getName() + "=? AND "
                    + SncfContent.t_sncf_rames.Columns.ID_RAME.getName() + ">?",
                    new String[]{Integer.toString(subSeriesEntity.id_sub_serie), Integer.toString(subSeriesEntity.id_serie), Integer.toString(0)},
                    null, SncfContent.t_sncf_rames.Columns.EAB.getName());
        }
    }

    private void manageSelectionSpinnerRames(Cursor cursor, int position) {
        if (position == 0) {
            mEditionData.setId_rame(null);

            mSpinnerCaisses.setSelection(0);
            this.manageSelectionSpinnerCaisses(null, 0);
            mSpinnerCaisses.setAdapter(null);

            FragmentAddCRI fragmentAddCRI = (FragmentAddCRI) getActivity().getSupportFragmentManager().findFragmentByTag(FragmentAddCRI.FRAGMENT_TAG);
            fragmentAddCRI.updateFragmentArbo(null);
            return;
        }
        if (cursor.moveToPosition(position - 1)) {

            RameEntity rameEntity                   = new RameEntity(cursor);
            mEditionData.setId_rame(rameEntity.getPrimaryKeyValue());

            FragmentAddCRI fragmentAddCRI = (FragmentAddCRI) getActivity().getSupportFragmentManager().findFragmentByTag(FragmentAddCRI.FRAGMENT_TAG);
            fragmentAddCRI.updateFragmentArbo(rameEntity.csGriffe);

            this.initializeSpinner(mSpinnerCaisses, SncfContent.t_sncf_compos.CONTENT_URI, SncfContent.t_sncf_compos.Columns.ID_RAME.getName() + "=?",
                    new String[]{Integer.toString(rameEntity.idRame)}, SncfContent.t_sncf_compos.Columns.NUM_ORDER.getName() + " ASC",
                    SncfContent.t_sncf_compos.Columns.NUM_VEHICULE.getName());
        }
    }

    private void manageSelectionSpinnerCaisses(Cursor cursor, int position) {
        if (position == 0) {
            mEditionData.setId_caisse(null);
            return;
        }
        if (cursor.moveToPosition(position - 1)) {
            CompoEntity compoEntity                 = new CompoEntity(cursor);
            mEditionData.setId_caisse(compoEntity.getPrimaryKeyValue());
        }
    }

    private void manageSelectionSpinnerIntervention(Cursor cursor, int position) {
        if (position == 0) {
            mEditionData.setId_intervention(null);
            return;
        }
        if (cursor.moveToPosition(position - 1)) {
            InterventionEntity interventionEntity   = new InterventionEntity(cursor);
            mEditionData.setId_intervention(interventionEntity.getPrimaryKeyValue());
        }
    }

    private void manageSelectionSpinnerUM(Cursor cursor, int position) {
        if (position == 0) {
            mEditionData.setId_um(null);
            return;
        }
        if (cursor.moveToPosition(position - 1)) {
            RameEntity rameEntity                   = new RameEntity(cursor);
            mEditionData.setId_um(rameEntity.getPrimaryKeyValue());
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {}
    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        mEditionData.setName_intervenant(s.toString());
    }
    @Override
    public void afterTextChanged(Editable s) {}

    private class SelectManager implements DatePickerDialog.OnDateSetListener {

        private EditText        mEditText;

        public SelectManager(EditText editText) {
            mEditText = editText;
        }

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            Calendar calendar                           = Calendar.getInstance();
            calendar.set(year, monthOfYear, dayOfMonth);
            mEditText.setText(String.format("%ta %d %tB %d", calendar, dayOfMonth, calendar, year));
            if (mEditTextDateSign == mEditText)         mEditionData.setDate_signalment(mEditText.getText().toString());
            else if (mEditTextDateInt == mEditText)     mEditionData.setDate_intervention(mEditText.getText().toString());
        }
    }
}
