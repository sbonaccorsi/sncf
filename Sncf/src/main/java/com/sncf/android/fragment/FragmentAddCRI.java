package com.sncf.android.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.sncf.android.R;
import com.sncf.android.base.AbstractPagerFragment;
import com.sncf.android.data.CRIEdition;
import com.sncf.android.observer.Observable;
import com.sncf.android.observer.Observer;
import com.sncf.android.utils.LogSncf;

import java.lang.reflect.Type;

/**
 * Created by zzzwist on 18/01/14.
 */
public class FragmentAddCRI extends AbstractPagerFragment implements View.OnClickListener, Observer {

    public static final String          FRAGMENT_TAG = FragmentAddCRI.class.getName();

    private static final String         KEY_PARAM_SAVE_CURRENT_PAGE_INDEX = "savecurrentpageindex";
    public static final int             COUNT_ADAPTER = 4;

    private SparseArray<Fragment>       mArray;
    private int                         mCurrentPageIndex = 0;

    @Override
    protected int getContentLayout() {
        return R.layout.fragment_viewpager;
    }

    @Override
    protected int getIdViewPager() {
        return R.id.viewpager;
    }

    @Override
    protected int getOffscreenPageLimit() {
        return 5;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        CRIEdition criEdition = CRIEdition.getInstance();
        criEdition.addObserver(this);

        mPagerAdapter = new ViewPagerAdapter(getChildFragmentManager());
        mViewPager.setAdapter(mPagerAdapter);
        mArray = new SparseArray<Fragment>();
        view.findViewById(R.id.button_next_pager).setOnClickListener(this);
        view.findViewById(R.id.button_prev_pager).setOnClickListener(this);

        if (savedInstanceState != null)
            mCurrentPageIndex = savedInstanceState.getInt(KEY_PARAM_SAVE_CURRENT_PAGE_INDEX);

        if (mCurrentPageIndex == 0)
            getView().findViewById(R.id.button_prev_pager).setEnabled(false);
    }

    @Override
    public void update(Observable observable) {

        if (observable instanceof CRIEdition) {
            LogSncf.e("FragmentAddCRI", "Update data");
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(KEY_PARAM_SAVE_CURRENT_PAGE_INDEX, mCurrentPageIndex);
    }

    @Override
    public void onPageScrolled(int position, float v, int i2) {
    }

    @Override
    public void onPageSelected(int position) {
        mCurrentPageIndex = position;

        if (position == 0)
            getView().findViewById(R.id.button_prev_pager).setEnabled(false);
        else if (position == (COUNT_ADAPTER - 1))
            ((Button)getView().findViewById(R.id.button_next_pager)).setText(getString(R.string.button_done));
        else if (position > 0 && position < (COUNT_ADAPTER - 1)) {
            getView().findViewById(R.id.button_prev_pager).setEnabled(true);
            ((Button)getView().findViewById(R.id.button_next_pager)).setText(getString(R.string.button_next));
        }
    }

    @Override
    public void onPageScrollStateChanged(int position) {
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_next_pager:
                if (mCurrentPageIndex < (COUNT_ADAPTER - 1)) {
                    mCurrentPageIndex++;
                    mViewPager.setCurrentItem(mCurrentPageIndex, true);
                }
                break;
            case R.id.button_prev_pager:
                if (mCurrentPageIndex > 0) {
                    mCurrentPageIndex--;
                    mViewPager.setCurrentItem(mCurrentPageIndex, true);
                }
                break;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LogSncf.e("FragmentAddCRI", "onDestroy");
    }

    @Override
    protected Fragment adapterGetItem(int position) {
        Fragment fragment = null;
        if (mArray.get(position) != null)
            fragment = mArray.get(position);
        else {
            switch (position) {
                case 0:
                    fragment = Fragment.instantiate(getActivity(), FragmentAddCRIIdent.class.getName());
                    break;
                case 1:
                    fragment = Fragment.instantiate(getActivity(), FragmentAddCRIArbo.class.getName());
                    break;
                case 2:
                    fragment = Fragment.instantiate(getActivity(), FragmentAddCRIMicroDJ.class.getName());
                    break;
                case 3:
                    fragment = Fragment.instantiate(getActivity(), FragmentAddCRIDesc.class.getName());
                    break;
            }
            mArray.put(position, fragment);
        }
        return fragment;
    }

    @Override
    protected int adapterGetCount() {
        return COUNT_ADAPTER;
    }

    @Override
    protected void adapterDestroyItem(ViewGroup container, int position, Object object) {

    }

    public void updateFragmentArbo(String cs) {

        for (int idx = 0; idx < mArray.size(); ++idx) {
            Fragment fragment                               = mArray.valueAt(idx);
            if (fragment instanceof FragmentAddCRIArbo)     ((FragmentAddCRIArbo)fragment).updateState(cs);
        }
    }
}
