package com.sncf.android.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TextView;

import com.sncf.android.R;
import com.sncf.android.application.SncfApplication;
import com.sncf.android.base.BaseFragment;
import com.sncf.android.entity.CompoEntity;
import com.sncf.android.entity.SemelleBogieEntity;
import com.sncf.android.entity.SemelleDataEntity;
import com.sncf.android.utils.LogSncf;
import com.sncf.android.utils.UtilsUI;

/**
 * Created by favre on 07/02/2014.
 */
public class FragmentDriving extends BaseFragment
{
    protected    CompoEntity compo;

    @Override
    protected int getContentLayout() {
        return R.layout.fragment_driving;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        TextView text = (TextView) view.findViewById(R.id.textView_title_sub_header);
        text.setText("Motrice : " + this.compo.name);

        if (SncfApplication.IS_UI_TABLET)   this.initViewTablet();
        else                                this.initViewHandset();
    }

    public FragmentDriving(CompoEntity compo)
    {
        super();
        this.compo = compo;
    }

    private void initViewTablet() {
        int container_bogie = R.id.container_content_bogie_one;
        for (SemelleBogieEntity semelleBogieEntity : this.compo.bogies) {

            LinearLayout bogieView = (LinearLayout) LayoutInflater.from(getActivity()).inflate(R.layout.content_sole_bogie, null);
            TextView bogieNumber = (TextView) bogieView.findViewById(R.id.textView_sole_bogie_number);
            CheckBox bmIsole = (CheckBox) bogieView.findViewById(R.id.checkbox_sole_bmisole);
            CheckBox bogieIsole = (CheckBox) bogieView.findViewById(R.id.checkbox_sole_bogieisole);

            bogieNumber.setText(Integer.toString(semelleBogieEntity.bogieIdentifier));
            bmIsole.setChecked(semelleBogieEntity.bmSole);
            bogieIsole.setChecked(semelleBogieEntity.bogieIsole);

            LinearLayout measuresView = (LinearLayout) LayoutInflater.from(getActivity()).inflate(R.layout.content_sole_measures, null);
            for (SemelleDataEntity semelleDataEntity : semelleBogieEntity.datas) {
                TextView measure = this.createTextViewMeasureTablet(semelleDataEntity);
                measuresView.addView(measure);
            }
            bogieView.addView(measuresView);

            ((LinearLayout)getView().findViewById(container_bogie)).addView(bogieView);

            container_bogie = R.id.container_content_bogie_two;
        }
    }

    private void initViewHandset() {
        for (SemelleBogieEntity semelleBogieEntity : this.compo.bogies) {

            LinearLayout bogieView = (LinearLayout) LayoutInflater.from(getActivity()).inflate(R.layout.content_sole_bogie, null);
            TextView bogieNumber = (TextView) bogieView.findViewById(R.id.textView_sole_bogie_number);
            CheckBox bmIsole = (CheckBox) bogieView.findViewById(R.id.checkbox_sole_bmisole);
            CheckBox bogieIsole = (CheckBox) bogieView.findViewById(R.id.checkbox_sole_bogieisole);

            bogieNumber.setText(Integer.toString(semelleBogieEntity.bogieIdentifier));
            bmIsole.setChecked(semelleBogieEntity.bmSole);
            bogieIsole.setChecked(semelleBogieEntity.bogieIsole);

            int index = 0;
            int container = 0;
            View measuresView = LayoutInflater.from(getActivity()).inflate(R.layout.content_sole_measures, null);
            for (SemelleDataEntity semelleDataEntity : semelleBogieEntity.datas) {

                if ((index % 2) == 0) {
                    if (container == 0) container = R.id.container_measure_one_two;
                    else                container = R.id.container_measure_three_four;
                }

                LinearLayout layout = (LinearLayout) measuresView.findViewById(container);
                layout.addView(this.createTextViewMeasureHandset(semelleDataEntity));
                index++;
            }
            bogieView.addView(measuresView);

            ((LinearLayout) getView().findViewById(R.id.container_driving)).addView(bogieView);
        }
    }

    private TextView createTextViewMeasureTablet(SemelleDataEntity semelleDataEntity) {
        TextView                    textView           = new TextView(getActivity());
        TableLayout.LayoutParams    params             = new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT);

        params.setMargins(0, (int)UtilsUI.convertDipToPx(20, getActivity()), 0, 0);
        textView.setLayoutParams(params);

        String measure = getString(R.string.content_text_measure, semelleDataEntity.numWheel, semelleDataEntity.measure);
        textView.setText(measure);
        return textView;
    }

    private TextView createTextViewMeasureHandset(SemelleDataEntity semelleDataEntity) {
        TextView textView                           = new TextView(getActivity());
        TableLayout.LayoutParams params             = new TableLayout.LayoutParams(0, TableLayout.LayoutParams.WRAP_CONTENT, 1f);

        String measure = getString(R.string.content_text_measure, semelleDataEntity.numWheel, semelleDataEntity.measure);
        textView.setText(measure);
        textView.setLayoutParams(params);
        return textView;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LogSncf.e("FragmentDriving", "onDestroy " + this.compo.name);
    }

}
