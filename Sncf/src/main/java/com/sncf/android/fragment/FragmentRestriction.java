package com.sncf.android.fragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.sncf.android.R;
import com.sncf.android.base.BaseFragment;
import com.sncf.android.entity.RestrictionEntity;
import com.sncf.android.orm.criteria.ICriteria;
import com.sncf.android.orm.criteria.SQLCriteria;
import com.sncf.android.orm.criteria.SQLPredicate;
import com.sncf.android.orm.criteria.operators.EqualSqlPredicate;
import com.sncf.android.orm.dao.RestrictionDAO;
import com.sncf.android.provider.SncfContent;
import com.sncf.android.utils.DateHelper;
import com.sncf.android.utils.LogSncf;

import java.util.Date;
import java.util.Locale;

/**
 * Created by zzzwist on 26/12/13.
 */
public class FragmentRestriction extends BaseFragment {

    private static final String             KEY_PARAM_SAVE_ID_RESTRICTION = "saveidrestriction";
    public static final String              EXTRA_RESTRICTION_ID = "extrarestrictionid";

    private int                             mCurrentIdRestriction;
    private RestrictionEntity               mRestrictionEntity;

    @Override
    protected int getContentLayout() {
        return R.layout.fragment_restriction;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (savedInstanceState == null)
            mCurrentIdRestriction = this.getParameters();
        else
            mCurrentIdRestriction = savedInstanceState.getInt(KEY_PARAM_SAVE_ID_RESTRICTION);

        RestrictionDAO dao          = new RestrictionDAO(view.getContext());
        ICriteria criteria          = new SQLCriteria();
        criteria.add(new SQLPredicate(SncfContent.t_gr_restriction_mvc.Columns.ID.getName(), new EqualSqlPredicate(mCurrentIdRestriction)));
        mRestrictionEntity = (RestrictionEntity) dao.read(criteria).toArray()[0];

        initializeViewContent(mRestrictionEntity);
    }

    public void updateView(RestrictionEntity entity) {
        mCurrentIdRestriction = entity.getPrimaryKeyValue();
        initializeViewContent(entity);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(KEY_PARAM_SAVE_ID_RESTRICTION, mCurrentIdRestriction);
    }

    private Integer getParameters() {
        if (getArguments() != null) {
            if (getArguments().containsKey(EXTRA_RESTRICTION_ID)) {
                int id = getArguments().getInt(EXTRA_RESTRICTION_ID);
                return id;
            }
        }
        return null;
    }

    private void initializeViewContent(RestrictionEntity entity) {
        mRestrictionEntity = entity;
        ((TextView)getView().findViewById(R.id.textView_title_content)).setText(mRestrictionEntity.lib_reduit);

        String specificiteAndSite   = mRestrictionEntity.specificite + " - " + mRestrictionEntity.site_poseur;
        String sitePoseurAndPoseur  = mRestrictionEntity.site_poseur + " - " + mRestrictionEntity.log_poseur;
        String datePose             = DateHelper.formatDate(mRestrictionEntity.date_pose, "yyyy-MM-dd hh:mm:ss.S", "dd/MM/yyyy", Locale.getDefault());
        String location             = mRestrictionEntity.localisation;

        Date date                   = DateHelper.getDateObjectFromStringDate(mRestrictionEntity.date_pose, "yyyy-MM-dd hh:mm:ss.S", Locale.getDefault());
        int nbDay;
        if (date == null)           nbDay = 0;
        else                        nbDay = DateHelper.getDaysNumberSinceDateToToday(date);

        boolean amorti              = mRestrictionEntity.amorti;
        String dateAmortie          = DateHelper.formatDate(mRestrictionEntity.date_amortissement, "yyyy-MM-dd hh:mm:ss.S", "dd/MM/yyyy", Locale.getDefault());
        String logAmorti            = mRestrictionEntity.log_amortissement;
        String comment              = mRestrictionEntity.commentaire;
        String comment_m            = mRestrictionEntity.commentaire_m;

        ((TextView) getView().findViewById(R.id.textView_restriction_spe_and_site_content)).setText(specificiteAndSite);
        ((TextView) getView().findViewById(R.id.textView_restriction_sitep_poseur_content)).setText(sitePoseurAndPoseur);
        ((TextView) getView().findViewById(R.id.textView_restriction_date_pose_content)).setText(datePose);
        ((TextView) getView().findViewById(R.id.textView_restriction_location_content)).setText(location);
        ((TextView) getView().findViewById(R.id.textView_restriction_nb_day_since_pose_content)).setText(nbDay > 1 ? nbDay + getString(R.string.content_text_days) : nbDay + getString(R.string.content_text_day));
        ((TextView) getView().findViewById(R.id.textView_restriction_amorti_content)).setText(amorti ? getString(R.string.content_text_yes) : getString(R.string.content_text_no));
        ((TextView) getView().findViewById(R.id.textView_restriction_amorti_date_content)).setText(dateAmortie);
        ((TextView) getView().findViewById(R.id.textView_restriction_amorti_name_content)).setText(logAmorti);
        if (TextUtils.isEmpty(comment))
            getView().findViewById(R.id.textView_restriction_comment_content).setVisibility(View.GONE);
        else {
            getView().findViewById(R.id.textView_restriction_comment_content).setVisibility(View.VISIBLE);
            ((TextView) getView().findViewById(R.id.textView_restriction_comment_content)).setText(comment);
        }
        ((TextView) getView().findViewById(R.id.textView_restriction_comment_m_content)).setText(comment_m);
    }
}
