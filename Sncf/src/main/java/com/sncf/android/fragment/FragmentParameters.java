package com.sncf.android.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.sncf.android.R;
import com.sncf.android.base.BaseFragment;
import com.sncf.android.components.CheckableLinearLayout;
import com.sncf.android.preference.PreferenceKey;
import com.sncf.android.preference.PreferencesManager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by zzzwist on 11/01/14.
 */
public class FragmentParameters extends BaseFragment implements AdapterView.OnItemClickListener {

//    private GridView                mGridView;
//    private STFAdapter              mAdapter;
//    private List<String>            mListSTFName;

    private PreferencesManager      mPrefManager;

//    private int                     mPositionSelection;

    @Override
    protected int getContentLayout() {
        return R.layout.fragment_parameters;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mPrefManager = PreferencesManager.getInstance(getActivity());

//        mGridView = (GridView) view.findViewById(R.id.gridView_stf);

        ((TextView)view.findViewById(R.id.textView_title_content)).setText(getString(R.string.title_header_parameters));

//        mListSTFName = Arrays.asList(getResources().getStringArray(R.array.array_value_stf));
//        int[] arraySTF = {R.drawable.rer_d, R.drawable.rer_c, R.drawable.trans_p,
//                R.drawable.trans_r, R.drawable.trans_u, R.drawable.rer_d};
//        mAdapter = new STFAdapter(arraySTF);
//        mGridView.setAdapter(mAdapter);
//        mGridView.setOnItemClickListener(this);
//
//        if (mPrefManager.getString(PreferenceKey.PREFERENCE_ID_USER_STF, null) != null) {
//            mPositionSelection = mListSTFName.indexOf(mPrefManager.getString(PreferenceKey.PREFERENCE_ID_USER_STF, ""));
//        }
//        else
//            mPositionSelection = -1;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

//        mPrefManager.putString(PreferenceKey.PREFERENCE_ID_USER_STF, mListSTFName.get(position));
//
//        CheckableLinearLayout checkableLinearLayout = (CheckableLinearLayout) view;
//        checkableLinearLayout.setChecked(true);
//        mPositionSelection = position;
//        mAdapter.notifyDataSetChanged();
    }

//    private class HolderSTF {
//
//        public ImageView        mImage;
//        public View             mSelection;
//
//        public HolderSTF(View v) {
//            mImage = (ImageView) v.findViewById(R.id.imageView_grid_stf);
//            mSelection = v.findViewById(R.id.view_selected_cell_stf);
//        }
//    }

//    private class STFAdapter extends BaseAdapter {
//
//        private List<Integer>       mData;
//
//        public STFAdapter(int[] data) {
//            mData = new ArrayList<Integer>();
//            for (int idx = 0; idx < data.length; ++idx) {
//                mData.add(data[idx]);
//            }
//        }
//
//        @Override
//        public int getCount() {
//            return mData.size();
//        }
//
//        @Override
//        public Object getItem(int position) {
//            return mData.get(position);
//        }
//
//        @Override
//        public long getItemId(int position) {
//            return 0;
//        }
//
//        @Override
//        public View getView(int position, View convertView, ViewGroup parent) {
//
//            HolderSTF holder = null;
//            if (convertView == null) {
//                convertView = LayoutInflater.from(getActivity()).inflate(R.layout.cell_grid_stf, null);
//                holder = new HolderSTF(convertView);
//                convertView.setTag(holder);
//            }
//            else
//                holder = (HolderSTF) convertView.getTag();
//            holder.mImage.setImageResource(mData.get(position));
//            CheckableLinearLayout checkableLinearLayout = (CheckableLinearLayout) convertView;
//            if (checkableLinearLayout.isChecked() || mPositionSelection == position) {
//                if (!checkableLinearLayout.isChecked())
//                    checkableLinearLayout.setChecked(true);
//                holder.mSelection.setVisibility(View.VISIBLE);
//            }
//            else
//                holder.mSelection.setVisibility(View.GONE);
//            return convertView;
//        }
//    }
}
