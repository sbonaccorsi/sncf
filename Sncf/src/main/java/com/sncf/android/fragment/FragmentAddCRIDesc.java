package com.sncf.android.fragment;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.widget.SimpleCursorAdapter;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.sncf.android.R;
import com.sncf.android.data.CRIEdition;
import com.sncf.android.entity.AnomalyEntity;
import com.sncf.android.entity.ConclusionEntity;
import com.sncf.android.entity.RepairEntity;
import com.sncf.android.provider.SncfContent;
import com.sncf.android.utils.LogSncf;

/**
 * Created by zzzwist on 18/01/14.
 */
public class FragmentAddCRIDesc extends FragmentAddCRISpinnerManagment implements AdapterView.OnItemSelectedListener {

    private CRIEdition              mEditionData;

    private EditText                mEditTextSign;
    private EditText                mEditTextInt;
    private Spinner                 mSpinnerAnom;
    private Spinner                 mSpinnerRepair;
    private Spinner                 mSpinnerConclusion;

    @Override
    protected int getContentLayout() {
        return R.layout.fragment_add_cri_desc;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mEditionData                = CRIEdition.getInstance();

        mEditTextSign               = (EditText) view.findViewById(R.id.editText_add_cri_signalement);
        mEditTextInt                = (EditText) view.findViewById(R.id.editText_add_cri_intervention);
        mSpinnerAnom                = (Spinner) view.findViewById(R.id.spinner_select_anomaly);
        mSpinnerRepair              = (Spinner) view.findViewById(R.id.spinner_select_repair);
        mSpinnerConclusion          = (Spinner) view.findViewById(R.id.spinner_select_conclusion);

        mSpinnerAnom.setOnItemSelectedListener(this);
        mSpinnerRepair.setOnItemSelectedListener(this);
        mSpinnerConclusion.setOnItemSelectedListener(this);
        mEditTextSign.addTextChangedListener(mTextWatcherSignalment);
        mEditTextInt.addTextChangedListener(mTextWatcherInt);

        this.initializeSpinner(mSpinnerAnom, SncfContent.t_gr_anomalies.CONTENT_URI,
                SncfContent.t_gr_anomalies.Columns.VALIDE.getName() + "=?", new String[]{Integer.toString(1)},
                null, SncfContent.t_gr_anomalies.Columns.ANOM_NOM.getName());
        this.initializeSpinner(mSpinnerRepair, SncfContent.t_gr_reparations.CONTENT_URI,
                SncfContent.t_gr_reparations.Columns.VALIDE.getName() + "=?", new String[]{Integer.toString(1)},
                null, SncfContent.t_gr_reparations.Columns.REPARATION_NOM.getName());
        this.initializeSpinner(mSpinnerConclusion, SncfContent.t_gr_conclusions.CONTENT_URI,
                SncfContent.t_gr_conclusions.Columns.VALIDE.getName() + "=?", new String[]{Integer.toString(1)},
                null, SncfContent.t_gr_conclusions.Columns.CONCLUSION_CRI.getName());

        ((TextView)view.findViewById(R.id.textView_title_sub_header)).setText(getString(R.string.title_header_add_cri_sign_int));
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
        SimpleCursorAdapter                     adapter;
        Cursor                                  cursor;

        adapter                                 = (SimpleCursorAdapter) adapterView.getAdapter();
        cursor                                  = adapter.getCursor();
        if (!cursor.moveToPosition(position))   return;

        if (mSpinnerAnom == adapterView) {
            AnomalyEntity anomalyEntity         = new AnomalyEntity(cursor);
            mEditionData.setId_anomaly(anomalyEntity.getPrimaryKeyValue());
        }
        else if (mSpinnerRepair == adapterView) {
            RepairEntity repairEntity           = new RepairEntity(cursor);
            mEditionData.setId_repair(repairEntity.getPrimaryKeyValue());
        }
        else if (mSpinnerConclusion == adapterView) {
            ConclusionEntity conclusionEntity   = new ConclusionEntity(cursor);
            mEditionData.setId_conclusion(conclusionEntity.getPrimaryKeyValue());
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    private TextWatcher             mTextWatcherSignalment = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {}
        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            mEditionData.setSignalment(charSequence.toString());
        }
        @Override
        public void afterTextChanged(Editable editable) {}
    };

    private TextWatcher             mTextWatcherInt = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {}
        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            mEditionData.setIntervention(charSequence.toString());
        }
        @Override
        public void afterTextChanged(Editable editable) {}
    };

    @Override
    public void onDestroy() {
        super.onDestroy();
        LogSncf.e("FragmentAddCRIDesc", "onDestroy");
    }
}
