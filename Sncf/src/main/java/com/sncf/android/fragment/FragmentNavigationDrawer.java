package com.sncf.android.fragment;

import android.support.v7.app.ActionBarActivity;;
import android.app.Activity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.Toast;

import com.sncf.android.R;
import com.sncf.android.base.AbstractListFragment;
import com.sncf.android.components.CheckableLinearLayout;
import com.sncf.android.preference.PreferenceKey;
import com.sncf.android.preference.PreferencesManager;

import java.util.Arrays;
import java.util.List;

public class FragmentNavigationDrawer extends AbstractListFragment {

    private static final String             STATE_SELECTED_POSITION = "selected_navigation_drawer_position";
    private static final String             PREF_USER_LEARNED_DRAWER = "navigation_drawer_learned";
    private NavigationDrawerCallbacks       mCallbacks;
    private ActionBarDrawerToggle           mDrawerToggle;
    private DrawerLayout                    mDrawerLayout;
    private View                            mFragmentContainerView;
    private int                             mCurrentSelectedPosition = 0;
    private boolean                         mFromSavedInstanceState;
    private boolean                         mUserLearnedDrawer;
    private List<String>                    mData;

    public FragmentNavigationDrawer() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getActivity());
        mUserLearnedDrawer = sp.getBoolean(PREF_USER_LEARNED_DRAWER, false);

        if (savedInstanceState != null) {
            mCurrentSelectedPosition = savedInstanceState.getInt(STATE_SELECTED_POSITION);
            mFromSavedInstanceState = true;
        }
    }

    @Override
    public void onActivityCreated (Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    protected int getContentLayout() {
        return R.layout.fragment_navigation_drawer;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mListView.setItemChecked(mCurrentSelectedPosition, true);

        mData = Arrays.asList(getResources().getStringArray(R.array.array_list_menu_drawer));
        mAdapter = new ListBaseAdapter(mData);
        mListView.setAdapter(mAdapter);
    }

    public boolean isDrawerOpen() {
        return mDrawerLayout != null && mDrawerLayout.isDrawerOpen(mFragmentContainerView);
    }

    public void setUp(int fragmentId, DrawerLayout drawerLayout) {
        mFragmentContainerView = getActivity().findViewById(fragmentId);
        mDrawerLayout = drawerLayout;
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        mDrawerToggle = new ActionBarDrawerToggle(
                getActivity(),
                mDrawerLayout,
                R.drawable.ic_drawer,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close
        ) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                if (!isAdded()) {
                    return;
                }

                getActivity().supportInvalidateOptionsMenu(); // calls onPrepareOptionsMenu()
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                if (!isAdded()) {
                    return;
                }
                if (!mUserLearnedDrawer) {
                    mUserLearnedDrawer = true;
                    SharedPreferences sp = PreferenceManager
                            .getDefaultSharedPreferences(getActivity());
                    sp.edit().putBoolean(PREF_USER_LEARNED_DRAWER, true).commit();
                }
                getActivity().supportInvalidateOptionsMenu(); // calls onPrepareOptionsMenu()
            }
        };
//        if (!mUserLearnedDrawer && !mFromSavedInstanceState) {
//            mDrawerLayout.openDrawer(mFragmentContainerView);
//        }
        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        selectItem(mCurrentSelectedPosition);
    }

    private void selectItem(int position) {
        mCurrentSelectedPosition = position;
        if (mListView != null) {
            mListView.setItemChecked(position, true);
        }
        if (mDrawerLayout != null) {
            mDrawerLayout.closeDrawer(mFragmentContainerView);
        }
        if (mCallbacks != null) {
            mCallbacks.onNavigationDrawerItemSelected(position);
        }
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mCallbacks = (NavigationDrawerCallbacks) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException("Activity must implement NavigationDrawerCallbacks.");
        }
    }

    public void setDrawerLockMode(int mode) {
        mDrawerLayout.setDrawerLockMode(mode);
    }

    public void displayDrawerToggle(boolean value) {
        mDrawerToggle.setDrawerIndicatorEnabled(value);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(STATE_SELECTED_POSITION, mCurrentSelectedPosition);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (mDrawerLayout != null && isDrawerOpen()) {
            inflater.inflate(R.menu.global, menu);
            showGlobalContextActionBar();
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showGlobalContextActionBar() {
        ActionBar actionBar = getActionBar();
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setTitle(R.string.app_name);
    }

    private ActionBar getActionBar() {
        return ((ActionBarActivity) getActivity()).getSupportActionBar();
    }

    @Override
    protected View initializeView(View convertView) {
        ViewHolderMenuDrawer holder = null;
        if (convertView == null) {
            convertView = LayoutInflater.from(getActivity()).inflate(R.layout.cell_list_menu_drawer, null);
            holder = new ViewHolderMenuDrawer(convertView);
            convertView.setTag(holder);
        }
        return convertView;
    }

    @Override
    protected void configureView(int position, BaseHolder baseHolder, View convertView) {
        ViewHolderMenuDrawer holder = (ViewHolderMenuDrawer) baseHolder;
        holder.mSection.setText(mData.get(position));

        if (((CheckableLinearLayout)convertView).isChecked() || mCurrentSelectedPosition == position)
            holder.mSelectedView.setVisibility(View.VISIBLE);
        else
            holder.mSelectedView.setVisibility(View.GONE);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

         CheckableLinearLayout checkableLinearLayout = (CheckableLinearLayout) view;
         checkableLinearLayout.setChecked(true);
         selectItem(position);
    }

    public static interface NavigationDrawerCallbacks {
        void onNavigationDrawerItemSelected(int position);
    }

    public class ViewHolderMenuDrawer extends BaseHolder {

        public TextView         mSection;
        public View             mSelectedView;

        public ViewHolderMenuDrawer(View v) {
            mSection = (TextView) v.findViewById(R.id.textView_content_menudrawer);
            mSelectedView = v.findViewById(R.id.view_selected_cell_drawer);
        }
    }
}
