package com.sncf.android.fragment;

import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.util.SparseArray;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;

import com.sncf.android.R;
import com.sncf.android.base.AbstractCursorListFragment;
import com.sncf.android.components.CheckableLinearLayout;
import com.sncf.android.entity.CRIEntity;
import com.sncf.android.entity.FunctionEntity;
import com.sncf.android.entity.RameEntity;
import com.sncf.android.entity.RestrictionEntity;
import com.sncf.android.entity.SemelleControlesEntity;
import com.sncf.android.orm.criteria.ICriteria;
import com.sncf.android.orm.criteria.SQLCriteria;
import com.sncf.android.orm.criteria.SQLPredicate;
import com.sncf.android.orm.criteria.aggregators.AndAggregator;
import com.sncf.android.orm.criteria.aggregators.OrAggregator;
import com.sncf.android.orm.criteria.operators.EqualSqlPredicate;
import com.sncf.android.orm.dao.FunctionDAO;
import com.sncf.android.orm.dao.RameDAO;
import com.sncf.android.preference.PreferenceKey;
import com.sncf.android.preference.PreferencesManager;
import com.sncf.android.provider.SncfContent;
import com.sncf.android.utils.DateHelper;
import com.sncf.android.utils.LogSncf;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

/**
 * Created by zzzwist on 26/12/13.
 */
public class FragmentNavigation extends AbstractCursorListFragment {

    public static final String          KEY_PARAM_SAVE_CURRENT_POSITION_DRAWER = "savecurrentpositiondrawer";
    public static final String          KEY_PARAM_POSITION_NAVIGATION = "keyparampositionnavigation";

    private OnOpenRightPane             mListenerOpenRightPane;
    private int                         mCurrentLoaderId;
    private int                         mCurrentPositionNavigation;

    private ICriteria                   mCriteriaLoader                     = new SQLCriteria();
    private AndAggregator               mAggregatorParent                   = new AndAggregator();

    private AndAggregator               mAggregatorInterventions            = new AndAggregator();
    private AndAggregator               mAggregatorPut                      = new AndAggregator();
    private AndAggregator               mAggregatorAmortization             = new AndAggregator();

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (getActivity() instanceof OnOpenRightPane)
            mListenerOpenRightPane = (OnOpenRightPane) getActivity();
    }

    @Override
    protected int getContentLayout() {
        return R.layout.fragment_navigation;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        if (savedInstanceState == null)
            mCurrentPositionNavigation = this.getPositionNavigationParam();
        else
            mCurrentPositionNavigation = savedInstanceState.getInt(KEY_PARAM_SAVE_CURRENT_POSITION_DRAWER);

        super.onViewCreated(view, savedInstanceState);

        mListView.setOnItemClickListener(this);
        mAdapter = new ListCursorAdapter(getActivity(), R.layout.cell_list_navigation, null, new String[]{}, new int[]{}, 0);
        mListView.setAdapter(mAdapter);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(KEY_PARAM_SAVE_CURRENT_POSITION_DRAWER, mCurrentPositionNavigation);
    }

    public void updateNavigationFragmentData(int position) {
        int idLoader = 0;
        mCurrentPositionNavigation = position;
        idLoader = getIdLoaderByPositionNavigation(position);
        if (mCurrentLoaderId != idLoader) {

            this.clearFilter();

            clearChoiceListView();
            getLoaderManager().destroyLoader(mCurrentLoaderId);
            mCurrentLoaderId = idLoader;
            getLoaderManager().initLoader(idLoader, null, this);
        }
    }

    private int getPositionNavigationParam() {
        if (getArguments() != null) {
            if (getArguments().containsKey(KEY_PARAM_POSITION_NAVIGATION))
                return getArguments().getInt(KEY_PARAM_POSITION_NAVIGATION);
        }
        return -1;
    }

    private int getIdLoaderByPositionNavigation(int position) {
        int idLoader = 0;
        switch (position) {
            case 0:
                idLoader = R.id.id_loader_manager_restriction;
                break;
            case 1:
                idLoader = R.id.id_loader_manager_cri;
                break;
            case 2:
                idLoader = R.id.id_loader_manager_semelles;
                break;
        }
        return idLoader;
    }

    @Override
    protected List<Integer> getLoaderCallbacksIds() {
        return null;
    }

    @Override
    protected int getLoaderCallbackId() {
        mCurrentLoaderId = getIdLoaderByPositionNavigation(mCurrentPositionNavigation);
        return mCurrentLoaderId;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        Fragment fragment = getFragmentManager().findFragmentById(R.id.container_content_fragment);

        CheckableLinearLayout checkableLinearLayout = (CheckableLinearLayout) view;
        checkableLinearLayout.setChecked(true);

        Cursor cursor = mAdapter.getCursor();
        if (cursor.moveToPosition(position)) {
            switch (mCurrentLoaderId) {
                case R.id.id_loader_manager_restriction:
                    RestrictionEntity entity = new RestrictionEntity(cursor);

                    if (mListenerOpenRightPane != null) {
                        Bundle param = new Bundle();
                        param.putInt(FragmentRestriction.EXTRA_RESTRICTION_ID, entity.getPrimaryKeyValue());
                        if (!(fragment instanceof FragmentRestriction))
                            fragment = Fragment.instantiate(getActivity(), FragmentRestriction.class.getName(), param);
                        else
                            ((FragmentRestriction)fragment).updateView(entity);
                        mListenerOpenRightPane.openRightPane(fragment);
                    }
                    break;
                case R.id.id_loader_manager_cri:
                    CRIEntity entityCRI = new CRIEntity(cursor);
                    if (mListenerOpenRightPane != null) {
                        Bundle param = new Bundle();
                        param.putInt(FragmentCRI.EXTRA_CRI_ID, entityCRI.getPrimaryKeyValue());
                        if (!(fragment instanceof FragmentCRI))
                            fragment = Fragment.instantiate(getActivity(), FragmentCRI.class.getName(), param);
                        else
                            ((FragmentCRI)fragment).updateView(entityCRI);
                        mListenerOpenRightPane.openRightPane(fragment);
                    }
                    break;
                case R.id.id_loader_manager_semelles:
                    SemelleControlesEntity semelleControlesEntity = new SemelleControlesEntity(cursor);
                    if (mListenerOpenRightPane != null) {
                        Bundle param = new Bundle();
                        param.putInt(FragmentSole.EXTRA_CONTROL_ID, semelleControlesEntity.getPrimaryKeyValue());
                        fragment = Fragment.instantiate(getActivity(), FragmentSole.class.getName(), param);
                        mListenerOpenRightPane.openRightPane(fragment);
                    }
                    break;
            }
        }
        mAdapter.notifyDataSetChanged();
    }

    public void updatePredicatePeriodIntervention(SQLPredicate predicate) {
        this.updatePredicatePeriod(predicate, mAggregatorInterventions);

        LogSncf.e("FragmentNavigation", "Intervention Criteria update " + mCriteriaLoader.toString());

        getLoaderManager().restartLoader(mCurrentLoaderId, null, this);
    }

    public void updatePredicatePeriodPut(SQLPredicate predicate) {
        this.updatePredicatePeriod(predicate, mAggregatorPut);

        LogSncf.e("FragmentNavigation", "Put Criteria update " + mCriteriaLoader.toString());

        getLoaderManager().restartLoader(mCurrentLoaderId, null, this);
    }

    public void updatePredicatePeriodAmortization(SQLPredicate predicate) {
        this.updatePredicatePeriod(predicate, mAggregatorAmortization);

        LogSncf.e("FragmentNavigation", "Amortization Criteria update " + mCriteriaLoader.toString());

        getLoaderManager().restartLoader(mCurrentLoaderId, null, this);
    }

    private void updatePredicatePeriod(SQLPredicate predicate, final AndAggregator andAggregator) {

        mAggregatorParent.remove(andAggregator);

        andAggregator.removeAll();
        andAggregator.add(predicate);

        mAggregatorParent.add(andAggregator);
//        mCriteriaLoader.add(mAggregatorParent);
    }

    public void clearFilter() {
        mAggregatorParent.removeAll();
        mAggregatorInterventions.removeAll();
        mAggregatorPut.removeAll();
        mAggregatorAmortization.removeAll();
        getLoaderManager().restartLoader(mCurrentLoaderId, null, this);
    }

    private SQLPredicate        mPredicateCRICaisse;
    private SQLPredicate        mPredicateCRIRame;
    private AndAggregator       mAndCRI;

    private SQLPredicate        mPredicateRestCaisse;
    private SQLPredicate        mPredicateRestRame;
    private AndAggregator       mAndRest;

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {

        int id_rame_filter  = PreferencesManager.getInstance(getActivity()).getInt(PreferenceKey.PREFERENCE_ID_RAME_FILTER, -1);
        int num_caisse      = PreferencesManager.getInstance(getActivity()).getInt(PreferenceKey.PREFERENCE_ID_CAISSE_FILTER, -1);

        mCriteriaLoader.removeAll();

        switch (i) {
            case R.id.id_loader_manager_restriction:

                mPredicateRestRame = null;
                if (id_rame_filter != -1) {
                    if (mPredicateRestRame == null) mPredicateRestRame = new SQLPredicate(SncfContent.t_gr_restriction_mvc.Columns.ID_RAME.getName(), new EqualSqlPredicate(id_rame_filter));
                    if (mAggregatorParent.size() != 0) {
                        mAndRest = new AndAggregator(mPredicateRestRame, mAggregatorParent);
                        mCriteriaLoader.add(mAndRest);
                    }
                    else {
                        if (!mCriteriaLoader.contain(mPredicateRestRame)) mCriteriaLoader.add(mPredicateRestRame);
                    }
                }
                else {
                    if (mAggregatorParent.size() != 0) mCriteriaLoader.add(mAggregatorParent);
                }

                LogSncf.e("FragmentNavigation", "CriteriaLoader Restriction = " + mCriteriaLoader.toString());
                return new CursorLoader(getActivity(), SncfContent.t_gr_restriction_mvc.CONTENT_URI, null,
                        mCriteriaLoader.size() == 0 ? null : mCriteriaLoader.toString(), null, SncfContent.t_gr_restriction_mvc.Columns.PRIORITE.getName() + " DESC");

            case R.id.id_loader_manager_cri:

                if (id_rame_filter != -1) {
                    mPredicateCRIRame = new SQLPredicate(SncfContent.t_gr_cri.Columns.ID_RAME.getName(), new EqualSqlPredicate(id_rame_filter));
                    if (num_caisse != -1) {
                        mPredicateCRICaisse = new SQLPredicate(SncfContent.t_gr_cri.Columns.ID_VEHICULE.getName(), new EqualSqlPredicate(num_caisse));
                        mAndCRI = new AndAggregator(mPredicateCRIRame, mPredicateCRICaisse);
                        if (mAggregatorParent.size() != 0) {
                            mAndCRI.add(mAggregatorParent);
                        }
                        mCriteriaLoader.add(mAndCRI);
                    }
                    else {
                        if (mAggregatorParent.size() != 0) {
                            mAndCRI = new AndAggregator(mPredicateCRIRame, mAggregatorParent);
                            mCriteriaLoader.add(mAndCRI);
                        }
                        else {
                            if (!mCriteriaLoader.contain(mPredicateCRIRame)) mCriteriaLoader.add(mPredicateCRIRame);
                        }
                    }
                }
                else {
                    if (mAggregatorParent.size() != 0) mCriteriaLoader.add(mAggregatorParent);
                }
                LogSncf.e("FragmentNavigation", "CriteriaLoader CRI = " + mCriteriaLoader.toString());
                return new CursorLoader(getActivity(), SncfContent.t_gr_cri.CONTENT_URI, null, mCriteriaLoader.size() == 0 ? null : mCriteriaLoader.toString(), null,
                        SncfContent.t_gr_cri.Columns.DATE_INTERVENTION.getName() + " DESC");

            case R.id.id_loader_manager_semelles:

                return new CursorLoader(getActivity(), SncfContent.t_sem_controles.CONTENT_URI, null, mCriteriaLoader.size() == 0 ? null : mCriteriaLoader.toString(), null,
                        SncfContent.t_sem_controles.Columns.DATE_CTRL.getName() + " DESC");
        }
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        mAdapter.changeCursor(cursor);
        DialogFragment d = new DialogFragment();

    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {
        mAdapter.changeCursor(null);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LogSncf.e("FragmentNavigation", "OnDestroy");
    }

    public class NavigationHolder extends BaseHolder {

        public TextView             mTitle;
        public TextView             mSubTitle;
        public View                 mViewSelected;

        public NavigationHolder(View v) {
            mTitle = (TextView) v.findViewById(R.id.textView_navigation_title);
            mSubTitle = (TextView) v.findViewById(R.id.textView_navigation_subtitle);
            mViewSelected = v.findViewById(R.id.view_selected_cell_navigation);
        }
    }

    @Override
    protected int getItemId(Cursor cursor, int position) {
        return 0;
    }

    @Override
    protected BaseHolder initializeHolder(View view, int position) {

        NavigationHolder holder;
        if (view.getTag() == null) {
            holder = new NavigationHolder(view);
            view.setTag(holder);
        }
        else
            holder = (NavigationHolder) view.getTag();

        return holder;
    }

    @Override
    protected void configureView(BaseHolder baseHolder, View view, Cursor cursor, int position) {

        NavigationHolder holder = (NavigationHolder) baseHolder;
        if (cursor.moveToPosition(position)) {

            int p = mListView.getCheckedItemPosition();
            if (p == position)
                holder.mViewSelected.setVisibility(View.VISIBLE);
            else
                holder.mViewSelected.setVisibility(View.GONE);

            switch (mCurrentLoaderId) {
                case R.id.id_loader_manager_restriction:
                    this.initializeRestrictionRow(cursor, holder);
                    break;
                case R.id.id_loader_manager_cri:
                    this.initializeCRIRow(cursor, holder);
                    break;
                case R.id.id_loader_manager_semelles:
                    this.initializeSemelleRow(cursor, holder);
                    break;
            }
        }
    }

    private void initializeSemelleRow(Cursor cursor, NavigationHolder holder) {
        SemelleControlesEntity semelleControlesEntity = new SemelleControlesEntity(cursor);
        Integer idRame = semelleControlesEntity.idRame;
        if (idRame != null) {
            RameDAO rameDAO = new RameDAO(getActivity());
            ICriteria criteria = new SQLCriteria();
            criteria.add(new SQLPredicate(SncfContent.t_sncf_rames.Columns.ID_RAME.getName(), new EqualSqlPredicate(idRame)));
            Collection<RameEntity> collection = rameDAO.read(criteria);
            if (collection != null && collection.size() != 0) {
                RameEntity rameEntity = (RameEntity) collection.toArray()[0];

                holder.mTitle.setText(rameEntity.eab);
            }
        }
        holder.mSubTitle.setText(DateHelper.formatDate(semelleControlesEntity.dateControl, "yyyy-MM-dd hh:mm:ss.S", "dd/MM/yyyy", Locale.getDefault()));
    }

    private void initializeRestrictionRow(Cursor cursor, NavigationHolder holder) {
        RestrictionEntity restrictionEntity = new RestrictionEntity(cursor);
        holder.mTitle.setText(restrictionEntity.num_rame);
        holder.mSubTitle.setText(restrictionEntity.lib_reduit);
    }

    private void initializeCRIRow(Cursor cursor, NavigationHolder holder) {
        CRIEntity entityCRI = new CRIEntity(cursor);
        if (entityCRI != null) {

            String rame = "";
            String function = "";

            Integer id_rame = entityCRI.rameId;
            if (id_rame != null) {
                RameDAO rameDAO = new RameDAO(getActivity());
                ICriteria criteria = new SQLCriteria();
                criteria.add(new SQLPredicate(SncfContent.t_sncf_rames.Columns.ID_RAME.getName(), new EqualSqlPredicate(id_rame)));

                Collection<RameEntity> rameEntityCollection = rameDAO.read(criteria);

                if (rameEntityCollection != null && rameEntityCollection.size() != 0) {
                    RameEntity rameEntity = (RameEntity) rameEntityCollection.toArray()[0];
                    rame = rameEntity != null ? rameEntity.eab : "";
                }
            }

            Integer id_function = entityCRI.functionId;
            if (id_function != null) {
                FunctionDAO functionDAO = new FunctionDAO(getActivity());
                ICriteria criteria = new SQLCriteria();
                criteria.add(new SQLPredicate(SncfContent.t_gr_fonctions.Columns.FONCTION.getName(), new EqualSqlPredicate(id_function)));
                Collection<FunctionEntity> functionEntityCollection = functionDAO.read(criteria);
                if (functionEntityCollection != null && functionEntityCollection.size() != 0) {
                    FunctionEntity functionEntity = (FunctionEntity) functionEntityCollection.toArray()[0];
                    function = functionEntity.name;
                }
            }
            holder.mTitle.setText(rame);
            holder.mSubTitle.setText(function);
        }
    }

    public interface OnOpenRightPane {
        void openRightPane(Fragment fragment);
    }
}
