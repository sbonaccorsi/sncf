package com.sncf.android.fragment;

import com.sncf.android.R;
import com.sncf.android.base.BaseFragment;

/**
 * Created by zzzwist on 18/01/14.
 */
public class FragmentAddWear extends BaseFragment {

    @Override
    protected int getContentLayout() {
        return R.layout.fragment_add_wear;
    }
}
