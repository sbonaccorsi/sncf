package com.sncf.android.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListAdapter;
import android.widget.Toast;

import com.sncf.android.R;
import com.sncf.android.application.SncfApplication;
import com.sncf.android.base.BaseFragment;
import com.sncf.android.listener.OnDeployFilterPane;
import com.sncf.android.preference.PreferenceKey;
import com.sncf.android.preference.PreferencesManager;
import com.sncf.android.utils.LogSncf;

/**
 * Created by zzzwist on 26/12/13.
 */
public class FragmentFilter extends BaseFragment implements OnDeployFilterPane {

    @Override
    protected int getContentLayout() {
        return R.layout.fragment_filter;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Fragment fragment = getChildFragmentManager().findFragmentByTag(FragmentFilterRame.FRAGMENT_FILTER_TRAIN_TAG);
        if (fragment == null) {
            fragment = Fragment.instantiate(getActivity(), FragmentFilterRame.class.getName());
            getChildFragmentManager()
                    .beginTransaction()
                    .add(R.id.container_filter, fragment, FragmentFilterRame.FRAGMENT_FILTER_TRAIN_TAG)
                    .commit();
        }

        int id_rame = PreferencesManager.getInstance(getActivity()).getInt(PreferenceKey.PREFERENCE_ID_RAME_FILTER, -1);
        if (id_rame != -1)
            deployFilterPane();
    }

    @Override
    public void deployFilterPane() {
        if (SncfApplication.IS_UI_TABLET)
            this.handleFragmentFilterForTablet();
        else
            this.handleFragmentFilterForPhone();
    }

    public void cancelFilters() {
        Toast.makeText(getActivity(), getString(R.string.content_text_cancel_filter), Toast.LENGTH_SHORT).show();

        FragmentFilterRame fragmentFilterRame = (FragmentFilterRame) getChildFragmentManager().findFragmentByTag(FragmentFilterRame.FRAGMENT_FILTER_TRAIN_TAG);
        if (fragmentFilterRame != null) {
            PreferencesManager.getInstance(getActivity()).putInt(PreferenceKey.PREFERENCE_ID_RAME_FILTER, -1);
            PreferencesManager.getInstance(getActivity()).putInt(PreferenceKey.PREFERENCE_ID_CAISSE_FILTER, -1);
            PreferencesManager.getInstance(getActivity()).putInt(PreferenceKey.PREFERENCE_POSITION_RAME_SELECTED, -1);
            fragmentFilterRame.clearChoiceListView();
            fragmentFilterRame.notifyDataSetChanged();
        }
        FragmentFilterCaisse fragmentFilterCaisse = (FragmentFilterCaisse) getChildFragmentManager().findFragmentByTag(FragmentFilterCaisse.FRAGMENT_FILTER_CAISSE_TAG);
        if (fragmentFilterCaisse != null) {
            getChildFragmentManager().beginTransaction().remove(fragmentFilterCaisse).commit();
        }
    }

    private void handleFragmentFilterForTablet() {
        Fragment fragment = getChildFragmentManager().findFragmentByTag(FragmentFilterCaisse.FRAGMENT_FILTER_CAISSE_TAG);
        if (fragment == null) {
            fragment = Fragment.instantiate(getActivity(), FragmentFilterCaisse.class.getName());
            getChildFragmentManager()
                    .beginTransaction()
                    .add(R.id.container_filter, fragment, FragmentFilterCaisse.FRAGMENT_FILTER_CAISSE_TAG)
                    .commit();
        }
        else {
            ((FragmentFilterCaisse)fragment).updateListFilterCaisse();
        }
    }

    private void handleFragmentFilterForPhone() {
        Fragment fragment = getChildFragmentManager().findFragmentById(R.id.container_filter);
        if (!(fragment instanceof FragmentFilterCaisse)) {
            fragment = Fragment.instantiate(getActivity(), FragmentFilterCaisse.class.getName());
            getChildFragmentManager()
                    .beginTransaction()
                    .add(R.id.container_filter, fragment, FragmentFilterCaisse.FRAGMENT_FILTER_CAISSE_TAG)
                    .commit();
        }
    }

    public boolean onBackPressed() {

        if (!SncfApplication.IS_UI_TABLET) {
            Fragment fragment = getChildFragmentManager().findFragmentByTag(FragmentFilterCaisse.FRAGMENT_FILTER_CAISSE_TAG);
            if (fragment instanceof FragmentFilterCaisse) {
                getChildFragmentManager()
                        .beginTransaction()
                        .remove(fragment)
                        .commit();
                return false;
            }
            else
                return true;
        }
        else
            return true;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LogSncf.e("FragmentFilter", "onDestroy");
    }
}
