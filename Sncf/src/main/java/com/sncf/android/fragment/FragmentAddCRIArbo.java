package com.sncf.android.fragment;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.TextView;

import com.sncf.android.R;
import com.sncf.android.data.CRIEdition;
import com.sncf.android.entity.FunctionEntity;
import com.sncf.android.entity.LevelFiveEntity;
import com.sncf.android.entity.LevelFourEntity;
import com.sncf.android.entity.LevelSixEntity;
import com.sncf.android.entity.LevelThreeEntity;
import com.sncf.android.entity.SetEntity;
import com.sncf.android.provider.SncfContent;
import com.sncf.android.utils.LogSncf;

/**
 * Created by zzzwist on 18/01/14.
 */
public class FragmentAddCRIArbo extends FragmentAddCRISpinnerManagment implements AdapterView.OnItemSelectedListener {

    private Spinner             mSpinnerFunction;
    private Spinner             mSpinnerSet;
    private Spinner             mSpinnerLevel3;
    private Spinner             mSpinnerLevel4;
    private Spinner             mSpinnerLevel5;
    private Spinner             mSpinnerLevel6;

    private String              mCSFilter;

    private CRIEdition          mEditionData;

    @Override
    protected int getContentLayout() {
        return R.layout.fragment_add_cri_arbo;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mEditionData                = CRIEdition.getInstance();

        mSpinnerFunction            = (Spinner) view.findViewById(R.id.spinner_select_function);
        mSpinnerSet                 = (Spinner) view.findViewById(R.id.spinner_select_set);
        mSpinnerLevel3              = (Spinner) view.findViewById(R.id.spinner_select_level_3);
        mSpinnerLevel4              = (Spinner) view.findViewById(R.id.spinner_select_level_4);
        mSpinnerLevel5              = (Spinner) view.findViewById(R.id.spinner_select_level_5);
        mSpinnerLevel6              = (Spinner) view.findViewById(R.id.spinner_select_level_6);

        mSpinnerFunction.setOnItemSelectedListener(this);
        mSpinnerSet.setOnItemSelectedListener(this);
        mSpinnerLevel3.setOnItemSelectedListener(this);
        mSpinnerLevel4.setOnItemSelectedListener(this);
        mSpinnerLevel5.setOnItemSelectedListener(this);
        mSpinnerLevel6.setOnItemSelectedListener(this);

        ((TextView)view.findViewById(R.id.textView_title_sub_header)).setText(getString(R.string.title_header_add_cri_arbo));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LogSncf.e("FragmentAddCRIArbo", "onDestroy");
    }

    public void updateState(String cs) {
        // TODO update on selection serie
        LogSncf.e("FragmentAddCRIArbo", "updateState");
        mCSFilter = cs;
        if (cs == null) {
            mEditionData.setId_function(null);
            this.manageSelectionSpinnerFunction(null, 0);
            mSpinnerFunction.setAdapter(null);
            return;
        }

        this.initializeSpinner(mSpinnerFunction, SncfContent.t_gr_fonctions.CONTENT_URI, SncfContent.t_gr_fonctions.Columns.CODE_SERIE.getName() + "=?",
                new String[]{cs}, null, SncfContent.t_gr_fonctions.Columns.LIBELLE_NIVEAU.getName());
    }

    private void manageSelectionSpinnerFunction(Cursor cursor, int position) {
        if (position == 0) {
            mEditionData.setId_function(null);

            this.manageSelectionSpinnerSet(null, 0);
            mSpinnerSet.setAdapter(null);
            return;
        }
        if (cursor.moveToPosition(position - 1)) {
            FunctionEntity functionEntity       = new FunctionEntity(cursor);
            mEditionData.setId_function(functionEntity.getPrimaryKeyValue());

            this.initializeSpinner(mSpinnerSet, SncfContent.t_gr_ensembles.CONTENT_URI,
                    SncfContent.t_gr_ensembles.Columns.CODE_SERIE.getName() + "=? AND " + SncfContent.t_gr_ensembles.Columns.FONCTION.getName() + "=?",
                    new String[]{mCSFilter, Integer.toString(functionEntity.id)},
                    null, SncfContent.t_gr_ensembles.Columns.LIBELLE_NIVEAU.getName());
        }
    }

    private void manageSelectionSpinnerSet(Cursor cursor, int position) {
        if (position == 0) {
            mEditionData.setId_set(null);

            this.manageSelectionSpinnerLevel3(null, 0);
            mSpinnerLevel3.setAdapter(null);
            return;
        }
        if (cursor.moveToPosition(position - 1)) {
            SetEntity setEntity                 = new SetEntity(cursor);
            mEditionData.setId_set(setEntity.getPrimaryKeyValue());

            this.initializeSpinner(mSpinnerLevel3, SncfContent.t_gr_niveau_3.CONTENT_URI,
                    SncfContent.t_gr_niveau_3.Columns.CODE_SERIE.getName() + "=? AND " +
                            SncfContent.t_gr_niveau_3.Columns.FONCTION.getName() + "=? AND " +
                            SncfContent.t_gr_niveau_3.Columns.ENSEMBLE.getName() + "=?",
                    new String[]{mCSFilter, Integer.toString(setEntity.function), Integer.toString(setEntity.set)},
                    null, SncfContent.t_gr_niveau_3.Columns.LIBELLE_NIVEAU.getName());
        }
    }

    private void manageSelectionSpinnerLevel3(Cursor cursor, int position) {
        if (position == 0) {
            mEditionData.setId_level3(null);

            this.manageSelectionSpinnerLevel4(null, 0);
            mSpinnerLevel4.setAdapter(null);
            return;
        }
        if (cursor.moveToPosition(position - 1)) {
            LevelThreeEntity levelThreeEntity   = new LevelThreeEntity(cursor);
            mEditionData.setId_level3(levelThreeEntity.getPrimaryKeyValue());

            this.initializeSpinner(mSpinnerLevel4, SncfContent.t_gr_niveau_4.CONTENT_URI,
                    SncfContent.t_gr_niveau_4.Columns.CODE_SERIE.getName() + "=? AND " +
                            SncfContent.t_gr_niveau_4.Columns.FONCTION.getName() + "=? AND " +
                            SncfContent.t_gr_niveau_4.Columns.ENSEMBLE.getName() + "=? AND " +
                            SncfContent.t_gr_niveau_4.Columns.NIVEAU_3.getName() + "=?",
                    new String[]{mCSFilter, Integer.toString(levelThreeEntity.functionId), Integer.toString(levelThreeEntity.setId),
                            Integer.toString(levelThreeEntity.id)},
                    null, SncfContent.t_gr_niveau_4.Columns.LIBELLE_NIVEAU.getName());
        }
    }

    private void manageSelectionSpinnerLevel4(Cursor cursor, int position) {
        if (position == 0) {
            mEditionData.setId_level4(null);

            this.manageSelectionSpinnerLevel5(null, 0);
            mSpinnerLevel5.setAdapter(null);
            return;
        }
        if (cursor.moveToPosition(position - 1)) {
            LevelFourEntity levelFourEntity     = new LevelFourEntity(cursor);
            mEditionData.setId_level4(levelFourEntity.getPrimaryKeyValue());

            this.initializeSpinner(mSpinnerLevel5, SncfContent.t_gr_niveau_5.CONTENT_URI,
                    SncfContent.t_gr_niveau_5.Columns.CODE_SERIE.getName() + "=? AND " +
                            SncfContent.t_gr_niveau_5.Columns.FONCTION.getName() + "=? AND " +
                            SncfContent.t_gr_niveau_5.Columns.ENSEMBLE.getName() + "=? AND " +
                            SncfContent.t_gr_niveau_5.Columns.NIVEAU_3.getName() + "=? AND " +
                            SncfContent.t_gr_niveau_5.Columns.NIVEAU_4.getName() + "=?",
                    new String[]{mCSFilter, Integer.toString(levelFourEntity.functionId), Integer.toString(levelFourEntity.setId),
                            Integer.toString(levelFourEntity.levelThreeId),
                            Integer.toString(levelFourEntity.id)},
                    null, SncfContent.t_gr_niveau_5.Columns.LIBELLE_NIVEAU.getName());
        }
    }

    private void manageSelectionSpinnerLevel5(Cursor cursor, int position) {
        if (position == 0) {
            mEditionData.setId_level5(null);

            this.manageSelectionSpinnerLevel6(null, 0);
            mSpinnerLevel6.setAdapter(null);
            return;
        }
        if (cursor.moveToPosition(position - 1)) {
            LevelFiveEntity levelFiveEntity     = new LevelFiveEntity(cursor);
            mEditionData.setId_level5(levelFiveEntity.getPrimaryKeyValue());

            this.initializeSpinner(mSpinnerLevel6, SncfContent.t_gr_niveau_6.CONTENT_URI,
                    SncfContent.t_gr_niveau_6.Columns.CODE_SERIE.getName() + "=? AND " +
                            SncfContent.t_gr_niveau_6.Columns.FONCTION.getName() + "=? AND " +
                            SncfContent.t_gr_niveau_6.Columns.ENSEMBLE.getName() + "=? AND " +
                            SncfContent.t_gr_niveau_6.Columns.NIVEAU_3.getName() + "=? AND " +
                            SncfContent.t_gr_niveau_6.Columns.NIVEAU_4.getName() + "=? AND " +
                            SncfContent.t_gr_niveau_6.Columns.NIVEAU_5.getName() + "=?",
                    new String[]{mCSFilter, Integer.toString(levelFiveEntity.functionId), Integer.toString(levelFiveEntity.setId), Integer.toString(levelFiveEntity.levelThreeId),
                            Integer.toString(levelFiveEntity.levelFourId), Integer.toString(levelFiveEntity.id)},
                    null, SncfContent.t_gr_niveau_6.Columns.LIBELLE_NIVEAU.getName());
        }
    }

    private void manageSelectionSpinnerLevel6(Cursor cursor, int position) {
        if (position == 0) {
            mEditionData.setId_level6(null);
            return;
        }
        if (cursor.moveToPosition(position - 1)) {
            LevelSixEntity levelSixEntity       = new LevelSixEntity(cursor);
            mEditionData.setId_level6(levelSixEntity.getPrimaryKeyValue());
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {

        SimpleCursorAdapter                     adapter;
        Cursor                                  cursor;

        adapter                                 = (SimpleCursorAdapter) adapterView.getAdapter();
        cursor                                  = adapter.getCursor();
        if (!cursor.moveToPosition(position))   return;

        if (mSpinnerFunction == adapterView)        this.manageSelectionSpinnerFunction(cursor, position);
        else if (mSpinnerSet == adapterView)        this.manageSelectionSpinnerSet(cursor, position);
        else if (mSpinnerLevel3 == adapterView)     this.manageSelectionSpinnerLevel3(cursor, position);
        else if (mSpinnerLevel4 == adapterView)     this.manageSelectionSpinnerLevel4(cursor, position);
        else if (mSpinnerLevel5 == adapterView)     this.manageSelectionSpinnerLevel5(cursor, position);
        else if (mSpinnerLevel6 == adapterView)     this.manageSelectionSpinnerLevel6(cursor, position);
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

}
