package com.sncf.android.fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.widget.SimpleCursorAdapter;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;

import com.sncf.android.R;
import com.sncf.android.components.CheckableLinearLayout;

/**
 * Created by zzzwist on 24/01/14.
 */
public class FragmentDialogListMultiChoice extends DialogFragment implements AdapterView.OnItemClickListener {

    public static final String              KEY_PARAM_TITLE_DIALOG                  = "titledialogue";
    public static final String              KEY_PARAM_POSITIVE_BUTTON               = "positivebutton";
    public static final String              KEY_PARAM_NEGATIVE_BUTTON               = "negativebutton";
    public static final String              KEY_PARAM_NEUTRAL_BUTTON                = "neutralbutton";
    public static final String              KEY_PARAM_IS_CANCELLABLE                = "iscancellable";
    public static final String              KEY_PARAM_COLUMNS_NAME_TO_SHOW          = "columnname";
    public static final String              KEY_PARAM_DIALOG_ID                     = "dialogid";
    public static final String              KEY_PARAM_MAX_ITEM_SELECTED             = "maxitemselected";
    public static final String              KEY_PARAM_CURRENT_NUMBER_ITEM_SELECTED  = "currentnumberitemselected";

    private static final String             SAVE_INSTANCE_DATA = "saveonstancedata";

    private int                             mIdDialog;
    private String                          mTitle;
    private Boolean                         mIsCancellable;
    private String                          mPositiveButton;
    private String                          mNegativeButton;
    private String                          mNeutralButton;
    private Integer                         mMaxItemSelected;
    private Integer                         mCurrentNumberItemSelected;
    private String[]                        mColumnsName;
    private Cursor                          mCursor;
    private SparseArray<String>             mHaveCheckedItem;

    private OnClickDialog                   mListener;
    private CursorAdapterDialogList         mAdapter;
    private ListView                        mListView;

    public interface OnClickDialog {
        void clickPositiveButton(DialogInterface dialog, int dialogId, int which, SparseArray<String> array);
        void clickNegativeButton(DialogInterface dialog, int dialogId, int which);
        void clickNeutralButton(DialogInterface dialog, int dialogId, int which);
        void onItemListClicked(int dialogId, AdapterView<?> parent, View view, int position, long id, Boolean isChecked, Cursor cursorToPosition);
    }

    public FragmentDialogListMultiChoice() {}

    public FragmentDialogListMultiChoice(SparseArray<String> haveCheckedItem, Cursor cursor,
                                         Bundle params, OnClickDialog listener) {
        mListener                   = listener;
        mCursor                     = cursor;
        mHaveCheckedItem            = haveCheckedItem;

        if (params != null) {
            if (params.containsKey(KEY_PARAM_DIALOG_ID))                        mIdDialog                   = params.getInt(KEY_PARAM_DIALOG_ID);
            if (params.containsKey(KEY_PARAM_TITLE_DIALOG))                     mTitle                      = params.getString(KEY_PARAM_TITLE_DIALOG);
            if (params.containsKey(KEY_PARAM_IS_CANCELLABLE))                   mIsCancellable              = params.getBoolean(KEY_PARAM_IS_CANCELLABLE);
            if (params.containsKey(KEY_PARAM_POSITIVE_BUTTON))                  mPositiveButton             = params.getString(KEY_PARAM_POSITIVE_BUTTON);
            if (params.containsKey(KEY_PARAM_NEGATIVE_BUTTON))                  mNegativeButton             = params.getString(KEY_PARAM_NEGATIVE_BUTTON);
            if (params.containsKey(KEY_PARAM_NEUTRAL_BUTTON))                   mNeutralButton              = params.getString(KEY_PARAM_NEUTRAL_BUTTON);
            if (params.containsKey(KEY_PARAM_COLUMNS_NAME_TO_SHOW))             mColumnsName                = params.getStringArray(KEY_PARAM_COLUMNS_NAME_TO_SHOW);
            if (params.containsKey(KEY_PARAM_MAX_ITEM_SELECTED))                mMaxItemSelected            = params.getInt(KEY_PARAM_MAX_ITEM_SELECTED);
            if (params.containsKey(KEY_PARAM_CURRENT_NUMBER_ITEM_SELECTED))     mCurrentNumberItemSelected  = params.getInt(KEY_PARAM_CURRENT_NUMBER_ITEM_SELECTED);
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder adb = new AlertDialog.Builder(getActivity());
        adb.setTitle(mTitle);
        adb.setCancelable(mIsCancellable);

        View customView = this.initCustomView();
        this.setCheckedItem();
        adb.setView(customView);

        if (mPositiveButton != null) {
            adb.setPositiveButton(mPositiveButton, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (mListener != null)
                        mListener.clickPositiveButton(dialog, mIdDialog, which, mHaveCheckedItem);
                }
            });
        }
        if (mNegativeButton != null) {
            adb.setNegativeButton(mNegativeButton, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (mListener != null)
                        mListener.clickNegativeButton(dialog, mIdDialog, which);
                }
            });
        }
        if (mNeutralButton != null) {
            adb.setNeutralButton(mNeutralButton, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (mListener != null)
                        mListener.clickNeutralButton(dialog, mIdDialog, which);
                }
            });
        }
        return adb.create();
    }

    private View initCustomView() {
        View customView = LayoutInflater.from(getActivity()).inflate(R.layout.custom_view_list_dialog, null);
        mAdapter = new CursorAdapterDialogList(getActivity(), R.layout.cell_item_dialog_list, mCursor, new String[]{}, new int[]{}, 0);
        mListView = (ListView) customView.findViewById(R.id.listView_dialog_list);
        mListView.setAdapter(mAdapter);
        mListView.setOnItemClickListener(this);
        return customView;
    }

    private void setCheckedItem() {
        for (int idx = 0; idx < mHaveCheckedItem.size(); ++idx) {
            int key = mHaveCheckedItem.keyAt(idx);
            if (mHaveCheckedItem.get(key) != null)
                mListView.setItemChecked(key, true);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        Boolean isChecked = ((CheckableLinearLayout)view).isChecked();
        ViewHolderDialog holder = (ViewHolderDialog) view.getTag();
        holder.mCheckItem.setChecked(isChecked);

        if (isChecked) {

            if (mMaxItemSelected > mCurrentNumberItemSelected) {
                mCurrentNumberItemSelected++;
                mHaveCheckedItem.put(position, holder.mTextItem.getText().toString());
            }
            else {
                ((CheckableLinearLayout)view).setChecked(false);
                holder.mCheckItem.setChecked(false);
                mListView.setItemChecked(position, false);
                isChecked = null;
            }

        }
        else {

            mHaveCheckedItem.remove(position);
            mCurrentNumberItemSelected--;

        }

        Cursor cursor = mAdapter.getCursor();
        cursor.moveToPosition(position);
        if (mListener != null)
            mListener.onItemListClicked(mIdDialog, parent, view, position, id, isChecked, cursor);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public class ViewHolderDialog {
        public TextView     mTextItem;
        public TextView     mSubTextItem;
        public CheckBox     mCheckItem;

        public ViewHolderDialog(View v) {
            mTextItem = (TextView) v.findViewById(R.id.textView_item_cell_dialog_list);
            mSubTextItem = (TextView) v.findViewById(R.id.textView_sub_item_cell_dialog_list);
            mCheckItem = (CheckBox) v.findViewById(R.id.checkBox_item_cell_dialog_list);
        }
    }

    private class CursorAdapterDialogList extends SimpleCursorAdapter {

        public CursorAdapterDialogList(Context context, int layout, Cursor c, String[] from, int[] to, int flags) {
            super(context, layout, c, from, to, flags);
        }

        @Override
        public int getCount() {
            return super.getCount();
        }

        @Override
        public long getItemId(int position) {
            Cursor cursor = getCursor();
            if (cursor.moveToPosition(position))    return cursor.getInt(cursor.getColumnIndex("_id"));
            else                                    return -1;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            ViewHolderDialog holder;
            Cursor cursor = getCursor();
            View v = super.getView(position, convertView, parent);
            if (v.getTag() == null) {
                holder = new ViewHolderDialog(v);
                v.setTag(holder);
            }
            else
                holder = (ViewHolderDialog) v.getTag();

            if (cursor.moveToPosition(position)) {

                if (mHaveCheckedItem.get(position) != null) {
                    holder.mCheckItem.setChecked(true);
                    ((CheckableLinearLayout)v).setChecked(true);
                }
                else {
                    holder.mCheckItem.setChecked(false);
                    ((CheckableLinearLayout)v).setChecked(false);
                }
                int size = mColumnsName.length;
                if (size == 2) {
                    holder.mSubTextItem.setVisibility(View.VISIBLE);
                    holder.mSubTextItem.setText(cursor.getString(cursor.getColumnIndex(mColumnsName[1])));
                }
                else if (size == 1)
                    holder.mSubTextItem.setVisibility(View.GONE);

                holder.mTextItem.setText(cursor.getString(cursor.getColumnIndex(mColumnsName[0])));
            }
            return v;
        }
    }
}
