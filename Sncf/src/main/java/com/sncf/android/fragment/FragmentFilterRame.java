package com.sncf.android.fragment;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import com.sncf.android.R;
import com.sncf.android.base.AbstractCursorListFragment;
import com.sncf.android.entity.RameEntity;
import com.sncf.android.listener.OnDeployFilterPane;
import com.sncf.android.orm.criteria.ICriteria;
import com.sncf.android.orm.criteria.SQLCriteria;
import com.sncf.android.orm.criteria.SQLPredicate;
import com.sncf.android.orm.criteria.operators.EqualSqlPredicate;
import com.sncf.android.orm.dao.RameDAO;
import com.sncf.android.preference.PreferenceKey;
import com.sncf.android.preference.PreferencesManager;
import com.sncf.android.provider.SncfContent;
import com.sncf.android.utils.LogSncf;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * Created by zzzwist on 25/12/13.
 */
public class FragmentFilterRame extends AbstractCursorListFragment implements TextWatcher {

    public static final String                  KEY_PARAM_SAVE_POSITION_CHECK = "savepositioncheck";
    public static final String                  FRAGMENT_FILTER_TRAIN_TAG = "fragmentfiltertraintag";

    private OnDeployFilterPane                  mListenerDeploy;
    private int                                 mCurrentPositionChecked = -1;

    private String                              mFilter = "";

    @Override
    protected int getContentLayout() {
        return R.layout.fragment_filter_rame;
    }

    @Override
    protected List<Integer> getLoaderCallbacksIds() {
        return null;
    }

    @Override
    protected int getLoaderCallbackId() {
        return R.id.id_loader_manager_filter_rame;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ((TextView)view.findViewById(R.id.textView_title_header_filter)).setText(getString(R.string.title_header_filter_rame));
        ((EditText)view.findViewById(R.id.editText_search_rame)).addTextChangedListener(this);

        if (getParentFragment() instanceof OnDeployFilterPane) {
            mListenerDeploy = (OnDeployFilterPane) getParentFragment();
        }

        mAdapter = new ListCursorAdapter(getActivity(), R.layout.cell_list_filter, null, new String[]{}, new int[]{}, 0);
        mListView.setAdapter(mAdapter);

        if (savedInstanceState != null) {
            mCurrentPositionChecked = savedInstanceState.getInt(KEY_PARAM_SAVE_POSITION_CHECK);
            if (mCurrentPositionChecked != -1)
                mListView.setItemChecked(mCurrentPositionChecked, true);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(KEY_PARAM_SAVE_POSITION_CHECK, mCurrentPositionChecked);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (id != -1) {
            RameDAO rameDAO = new RameDAO(getActivity());
            ICriteria criteria = new SQLCriteria();
            criteria.add(new SQLPredicate(SncfContent.t_sncf_rames.Columns.ID.getName(), new EqualSqlPredicate(id)));
            Collection<RameEntity> rameEntityCollection = rameDAO.read(criteria);
            if (rameEntityCollection != null && rameEntityCollection.size() != 0) {
                RameEntity rameEntity = (RameEntity) rameEntityCollection.toArray()[0];
                PreferencesManager.getInstance(getActivity())
                        .putInt(PreferenceKey.PREFERENCE_ID_RAME_FILTER, rameEntity.idRame);
                PreferencesManager.getInstance(getActivity()).putInt(PreferenceKey.PREFERENCE_POSITION_RAME_SELECTED, position);
            }

            mCurrentPositionChecked = position;
            mListView.setItemChecked(position, true);

            if (mListenerDeploy != null)
                mListenerDeploy.deployFilterPane();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LogSncf.e("FragmentFilterRame", "onDestroy");
    }

    public void notifyDataSetChanged() {
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        switch (i) {
            case R.id.id_loader_manager_filter_rame:
                return new CursorLoader(getActivity(), SncfContent.t_sncf_rames.CONTENT_URI, null,
                        SncfContent.t_sncf_rames.Columns.EAB.getName() + " LIKE \"" + mFilter  + "%\"", null, null);
        }
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        LogSncf.e("FragmentFilterRame", "onLoadFinished");
        int position = PreferencesManager.getInstance(getActivity()).getInt(PreferenceKey.PREFERENCE_POSITION_RAME_SELECTED, -1);
        if (position != -1) mListView.setSelection(position);
        mAdapter.changeCursor(cursor);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {
        mAdapter.changeCursor(null);
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        mFilter = charSequence.toString();
    }

    @Override
    public void afterTextChanged(Editable editable) {
        getLoaderManager().restartLoader(R.id.id_loader_manager_filter_rame, null, this);
    }

    private class TrainHolder extends BaseHolder {

        public TextView                         mName;
        public RadioButton                      mRadioButton;

        public TrainHolder(View v) {
            mName = (TextView) v.findViewById(R.id.textView_filter_name);
            mRadioButton = (RadioButton) v.findViewById(R.id.radioButton_filter_check);
        }
    }

    @Override
    protected int getItemId(Cursor cursor, int position) {
        if (cursor.moveToPosition(position)) {
            RameEntity rameEntity = new RameEntity(cursor);
            return rameEntity.getPrimaryKeyValue();
        }
        return -1;
    }

    @Override
    protected BaseHolder initializeHolder(View view, int position) {

        TrainHolder holder = null;
        if (view.getTag() == null) {
            holder = new TrainHolder(view);
            view.setTag(holder);
        }
        else
            holder = (TrainHolder) view.getTag();
        return holder;
    }

    @Override
    protected void configureView(BaseHolder baseHolder, View view, Cursor cursor, int position) {
        TrainHolder holder = (TrainHolder) baseHolder;
        if (cursor.moveToPosition(position)) {
            RameEntity rameEntity = new RameEntity(cursor);
            if (rameEntity != null) {
                holder.mName.setText(rameEntity.eab != null ? rameEntity.eab : "");
                int id_rame = PreferencesManager.getInstance(getActivity()).getInt(PreferenceKey.PREFERENCE_ID_RAME_FILTER, -1);
                if (id_rame != -1 && rameEntity.idRame == id_rame) {
                    mListView.setItemChecked(position, true);
                }
            }

            int p = mListView.getCheckedItemPosition();
            if (p == position)
                holder.mRadioButton.setChecked(true);
            else
                holder.mRadioButton.setChecked(false);
        }
    }
}
