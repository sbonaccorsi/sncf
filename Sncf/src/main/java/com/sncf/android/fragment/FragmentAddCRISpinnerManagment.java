package com.sncf.android.fragment;

import android.database.Cursor;
import android.net.Uri;
import android.widget.Spinner;

import com.sncf.android.adapter.CursorSpinnerAdapter;
import com.sncf.android.base.BaseFragment;

/**
 * Created by sbonaccorsi on 18/02/2014.
 */
public abstract class FragmentAddCRISpinnerManagment extends BaseFragment {

    protected void initializeSpinner(Spinner spinner, Uri uriTable, String selection, String[] selectionArgs, String sortOrder, String columnToDisplay) {

        Cursor cursor = getActivity().getContentResolver().query(uriTable, null, selection, selectionArgs, null);
        CursorSpinnerAdapter adp = new CursorSpinnerAdapter(getActivity(), android.R.layout.simple_spinner_item, cursor,
                new String[]{columnToDisplay}, new int[]{android.R.id.text1}, 0);
        adp.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adp);
    }
}
