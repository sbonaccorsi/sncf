package com.sncf.android.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TextView;

import com.sncf.android.R;
import com.sncf.android.application.SncfApplication;
import com.sncf.android.base.AbstractPagerFragment;
import com.sncf.android.base.BaseFragment;
import com.sncf.android.components.AsyncInitializerView;
import com.sncf.android.entity.CompoEntity;
import com.sncf.android.entity.InterventionEntity;
import com.sncf.android.entity.RameEntity;
import com.sncf.android.entity.SemelleBogieEntity;
import com.sncf.android.entity.SemelleControlesEntity;
import com.sncf.android.entity.SemelleDataEntity;
import com.sncf.android.orm.DAO;
import com.sncf.android.orm.Entity;
import com.sncf.android.orm.ORM;
import com.sncf.android.orm.criteria.ICriteria;
import com.sncf.android.orm.criteria.SQLCriteria;
import com.sncf.android.orm.criteria.SQLPredicate;
import com.sncf.android.orm.criteria.operators.EqualSqlPredicate;
import com.sncf.android.orm.dao.CompoDAO;
import com.sncf.android.orm.dao.InterventionDAO;
import com.sncf.android.orm.dao.RameDAO;
import com.sncf.android.orm.dao.SemelleBogieDAO;
import com.sncf.android.orm.dao.SemelleControleDAO;
import com.sncf.android.orm.dao.SemelleDataDAO;
import com.sncf.android.provider.SncfContent;
import com.sncf.android.utils.CriteriaHelper;
import com.sncf.android.utils.DateHelper;
import com.sncf.android.utils.LogSncf;

import java.util.Collection;
import java.util.Locale;
import java.util.Scanner;

/**
 * Created by zzzwist on 11/01/14.
 */
public class FragmentSole extends AbstractPagerFragment implements View.OnClickListener{

    public static final String              EXTRA_CONTROL_ID = "extrasemelleid";

    private SemelleControlesEntity          controle;

    private Integer                         mControlId;
    private int                             currentPageIndex;

    @Override
    protected int getContentLayout()        { return R.layout.fragment_sole;}

    @Override
    protected int getIdViewPager()          { return R.id.viewpager;        }

    @Override
    protected int getOffscreenPageLimit()   { return 1;                     }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        view.findViewById(R.id.button_next_pager).setOnClickListener(this);
        view.findViewById(R.id.button_prev_pager).setOnClickListener(this);
        view.findViewById(R.id.button_prev_pager).setEnabled(false);

        mControlId              = this.getParameters();
        this.currentPageIndex   = 0;
        ICriteria criteria      = new SQLCriteria();

        mPagerAdapter           = new ViewPagerAdapter(getFragmentManager());

        criteria.add(new SQLPredicate(SncfContent.t_sem_controles.Columns.ID.getName(), new EqualSqlPredicate(mControlId)));
        updateView(ORM.createEntityByPredicate(CriteriaHelper.equal(SncfContent.t_sem_controles.Columns.ID.getName(), mControlId), new SemelleControleDAO(getActivity())));

        this.mViewPager.setAdapter(mPagerAdapter);
        this.mViewPager.setCurrentItem(0, true);
    }

    @Override
    protected Fragment adapterGetItem(int position) {
        LogSncf.e("FragmentSole", "adapterGetItem = " + ((CompoEntity) this.controle.compos.toArray()[position]).name);
        return new FragmentDriving((CompoEntity) this.controle.compos.toArray()[position]);
    }

    @Override
    protected int adapterGetCount() {
        return this.controle.compos.size();
    }

    @Override
    protected void adapterDestroyItem(ViewGroup container, int position, Object object) {
        LogSncf.e("FragmentSole", "adapterDestroyItem = " + ((FragmentDriving)object).compo.name);
    }

    public void updateView(SemelleControlesEntity entitySemControl)
    {
        ORM.mapSemelleControleEntity(getActivity(), entitySemControl);
        this.controle                                               = entitySemControl;

        String date = DateHelper.formatDate(this.controle.dateControl, "yyyy-MM-dd hh:mm:ss.S", "dd/MM/yyyy", Locale.getDefault());
        ((TextView) getView().findViewById(R.id.textView_title_content))
                .setText(this.controle.intervention.name + " - " + this.controle.ControlerName);

        this.mPagerAdapter.notifyDataSetChanged();
        mViewPager.invalidate();

        if (this.controle.compos.size() == 1)
            getView().findViewById(R.id.container_button_bar_navigation).setVisibility(View.GONE);
        else
            getView().findViewById(R.id.container_button_bar_navigation).setVisibility(View.VISIBLE);
    }

    private Integer getParameters() {
        if (getArguments() != null) {
            if (getArguments().containsKey(EXTRA_CONTROL_ID)) {
                int id = getArguments().getInt(EXTRA_CONTROL_ID);
                return id;
            }
        }
        return null;
    }

    @Override
    public void onPageSelected(int i)
    {
        this.currentPageIndex = i;

        getView().findViewById(R.id.button_next_pager).setEnabled(true);
        getView().findViewById(R.id.button_prev_pager).setEnabled(true);

        if (this.currentPageIndex == 0)                                         getView().findViewById(R.id.button_prev_pager).setEnabled(false);
        else if (this.currentPageIndex == this.controle.compos.size() - 1)      getView().findViewById(R.id.button_next_pager).setEnabled(false);
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.button_next_pager:
                if (this.currentPageIndex < (this.controle.compos.size() - 1))
                {
                    this.currentPageIndex++;
                    mViewPager.setCurrentItem(this.currentPageIndex, true);
                }
                break;
            case R.id.button_prev_pager:
                if (this.currentPageIndex > 0)
                {
                    this.currentPageIndex--;
                    mViewPager.setCurrentItem(this.currentPageIndex, true);
                }
                break;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LogSncf.e("FragmentSole", "onDestroy");
        mViewPager.setAdapter(null);
        mPagerAdapter = null;
        mViewPager.invalidate();
    }

    @Override
    public void onPageScrolled(int i, float v, int i2) {
    }

    @Override
    public void onPageScrollStateChanged(int i) {
    }
}
