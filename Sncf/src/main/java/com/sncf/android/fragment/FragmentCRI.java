package com.sncf.android.fragment;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.sncf.android.R;
import com.sncf.android.base.BaseFragment;
import com.sncf.android.entity.AnomalyEntity;
import com.sncf.android.entity.CRIEntity;
import com.sncf.android.entity.ConclusionEntity;
import com.sncf.android.entity.FunctionEntity;
import com.sncf.android.entity.LevelFiveEntity;
import com.sncf.android.entity.LevelFourEntity;
import com.sncf.android.entity.LevelSixEntity;
import com.sncf.android.entity.LevelThreeEntity;
import com.sncf.android.entity.MicroDjEntity;
import com.sncf.android.entity.RepairEntity;
import com.sncf.android.entity.SetEntity;
import com.sncf.android.orm.DAO;
import com.sncf.android.orm.Entity;
import com.sncf.android.orm.criteria.ICriteria;
import com.sncf.android.orm.criteria.SQLCriteria;
import com.sncf.android.orm.criteria.SQLPredicate;
import com.sncf.android.orm.criteria.operators.EqualSqlPredicate;
import com.sncf.android.orm.dao.AnomalyDAO;
import com.sncf.android.orm.dao.CRIDAO;
import com.sncf.android.orm.dao.ConclusionDAO;
import com.sncf.android.orm.dao.FunctionDAO;
import com.sncf.android.orm.dao.LevelFiveDAO;
import com.sncf.android.orm.dao.LevelFourDAO;
import com.sncf.android.orm.dao.LevelSixDAO;
import com.sncf.android.orm.dao.LevelThreeDAO;
import com.sncf.android.orm.dao.MicroDjDAO;
import com.sncf.android.orm.dao.RepairDAO;
import com.sncf.android.orm.dao.SetDAO;
import com.sncf.android.provider.SncfContent;
import com.sncf.android.utils.DateHelper;

import java.util.Collection;
import java.util.Locale;

/**
 * Created by zzzwist on 30/12/13.
 */
public class FragmentCRI extends BaseFragment {

    private static final String             KEY_PARAM_SAVE_ID_CRI = "saveidcri";
    public static final String              EXTRA_CRI_ID = "extracriid";

    private int                             mCurrentIdCRI;

    @Override
    protected int getContentLayout() {
        return R.layout.fragment_cri;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (savedInstanceState == null)
            mCurrentIdCRI = this.getParameters();
        else
            mCurrentIdCRI = savedInstanceState.getInt(KEY_PARAM_SAVE_ID_CRI);

        CRIDAO      dao         = new CRIDAO(view.getContext());
        ICriteria   criteria    = new SQLCriteria();

        criteria.add(new SQLPredicate(SncfContent.t_gr_cri.Columns.ID.getName(), new EqualSqlPredicate(mCurrentIdCRI)));
        CRIEntity entity = (CRIEntity) dao.read(criteria).toArray()[0];
        initializeViewContent(entity);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(KEY_PARAM_SAVE_ID_CRI, mCurrentIdCRI);
    }

    public void updateView(CRIEntity entity) {
        mCurrentIdCRI = entity.getPrimaryKeyValue();
        initializeViewContent(entity);
    }

    private Integer getParameters() {
        if (getArguments() != null) {
            if (getArguments().containsKey(EXTRA_CRI_ID)) {
                int id = getArguments().getInt(EXTRA_CRI_ID);
                return id;
            }
        }
        return null;
    }

    private void initializeViewContent(CRIEntity entity) {
        ((TextView)getView().findViewById(R.id.textView_title_content)).setText(entity.label);

        String dateIntervention = DateHelper.formatDate(entity.interventionDate, "dd/MM/yyyy hh:mm", "dd/MM/yyyy", Locale.getDefault());
        String intervenant = entity.intervenerName;

        String numChantier = entity.numChantier != null ? Integer.toString(entity.numChantier) : "";
        String caisse = entity.compoId != null ? Integer.toString(entity.compoId) : "";

        FunctionEntity functionEntity = (FunctionEntity) createEntityByPredicate(
                new SQLPredicate(SncfContent.t_gr_fonctions.Columns.FONCTION.getName(),
                        new EqualSqlPredicate(entity.functionId)),
                new FunctionDAO(getActivity()));
        String function = functionEntity != null ? functionEntity.name : "";

        SetEntity setEntity = (SetEntity) createEntityByPredicate(
                new SQLPredicate(SncfContent.t_gr_ensembles.Columns.ENSEMBLE.getName(),
                        new EqualSqlPredicate(entity.setId)),
                new SetDAO(getActivity()));
        String set = setEntity != null ? setEntity.name : "";

        LevelThreeEntity levelThreeEntity = (LevelThreeEntity) createEntityByPredicate(
                new SQLPredicate(SncfContent.t_gr_niveau_3.Columns.NIVEAU_3.getName(),
                        new EqualSqlPredicate(entity.levelThreeId)),
                new LevelThreeDAO(getActivity()));
        String level3 = levelThreeEntity != null ? levelThreeEntity.name : "";

        LevelFourEntity levelFourEntity = (LevelFourEntity) createEntityByPredicate(
                new SQLPredicate(SncfContent.t_gr_niveau_4.Columns.NIVEAU_4.getName(),
                        new EqualSqlPredicate(entity.levelFourId)),
                new LevelFourDAO(getActivity()));
        String level4 = levelFourEntity != null ? levelFourEntity.name : "";

        LevelFiveEntity levelFiveEntity = (LevelFiveEntity) createEntityByPredicate(
                new SQLPredicate(SncfContent.t_gr_niveau_5.Columns.NIVEAU_5.getName(),
                        new EqualSqlPredicate(entity.levelFiveId)),
                new LevelFiveDAO(getActivity()));
        String level5 = levelFiveEntity != null ? levelFiveEntity.name : "";

        LevelSixEntity levelSixEntity = (LevelSixEntity) createEntityByPredicate(
                new SQLPredicate(SncfContent.t_gr_niveau_6.Columns.NIVEAU_6.getName(),
                        new EqualSqlPredicate(entity.levelSixId)),
                new LevelSixDAO(getActivity()));
        String level6 = levelSixEntity != null ? levelSixEntity.name : "";


        AnomalyEntity anomalyEntity = (AnomalyEntity) createEntityByPredicate(
                new SQLPredicate(SncfContent.t_gr_anomalies.Columns.ANOM_CODE.getName(),
                        new EqualSqlPredicate(entity.anomalyId)),
                new AnomalyDAO(getActivity()));
        String anomaly = anomalyEntity != null ? anomalyEntity.name : "";

        RepairEntity repairEntity = (RepairEntity) createEntityByPredicate(
                new SQLPredicate(SncfContent.t_gr_reparations.Columns.REPARATION_CODE.getName(),
                        new EqualSqlPredicate(entity.repairId)),
                new RepairDAO(getActivity()));
        String repair = repairEntity != null ? repairEntity.name : "";

        ConclusionEntity conclusionEntity = (ConclusionEntity) createEntityByPredicate(
                new SQLPredicate(SncfContent.t_gr_conclusions.Columns.ID_CONCLUSION.getName(),
                        new EqualSqlPredicate(entity.conclusionId)),
                new ConclusionDAO(getActivity()));
        String conclusion = conclusionEntity != null ? conclusionEntity.name : "";

        String bs = entity.BS;
        String dateSignalement = DateHelper.formatDate(entity.signalDate, "dd/MM/yyyy hh:mm", "dd/MM/yyyy", Locale.getDefault());
        String comment = entity.comment;

        this.createMdjView(entity);

        ((TextView) getView().findViewById(R.id.textView_cri_date_intervention_content)).setText(dateIntervention);
        ((TextView)getView().findViewById(R.id.textView_cri_intervenant_content)).setText(intervenant);
        ((TextView)getView().findViewById(R.id.textView_cri_uo_site_op_content)).setText(numChantier);
        ((TextView)getView().findViewById(R.id.textView_cri_caisse_content)).setText(caisse);
        ((TextView)getView().findViewById(R.id.textView_cri_function_content)).setText(function);
        ((TextView)getView().findViewById(R.id.textView_cri_set_content)).setText(set);
        ((TextView)getView().findViewById(R.id.textView_cri_level3_content)).setText(level3);
        ((TextView)getView().findViewById(R.id.textView_cri_level4_content)).setText(level4);
        ((TextView)getView().findViewById(R.id.textView_cri_level5_content)).setText(level5);
        ((TextView)getView().findViewById(R.id.textView_cri_level6_content)).setText(level6);
        ((TextView)getView().findViewById(R.id.textView_cri_anomaly_content)).setText(anomaly);
        ((TextView)getView().findViewById(R.id.textView_cri_repair_content)).setText(repair);
        ((TextView)getView().findViewById(R.id.textView_cri_conclusion_content)).setText(conclusion);
        ((TextView)getView().findViewById(R.id.textView_cri_bs_content)).setText(bs);
        ((TextView)getView().findViewById(R.id.textView_cri_signalement_content)).setText(dateSignalement);
        ((TextView)getView().findViewById(R.id.textView_cri_intervention_content)).setText(comment);
    }

    private Entity createEntityByPredicate(SQLPredicate predicate, DAO dao) {
        Entity entity = null;
        ICriteria criteria = new SQLCriteria();
        criteria.add(predicate);
        Collection<Entity> collection = dao.read(criteria);
        if (collection != null && collection.size() != 0) entity = (Entity) collection.toArray()[0];
        return entity;
    }

    private MicroDjEntity createMDJ(Integer id) {
        return ((MicroDjEntity) createEntityByPredicate(
                new SQLPredicate(SncfContent.t_gr_micro_dj.Columns.ID_MICRO_DJ.getName(),
                        new EqualSqlPredicate(id)),
                new MicroDjDAO(getActivity())));
    }

    private void createMdjView(CRIEntity entity) {
        StringBuilder builder = new StringBuilder();
        MicroDjEntity microDjEntity = null;
        builder.append((microDjEntity   = createMDJ(entity.MicroDj1))   != null ? microDjEntity.name + " \n" : "")
                .append((microDjEntity  = createMDJ(entity.MicroDj2))   != null ? microDjEntity.name + " \n" : "")
                .append((microDjEntity  = createMDJ(entity.MicroDj3))   != null ? microDjEntity.name + " \n" : "")
                .append((microDjEntity  = createMDJ(entity.MicroDj4))   != null ? microDjEntity.name + " \n" : "")
                .append((microDjEntity  = createMDJ(entity.MicroDj5))   != null ? microDjEntity.name + " \n" : "")
                .append((microDjEntity  = createMDJ(entity.MicroDj6))   != null ? microDjEntity.name + " \n" : "")
                .append((microDjEntity  = createMDJ(entity.MicroDj7))   != null ? microDjEntity.name + " \n" : "")
                .append((microDjEntity  = createMDJ(entity.MicroDj8))   != null ? microDjEntity.name + " \n" : "")
                .append((microDjEntity  = createMDJ(entity.MicroDj9))   != null ? microDjEntity.name + " \n" : "")
                .append((microDjEntity  = createMDJ(entity.MicroDj10))  != null ? microDjEntity.name + " \n" : "")
                .append((microDjEntity  = createMDJ(entity.MicroDj11))  != null ? microDjEntity.name + " \n" : "")
                .append((microDjEntity  = createMDJ(entity.MicroDj12))  != null ? microDjEntity.name + " \n" : "");

        ((TextView)getView().findViewById(R.id.textView_cri_microdj_content)).setText(builder.toString());
    }
}
