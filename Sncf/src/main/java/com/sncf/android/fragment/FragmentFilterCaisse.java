package com.sncf.android.fragment;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.sncf.android.R;
import com.sncf.android.base.AbstractCursorListFragment;
import com.sncf.android.base.AbstractListFragment;
import com.sncf.android.base.BaseListFragment;
import com.sncf.android.entity.CompoEntity;
import com.sncf.android.entity.RameEntity;
import com.sncf.android.orm.criteria.ICriteria;
import com.sncf.android.orm.criteria.SQLCriteria;
import com.sncf.android.orm.criteria.SQLPredicate;
import com.sncf.android.orm.criteria.operators.EqualSqlPredicate;
import com.sncf.android.orm.dao.CompoDAO;
import com.sncf.android.orm.dao.RameDAO;
import com.sncf.android.preference.PreferenceKey;
import com.sncf.android.preference.PreferencesManager;
import com.sncf.android.provider.SncfContent;
import com.sncf.android.utils.LogSncf;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * Created by zzzwist on 25/12/13.
 */
public class FragmentFilterCaisse extends AbstractCursorListFragment {

    public static final String              KEY_PARAM_SAVE_POSITION_CHECK = "savepositioncheck";
    public static final String              FRAGMENT_FILTER_CAISSE_TAG = "fragmentfilterwagontag";

    private int                             mCurrentPositionChecked = -1;

    @Override
    protected int getContentLayout() {
        return R.layout.fragment_filter_caisse;
    }

    @Override
    protected List<Integer> getLoaderCallbacksIds() {
        return null;
    }

    @Override
    protected int getLoaderCallbackId() {
        return R.id.id_loader_manager_filter_caisse;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        this.addTextViewActualRame(this.createActualRameEntity());
        ((TextView) view.findViewById(R.id.textView_title_header_filter)).setText(getString(R.string.title_header_filter_caisse));

        mAdapter = new ListCursorAdapter(getActivity(), R.layout.cell_list_filter, null, new String[]{}, new int[]{}, 0);
        mListView.setAdapter(mAdapter);

        if (savedInstanceState != null) {
            mCurrentPositionChecked = savedInstanceState.getInt(KEY_PARAM_SAVE_POSITION_CHECK);
            if (mCurrentPositionChecked != -1)
                mListView.setItemChecked(mCurrentPositionChecked, true);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(KEY_PARAM_SAVE_POSITION_CHECK, mCurrentPositionChecked);
    }

    public void updateListFilterCaisse() {
        RameEntity rameEntity = this.createActualRameEntity();
        ((TextView)getView().findViewById(R.id.id_textView_name_rame_for_caisse_filter)).setText("Rame : " + rameEntity.eab);
        getLoaderManager().restartLoader(R.id.id_loader_manager_filter_caisse, null, this);
    }

    private RameEntity createActualRameEntity() {
        int id_rame = PreferencesManager.getInstance(getActivity()).getInt(PreferenceKey.PREFERENCE_ID_RAME_FILTER, -1);
        RameDAO rameDAO = new RameDAO(getActivity());
        ICriteria criteria = new SQLCriteria();
        criteria.add(new SQLPredicate(SncfContent.t_sncf_rames.Columns.ID_RAME.getName(), new EqualSqlPredicate(id_rame)));
        Collection<RameEntity> collection = rameDAO.read(criteria);
        RameEntity rameEntity = (RameEntity) collection.toArray()[0];
        return rameEntity;
    }

    private void addTextViewActualRame(RameEntity rameEntity) {
        rameEntity = this.createActualRameEntity();
        TextView rame = new TextView(getActivity());
        rame.setId(R.id.id_textView_name_rame_for_caisse_filter);
        rame.setText("Rame : " + rameEntity.eab);
        rame.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
        ((LinearLayout)getView().findViewById(R.id.container_title_content_list_filter)).addView(rame);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        CompoDAO compoDAO = new CompoDAO(getActivity());
        ICriteria criteria = new SQLCriteria();
        criteria.add(new SQLPredicate(SncfContent.t_sncf_compos.Columns.ID.getName(), new EqualSqlPredicate(id)));
        Collection<CompoEntity> entityCollection = compoDAO.read(criteria);
        CompoEntity compoEntity = (CompoEntity) entityCollection.toArray()[0];

        PreferencesManager.getInstance(getActivity()).putInt(PreferenceKey.PREFERENCE_ID_CAISSE_FILTER, Integer.valueOf(compoEntity.name));

        mCurrentPositionChecked = position;
        mListView.setItemChecked(position, true);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LogSncf.e("FragmentFilterCaisse", "onDestroy");
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        String condition = null;
        int id_rame = PreferencesManager.getInstance(getActivity()).getInt(PreferenceKey.PREFERENCE_ID_RAME_FILTER, -1);
        if (id_rame != -1) {
            condition = SncfContent.t_sncf_compos.Columns.ID_RAME.getName() + "=" + id_rame;
        }
        //TODO
        //Replace by criteria

        switch (i) {
            case R.id.id_loader_manager_filter_caisse:
                return new CursorLoader(getActivity(), SncfContent.t_sncf_compos.CONTENT_URI, null, condition, null,
                        SncfContent.t_sncf_compos.Columns.NUM_ORDER.getName() + " ASC");
        }
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        mAdapter.changeCursor(cursor);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {
        mAdapter.changeCursor(null);
    }

    private class CaisseHolder extends BaseHolder {

        public TextView mName;
        public RadioButton mRadioButton;

        public CaisseHolder(View v) {
            mName = (TextView) v.findViewById(R.id.textView_filter_name);
            mRadioButton = (RadioButton) v.findViewById(R.id.radioButton_filter_check);
        }
    }

    @Override
    protected int getItemId(Cursor cursor, int position) {
        if (cursor.moveToPosition(position)) {
            CompoEntity compoEntity = new CompoEntity(cursor);
            return compoEntity.getPrimaryKeyValue();
        }
        return -1;
    }

    @Override
    protected BaseHolder initializeHolder(View view, int position) {
        CaisseHolder holder = null;
        if (view.getTag() == null) {
            holder = new CaisseHolder(view);
            view.setTag(holder);
        }
        else
            holder = (CaisseHolder) view.getTag();
        return holder;
    }

    @Override
    protected void configureView(BaseHolder baseHolder, View view, Cursor cursor, int position) {
        CaisseHolder holder = (CaisseHolder) baseHolder;
        if (cursor.moveToPosition(position)) {

            CompoEntity compoEntity = new CompoEntity(cursor);
            if (compoEntity != null) {
                int id_caisse = PreferencesManager.getInstance(getActivity()).getInt(PreferenceKey.PREFERENCE_ID_CAISSE_FILTER, -1);
                if (id_caisse != -1 && id_caisse == Integer.valueOf(compoEntity.name)) {
                    mListView.setItemChecked(position, true);
                }
                holder.mName.setText(compoEntity.name);
            }

            int p = mListView.getCheckedItemPosition();
            if (p == position)
                holder.mRadioButton.setChecked(true);
            else
                holder.mRadioButton.setChecked(false);
        }
    }
}
