package com.sncf.android.fragment;

import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.util.SparseArray;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.Toast;

import com.sncf.android.R;
import com.sncf.android.base.BaseFragment;
import com.sncf.android.data.CRIEdition;
import com.sncf.android.entity.LocationMicroDjEntity;
import com.sncf.android.orm.criteria.ICriteria;
import com.sncf.android.orm.criteria.SQLCriteria;
import com.sncf.android.orm.criteria.SQLPredicate;
import com.sncf.android.orm.criteria.operators.EqualSqlPredicate;
import com.sncf.android.orm.dao.LocationMicroDjDAO;
import com.sncf.android.provider.SncfContent;
import com.sncf.android.utils.LogSncf;

import java.util.Collection;

/**
 * Created by zzzwist on 18/01/14.
 */
public class FragmentAddCRIMicroDJ extends BaseFragment implements View.OnClickListener, FragmentDialogListMultiChoice.OnClickDialog {

    private static final Integer            MAX_ITEM = 12;

    private Integer                         mCurrentNumberItem = 0;
    private CRIEdition                      mEditionData;

    private SparseArray<String>             mSelectionBT;
    private SparseArray<String>             mSelectionBA;
    private SparseArray<String>             mSelectionCVS;
    private SparseArray<String>             mSelectionBM;
    private SparseArray<String>             mSelectionRM;
    private SparseArray<String>             mSelectionSubStairs;

    @Override
    protected int getContentLayout() {
        return R.layout.fragment_add_cri_microdj;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mEditionData                = CRIEdition.getInstance();

        mSelectionBT                = new SparseArray<String>();
        mSelectionBA                = new SparseArray<String>();
        mSelectionCVS               = new SparseArray<String>();
        mSelectionBM                = new SparseArray<String>();
        mSelectionRM                = new SparseArray<String>();
        mSelectionSubStairs         = new SparseArray<String>();

        ((TextView)view.findViewById(R.id.textView_title_sub_header)).setText(getString(R.string.title_header_add_cri_microdj));

        view.findViewById(R.id.button_add_armoire_ba).setOnClickListener(this);
        view.findViewById(R.id.button_add_armoire_bt).setOnClickListener(this);
        view.findViewById(R.id.button_add_cvs).setOnClickListener(this);
        view.findViewById(R.id.button_add_bm).setOnClickListener(this);
        view.findViewById(R.id.button_add_rm).setOnClickListener(this);
        view.findViewById(R.id.button_add_sub_stairs).setOnClickListener(this);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LogSncf.e("FragmentAddCRIMicroDJ", "onDestroy");
    }

    @Override
    public void onClick(View v) {
        String title                    = "";
        Cursor cursor                   = null;
        SparseArray<String> checkedItem = null;
        String columnName               = null;
        int idDialog                    = 0;

        switch (v.getId()) {
            case R.id.button_add_armoire_ba:
                title                   = getString(R.string.title_dialog_microdj_wardrobe_ba);
                cursor                  = this.createMDJCursorByLocMDJName(getString(R.string.column_value_microdj_wardrobe_ba));
                idDialog                = R.id.id_dialog_wardrobe_ba;
                checkedItem             = mSelectionBA;
                columnName              = SncfContent.t_gr_micro_dj.Columns.LIBELLE.getName();
                break;
            case R.id.button_add_armoire_bt:
                title                   = getString(R.string.title_dialog_microdj_wardrobe_bt);
                cursor                  = this.createMDJCursorByLocMDJName(getString(R.string.column_value_microdj_wardrobe_bt));
                idDialog                = R.id.id_dialog_wardrobe_bt;
                checkedItem             = mSelectionBT;
                columnName              = SncfContent.t_gr_micro_dj.Columns.LIBELLE.getName();
                break;
            case R.id.button_add_cvs:
                title                   = getString(R.string.title_dialog_microdj_cvs);
                cursor                  = this.createMDJCursorByLocMDJName(getString(R.string.column_value_microdj_cvs));
                idDialog                = R.id.id_dialog_cvs;
                checkedItem             = mSelectionCVS;
                columnName              = SncfContent.t_gr_micro_dj.Columns.LIBELLE.getName();
                break;
            case R.id.button_add_bm:
                title                   = getString(R.string.title_dialog_microdj_bm);
                cursor                  = this.createMDJCursorByLocMDJName(getString(R.string.column_value_microdj_bm));
                idDialog                = R.id.id_dialog_bm;
                checkedItem             = mSelectionBM;
                columnName              = SncfContent.t_gr_micro_dj.Columns.LIBELLE.getName();
                break;
            case R.id.button_add_rm:
                title                   = getString(R.string.title_dialog_microdj_rm);
                cursor                  = this.createMDJCursorByLocMDJName(getString(R.string.column_value_microdj_rm));
                idDialog                = R.id.id_dialog_rm;
                checkedItem             = mSelectionRM;
                columnName              = SncfContent.t_gr_micro_dj.Columns.LIBELLE.getName();
                break;
            case R.id.button_add_sub_stairs:
                title                   = getString(R.string.title_dialog_microdj_substair);
                cursor                  = this.createMDJCursorByLocMDJName(getString(R.string.column_value_microdj_substair));
                idDialog                = R.id.id_dialog_substairs;
                checkedItem             = mSelectionSubStairs;
                columnName              = SncfContent.t_gr_micro_dj.Columns.LIBELLE.getName();
                break;
        }
        Bundle params                   = new Bundle();
        params.putBoolean(FragmentDialogListMultiChoice.KEY_PARAM_IS_CANCELLABLE, true);
        params.putString(FragmentDialogListMultiChoice.KEY_PARAM_TITLE_DIALOG, title);
        params.putString(FragmentDialogListMultiChoice.KEY_PARAM_POSITIVE_BUTTON, getString(R.string.button_dialog_ok));
        params.putString(FragmentDialogListMultiChoice.KEY_PARAM_NEGATIVE_BUTTON, getString(R.string.button_dialog_cancel));
        params.putStringArray(FragmentDialogListMultiChoice.KEY_PARAM_COLUMNS_NAME_TO_SHOW, new String[]{columnName});
        params.putInt(FragmentDialogListMultiChoice.KEY_PARAM_DIALOG_ID, idDialog);
        params.putInt(FragmentDialogListMultiChoice.KEY_PARAM_MAX_ITEM_SELECTED, MAX_ITEM);
        params.putInt(FragmentDialogListMultiChoice.KEY_PARAM_CURRENT_NUMBER_ITEM_SELECTED, mCurrentNumberItem);
        FragmentDialogListMultiChoice dialogListMultiChoice = new FragmentDialogListMultiChoice(checkedItem, cursor, params, this);
        dialogListMultiChoice.show(getFragmentManager(), "tag");
    }

    private Cursor createMDJCursorByLocMDJName(String name) {
        ICriteria               criteria                    = new SQLCriteria();
        LocationMicroDjDAO      locationMicroDjDAO          = new LocationMicroDjDAO(getActivity());

        criteria.add(new SQLPredicate(SncfContent.t_gr_localisations_mdj.Columns.LOCALISATION_MDJ.getName(), new EqualSqlPredicate(name)));
        Collection<LocationMicroDjEntity> collection        = locationMicroDjDAO.read(criteria);

        if (collection == null || collection.size() == 0) return null;
        LocationMicroDjEntity locationMicroDjEntity         = (LocationMicroDjEntity) collection.toArray()[0];

        Cursor cursor                                       = getActivity().getContentResolver().query(SncfContent.t_gr_micro_dj.CONTENT_URI, null,
                SncfContent.t_gr_micro_dj.Columns.ID_LOCALISATION.getName() + "=?",
                new String[]{Integer.toString(locationMicroDjEntity.idLocationMicroDj)}, null);
        return cursor;
    }

    private void fillTextViewSelection(SparseArray<String> array, int idTextView) {

        TextView container                                  = (TextView) getView().findViewById(idTextView);
        StringBuilder builder                               = new StringBuilder();

        for (int idx = 0; idx < array.size(); idx++) {
            int key = array.keyAt(idx);
            builder.append(array.get(key));
            builder.append('\n');
        }
        container.setText(builder.toString());
    }

    @Override
    public void clickPositiveButton(DialogInterface dialog, int dialogId, int which, SparseArray<String> array) {
        int id                                      = 0;
        switch (dialogId) {
            case R.id.id_dialog_wardrobe_ba:
                id                                  = R.id.textView_select_item_armoire_ba;
                mSelectionBA                        = array;
                break;
            case R.id.id_dialog_wardrobe_bt:
                id                                  = R.id.textView_select_item_armoire_bt;
                mSelectionBT                        = array;
                break;
            case R.id.id_dialog_cvs:
                id                                  = R.id.textView_select_item_cvs;
                mSelectionCVS                       = array;
                break;
            case R.id.id_dialog_bm:
                id                                  = R.id.textView_select_item_bm;
                mSelectionBM                        = array;
                break;
            case R.id.id_dialog_rm:
                id                                  = R.id.textView_select_item_rm;
                mSelectionRM                        = array;
                break;
            case R.id.id_dialog_substairs:
                id                                  = R.id.textView_select_item_substairs;
                mSelectionSubStairs                 = array;
                break;
        }
        mEditionData.createListMDJ(mSelectionBA, mSelectionBT, mSelectionCVS, mSelectionBM, mSelectionRM, mSelectionSubStairs);
        fillTextViewSelection(array, id);
    }
    @Override
    public void clickNegativeButton(DialogInterface dialog, int dialogId, int which) {
        dialog.dismiss();
    }
    @Override
    public void clickNeutralButton(DialogInterface dialog, int dialogId, int which) {}
    @Override
    public void onItemListClicked(int dialogId, AdapterView<?> parent, View view, int position, long id,
                                  Boolean isChecked, Cursor cursorPosition) {

        LogSncf.e("FragmentAddCRIMicroDJ", "Id item clicked = " + id);

        if (isChecked != null) {
            if (isChecked)
                mCurrentNumberItem++;
            else
                mCurrentNumberItem--;
        }
        else {
            Toast.makeText(getActivity(), getString(R.string.message_selection_max_microdj), Toast.LENGTH_LONG).show();
        }

    }
}
