package com.sncf.android.importExport;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import com.sncf.android.R;
import com.sncf.android.builder.Director;
import com.sncf.android.builder.tablebuilder.AnomaliesBuilder;
import com.sncf.android.builder.tablebuilder.CRIBuilder;
import com.sncf.android.builder.tablebuilder.ComposBuilder;
import com.sncf.android.builder.tablebuilder.ConclusionsBuilder;
import com.sncf.android.builder.tablebuilder.FunctionsBuilder;
import com.sncf.android.builder.tablebuilder.InterventionsBuilder;
import com.sncf.android.builder.tablebuilder.LevelFiveBuilder;
import com.sncf.android.builder.tablebuilder.LevelFourBuilder;
import com.sncf.android.builder.tablebuilder.LevelSixBuilder;
import com.sncf.android.builder.tablebuilder.LevelThreeBuilder;
import com.sncf.android.builder.tablebuilder.LinkSeriesBuilder;
import com.sncf.android.builder.tablebuilder.LinkSpecialtiesBuilder;
import com.sncf.android.builder.tablebuilder.LocationMicroDjBuilder;
import com.sncf.android.builder.tablebuilder.MicroDjBuilder;
import com.sncf.android.builder.tablebuilder.RamesBuilder;
import com.sncf.android.builder.tablebuilder.RepairBuilder;
import com.sncf.android.builder.tablebuilder.RestrictionBuilder;
import com.sncf.android.builder.tablebuilder.STFBuilder;
import com.sncf.android.builder.tablebuilder.SemelleBogieBuilder;
import com.sncf.android.builder.tablebuilder.SemelleControlesBuilder;
import com.sncf.android.builder.tablebuilder.SemelleDataBuilder;
import com.sncf.android.builder.tablebuilder.SeriesBuilder;
import com.sncf.android.builder.tablebuilder.SetBuilder;
import com.sncf.android.builder.tablebuilder.SiteBuilder;
import com.sncf.android.builder.tablebuilder.SpecialtiesBuilder;
import com.sncf.android.builder.tablebuilder.SpecificiteBuilder;
import com.sncf.android.builder.tablebuilder.SubSeriesBuilder;
import com.sncf.android.builder.tablebuilder.UOBuilder;
import com.sncf.android.entity.ParentModelEntity;
import com.sncf.android.entity.SeriesEntity;
import com.sncf.android.entity.SubSeriesEntity;
import com.sncf.android.preference.PreferencesManager;
import com.sncf.android.provider.SncfContent;
import com.sncf.android.utils.FileHelper;
import com.sncf.android.utils.JSONParser;
import com.sncf.android.utils.LogSncf;

import java.io.File;

/**
 * Created by favre on 31/01/2014.
 */


public class Loader extends AsyncTask<Void, Void, Boolean>
{
    ///////////////////////////////////////
    /// Interface Listener

    public interface OnDataLoaderListener
    {
        public void onSuccess();
        public void onFailure();

    }

    ///////////////////////////////////////
    /// Properties

    protected OnDataLoaderListener  listener;
    protected Context               context;

    ///////////////////////////////////////
    /// Methods & Constructors

    public Loader(Context context, OnDataLoaderListener listener)
    {
        this.context    = context;
        this.listener   = listener;
    }


    public void executeAsync()
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            Log.e("AbstractRequestManager", "executeAsync ");
            executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
        else {
            Log.e("AbstractRequestManager", "executeAsync ");
            execute();
        }
    }

    @Override
    protected Boolean doInBackground(Void... voids)
    {
        try
        {
            this.dropTables();

            File                importFile  = FileHelper.getDocumentStorage(context.getString(R.string.filename));
            String              content     = FileHelper.readFile(importFile);

            Log.e("Loader", content);

            ParentModelEntity   entities    = JSONParser.deserializeData(content, ParentModelEntity.class);

            /*
            *   Director.addBuilder();
            *   ....
            *
            *   Director.build();
            * */

            Director director = new Director();

            director.addBuilder(new AnomaliesBuilder(this.context, entities.t_gr_anomalies));
            director.addBuilder(new ConclusionsBuilder(this.context, entities.t_gr_conclusions));
            director.addBuilder(new CRIBuilder(this.context, entities.t_gr_cris));
            director.addBuilder(new FunctionsBuilder(this.context, entities.t_gr_functions));
            director.addBuilder(new InterventionsBuilder(this.context, entities.t_gr_interventions));
            director.addBuilder(new LevelThreeBuilder(this.context, entities.t_gr_level_3));
            director.addBuilder(new LevelFourBuilder(this.context, entities.t_gr_level_4));
            director.addBuilder(new LevelFiveBuilder(this.context, entities.t_gr_level_5));
            director.addBuilder(new LevelSixBuilder(this.context, entities.t_gr_level_6));
            director.addBuilder(new LocationMicroDjBuilder(this.context, entities.t_gr_locations_microdjs));
            director.addBuilder(new MicroDjBuilder(this.context, entities.t_gr_microdjs));
            director.addBuilder(new RepairBuilder(this.context, entities.t_gr_repairs));
            director.addBuilder(new SetBuilder(this.context, entities.t_gr_sets));
            director.addBuilder(new SpecialtiesBuilder(this.context, entities.t_gr_specialties));
            director.addBuilder(new InterventionsBuilder(this.context, entities.t_gr_interventions));
            director.addBuilder(new ComposBuilder(this.context, entities.t_sncf_compos));
            director.addBuilder(new LinkSeriesBuilder(this.context, entities.t_sncf_link_series));
            director.addBuilder(new RamesBuilder(this.context, entities.t_sncf_rames));
            director.addBuilder(new LinkSpecialtiesBuilder(this.context, entities.t_sncf_link_specialties));
            director.addBuilder(new UOBuilder(this.context, entities.t_sncf_uo));
            director.addBuilder(new STFBuilder(this.context, entities.t_sncf_stf));

            director.addBuilder(new SeriesBuilder(this.context, entities.t_sncf_series));

            for (SeriesEntity seriesEntity : entities.t_sncf_series)  director.addBuilder(new SubSeriesBuilder(this.context, seriesEntity.sub_series));

            director.addBuilder(new SiteBuilder(this.context, entities.t_sncf_sites));
            director.addBuilder(new SemelleBogieBuilder(this.context, entities.t_sem_bogie));
            director.addBuilder(new SemelleControlesBuilder(this.context, entities.t_sem_controles));
            director.addBuilder(new SemelleDataBuilder(this.context, entities.t_sem_data));
            director.addBuilder(new RestrictionBuilder(this.context, entities.t_gr_restrictions));
            director.addBuilder(new SpecificiteBuilder(this.context, entities.t_gr_specificities));
            director.build();

            return true;
        }
        catch(Exception e) { LogSncf.e("Loader", e.toString()); e.printStackTrace(); return false; }
    }

    private void dropTables() {

        PreferencesManager.getInstance(context).clear();

        context.getContentResolver().delete(SncfContent.t_gr_anomalies.CONTENT_URI, null, null);
        context.getContentResolver().delete(SncfContent.t_gr_conclusions.CONTENT_URI, null, null);
        context.getContentResolver().delete(SncfContent.t_gr_cri.CONTENT_URI, null, null);
        context.getContentResolver().delete(SncfContent.t_gr_fonctions.CONTENT_URI, null, null);
        context.getContentResolver().delete(SncfContent.t_gr_interventions.CONTENT_URI, null, null);
        context.getContentResolver().delete(SncfContent.t_gr_niveau_3.CONTENT_URI, null, null);
        context.getContentResolver().delete(SncfContent.t_gr_niveau_4.CONTENT_URI, null, null);
        context.getContentResolver().delete(SncfContent.t_gr_niveau_5.CONTENT_URI, null, null);
        context.getContentResolver().delete(SncfContent.t_gr_niveau_6.CONTENT_URI, null, null);
        context.getContentResolver().delete(SncfContent.t_gr_localisations_mdj.CONTENT_URI, null, null);
        context.getContentResolver().delete(SncfContent.t_gr_micro_dj.CONTENT_URI, null, null);
        context.getContentResolver().delete(SncfContent.t_gr_reparations.CONTENT_URI, null, null);
        context.getContentResolver().delete(SncfContent.t_gr_ensembles.CONTENT_URI, null, null);
        context.getContentResolver().delete(SncfContent.t_gr_specialities.CONTENT_URI, null, null);
        context.getContentResolver().delete(SncfContent.t_gr_interventions.CONTENT_URI, null, null);
        context.getContentResolver().delete(SncfContent.t_sncf_compos.CONTENT_URI, null, null);
        context.getContentResolver().delete(SncfContent.t_sncf_link_series.CONTENT_URI, null, null);
        context.getContentResolver().delete(SncfContent.t_sncf_series.CONTENT_URI, null, null);
        context.getContentResolver().delete(SncfContent.t_sncf_rames.CONTENT_URI, null, null);
        context.getContentResolver().delete(SncfContent.t_sncf_link_specialities.CONTENT_URI, null, null);
        context.getContentResolver().delete(SncfContent.t_sncf_uo.CONTENT_URI, null, null);
        context.getContentResolver().delete(SncfContent.t_sncf_stf.CONTENT_URI, null, null);
        context.getContentResolver().delete(SncfContent.t_sncf_sites.CONTENT_URI, null, null);
        context.getContentResolver().delete(SncfContent.t_sem_bogie.CONTENT_URI, null, null);
        context.getContentResolver().delete(SncfContent.t_sem_controles.CONTENT_URI, null, null);
        context.getContentResolver().delete(SncfContent.t_sem_data.CONTENT_URI, null, null);
        context.getContentResolver().delete(SncfContent.t_gr_restriction_mvc.CONTENT_URI, null, null);
        context.getContentResolver().delete(SncfContent.t_gr_specificite.CONTENT_URI, null, null);
        context.getContentResolver().delete(SncfContent.t_sncf_subseries.CONTENT_URI, null, null);
    }

    @Override
    protected void onPostExecute(Boolean successful) {
        super.onPostExecute(successful);

        if (this.listener != null)
        {
            if (successful) this.listener.onSuccess();
            else            this.listener.onFailure();
        }

        this.context    = null;
        this.listener   = null;
    }

    public void setOnDataLoaderListener(OnDataLoaderListener listener) { this.listener = listener; }
}
