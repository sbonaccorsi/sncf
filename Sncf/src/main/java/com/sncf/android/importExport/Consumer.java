package com.sncf.android.importExport;

import android.util.Log;

import java.util.concurrent.BlockingQueue;

/**
 * Created by favre on 28/12/13.
 */
public class Consumer<T> implements Runnable
{
    ////////////////////////////////////
    /// Interface Listener

    public interface OnConsumerListener<T>
    {
        public void consume(T product);

        public void onConsumeFailed();
        public void onConsumeSucceed();
    }

    ////////////////////////////////////
    /// Properties

    private final   BlockingQueue<T>        sharedQueue;
    protected       OnConsumerListener<T>   delegate;

    ////////////////////////////////////
    /// Methods & Constructors

    public Consumer(BlockingQueue<T> sharedQueue)           { this.sharedQueue  = sharedQueue;  }
    public void setConsume(OnConsumerListener<T> delegate)  { this.delegate     = delegate;     }

    @Override
    public void run()
    {
        while (true)
            try                             { consume(); Thread.sleep(50);                                          }
            catch (InterruptedException ex) { Log.e("Consumer", "[run : InterruptedException] " + ex.toString());   }
    }

    private void consume() throws InterruptedException
    {
        while (sharedQueue.isEmpty())
        {
            this.delegate.onConsumeSucceed();
            synchronized (sharedQueue) { sharedQueue.wait(); }
        }

        synchronized (sharedQueue)
        {
            try
            {
                this.delegate.consume(sharedQueue.take());
                sharedQueue.notifyAll();
            }
            catch(Exception e) { this.delegate.onConsumeFailed(); }
        }
    }
}
