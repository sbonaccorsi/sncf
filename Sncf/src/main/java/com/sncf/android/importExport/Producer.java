package com.sncf.android.importExport;

import android.util.Log;

import java.util.concurrent.BlockingQueue;

/**
 * Created by favre on 28/12/13.
 */
public class Producer<T> implements Runnable
{
    ////////////////////////////////////
    /// Interface Listener

    public interface OnProducerListener<T>
    {
        public T produce();

        public void onProduceFailed();
        public void onProducesucceed();
    }

    ////////////////////////////////////
    /// Properties

    private final   BlockingQueue<T>        sharedQueue;
    protected       OnProducerListener<T>   delegate;

    ////////////////////////////////////
    /// Methods & Constructors

    public Producer(BlockingQueue<T> sharedQueue)           { this.sharedQueue  = sharedQueue;  }
    public void setProduce(OnProducerListener<T> delegate)  { this.delegate     = delegate;     }

    @Override
    public void run()
    {
        while (true)
            try                             { this.produce();Thread.sleep(50);  }
            catch (InterruptedException ex) { Log.e("Producer", ex.toString()); }
    }

    public void produce() throws InterruptedException
    {
        synchronized (sharedQueue)
        {
            try
            {
                sharedQueue.put(this.delegate.produce());
                sharedQueue.notifyAll();

                this.delegate.onProducesucceed();
            }
            catch (Exception e) {Log.e("Producer", e.toString()); this.delegate.onProduceFailed(); }
        }
    }
}
