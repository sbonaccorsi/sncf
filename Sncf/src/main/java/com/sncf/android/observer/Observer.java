package com.sncf.android.observer;

/**
 * Created by sbonaccorsi on 22/02/2014.
 */
public interface Observer {

    public void update(Observable observable);
}
