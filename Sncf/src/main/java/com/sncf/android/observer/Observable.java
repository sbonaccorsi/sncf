package com.sncf.android.observer;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sbonaccorsi on 22/02/2014.
 */
public abstract class Observable {

    protected List<Observer>            mListObserver = new ArrayList<Observer>();

    public void addObserver(Observer observer) {
        mListObserver.add(observer);
    }

    protected void notifyObserver() {
        for (Observer observer : mListObserver)         observer.update(this);
    }

    public void clearObservers() {
        mListObserver.clear();
    }

    public boolean haveObservers() {
        return !mListObserver.isEmpty();
    }
}
