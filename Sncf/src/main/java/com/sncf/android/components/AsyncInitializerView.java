package com.sncf.android.components;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;

/**
 * Created by zzzwist on 04/02/14.
 */
public class AsyncInitializerView<Params> extends AsyncTask<Params, Void, Object> {

    public interface OnInitializerViewListener<GenericData, Params, GenericView extends View> {

        public GenericData doBackgroundWork(Params... params);

        public void initView(GenericView view, GenericData data);
    }

    private View mView;
    private int                                     mIdLayout;
    private OnInitializerViewListener               mListener;
    private Context mContext;

    public AsyncInitializerView(Context context) {
        mContext = context;
    }

    public void executeInitializerView(View view, OnInitializerViewListener listener, Params... params) {
        mView = view;
        mListener = listener;
        this.exec(params);
    }

    public void executeInitializerView(int id_layout, OnInitializerViewListener listener, Params... params) {
        mIdLayout = id_layout;
        mListener = listener;
        this.exec(params);
    }

    private void exec(Params... params) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, params);
        else execute(params);
    }

    @Override
    protected Object doInBackground(Params... params) {
        if (mListener != null) {
            if (mView == null)
                mView = LayoutInflater.from(mContext).inflate(mIdLayout, null);
            return mListener.doBackgroundWork(params);
        }
        return null;
    }

    @Override
    protected void onPostExecute(Object obj) {
        super.onPostExecute(obj);
        if (mListener != null)
            mListener.initView(mView, obj);
        mView = null;
        mListener = null;
    }
}
