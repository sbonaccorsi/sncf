package com.sncf.android.preference;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by zzzwist on 15/01/14.
 */
public class PreferencesManager {

    private static final String						NAME_SHARED_PREFERENCES = "SncfPreferences";

    private static PreferencesManager 				mInstance;
    private Context                                 mContext;
    private SharedPreferences                       mSharedPrefs;

    private PreferencesManager(Context context) {
        mContext = context;
        mSharedPrefs = mContext.getSharedPreferences(NAME_SHARED_PREFERENCES, Context.MODE_PRIVATE);
    }

    public static PreferencesManager getInstance(Context applicationContext) {

        if (mInstance == null) {
            mInstance = new PreferencesManager(applicationContext);
        }
        return mInstance;
    }

    public SharedPreferences getSharedPreferences() {
        return mSharedPrefs;
    }

    public void putInt(String key, int value) {
        mSharedPrefs.edit().putInt(key, value).commit();
    }

    public void putString(String key, String value) {
        mSharedPrefs.edit().putString(key, value).commit();
    }

    public void putBoolean(String key, Boolean value) {
        mSharedPrefs.edit().putBoolean(key, value).commit();
    }

    public void putFloat(String key, float value) {
        mSharedPrefs.edit().putFloat(key, value).commit();
    }

    public int getInt(String key, int defValue) {
        return mSharedPrefs.getInt(key, defValue);
    }

    public String getString(String key, String defValue) {
        return mSharedPrefs.getString(key, defValue);
    }

    public boolean getBoolean(String key, boolean defValue) {
        return mSharedPrefs.getBoolean(key, defValue);
    }

    public float getFloat(String key, float defValue) {
        return mSharedPrefs.getFloat(key, defValue);
    }

    public void clear() {
        mSharedPrefs.edit().clear().commit();
    }
}
