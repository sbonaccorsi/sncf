package com.sncf.android.preference;

/**
 * Created by zzzwist on 15/01/14.
 */
public interface PreferenceKey {
    public static final String          PREFERENCE_ID_RAME_FILTER = "preference_id_rame_filter";
    public static final String          PREFERENCE_ID_CAISSE_FILTER = "preference_id_caisse_filter";
    public static final String          PREFERENCE_POSITION_RAME_SELECTED = "preference_position_rame_selected";
}
