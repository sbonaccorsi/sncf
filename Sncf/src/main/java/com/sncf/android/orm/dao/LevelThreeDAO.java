package com.sncf.android.orm.dao;

import android.content.Context;
import android.net.Uri;

import com.sncf.android.entity.LevelThreeEntity;
import com.sncf.android.orm.DAO;
import com.sncf.android.provider.SncfContent;

/**
 * Created by favre on 23/01/2014.
 */
public class LevelThreeDAO extends DAO<LevelThreeEntity>
{
    public LevelThreeDAO(Context context) {
        super(context, SncfContent.t_gr_niveau_3.CONTENT_URI, LevelThreeEntity.class.getName());
    }
}
