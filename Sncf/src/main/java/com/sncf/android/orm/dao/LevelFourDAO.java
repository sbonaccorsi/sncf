package com.sncf.android.orm.dao;

import android.content.Context;
import android.net.Uri;

import com.sncf.android.entity.LevelFourEntity;
import com.sncf.android.orm.DAO;
import com.sncf.android.provider.SncfContent;

/**
 * Created by favre on 23/01/2014.
 */
public class LevelFourDAO extends DAO<LevelFourEntity>
{
    public LevelFourDAO(Context context) {
        super(context, SncfContent.t_gr_niveau_4.CONTENT_URI, LevelFourEntity.class.getName());
    }
}
