package com.sncf.android.orm.criteria.aggregators;

import com.sncf.android.orm.criteria.IPredicate;


public interface IAggregator extends IPredicate
{
	public  boolean		add(IPredicate predicate);
	public	boolean		remove(IPredicate predicate);
	public	IPredicate	remove(int index);
    public  int         size();
    public  void        removeAll();
    public  boolean     contain(IPredicate predicate);
	
	public 	String		toString();
}
