package com.sncf.android.orm.criteria.operators;

public class LowerSqlPredicate extends SQLOperator
{

	public LowerSqlPredicate(Object value) 	{ super("< ?", value); }

}
