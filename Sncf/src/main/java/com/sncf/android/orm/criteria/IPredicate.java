package com.sncf.android.orm.criteria;


import com.sncf.android.orm.criteria.operators.IOperator;

/**
 * The Predicate interface is a standard interface that applications can implement to define the filter they wish to apply to an object.
 * @author favre
 *
 */
public interface IPredicate<T> 
{ 
	/**<pre>
	 * @param obj	Object to evaluate.
	 * @return	<b><code>true</code></b>	if success.
	 * 	<b><code>false</code></b>	otherwise.
	 * </pre>
	 */
	public boolean		evaluate(T obj);	
	public	String		toString();
}
