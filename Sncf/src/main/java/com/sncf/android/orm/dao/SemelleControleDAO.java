package com.sncf.android.orm.dao;

import android.content.Context;
import android.net.Uri;

import com.sncf.android.entity.SemelleControlesEntity;
import com.sncf.android.orm.DAO;
import com.sncf.android.provider.SncfContent;

/**
 * Created by favre on 24/01/2014.
 */
public class SemelleControleDAO extends DAO<SemelleControlesEntity>
{
    public SemelleControleDAO(Context context) {
        super(context, SncfContent.t_sem_controles.CONTENT_URI, SemelleControlesEntity.class.getName());
    }
}
