package com.sncf.android.orm.dao;

import android.content.Context;
import android.net.Uri;

import com.sncf.android.entity.SemelleBogieEntity;
import com.sncf.android.orm.DAO;
import com.sncf.android.provider.SncfContent;

/**
 * Created by favre on 24/01/2014.
 */
public class SemelleBogieDAO extends DAO<SemelleBogieEntity>
{
    public SemelleBogieDAO(Context context) {
        super(context, SncfContent.t_sem_bogie.CONTENT_URI, SemelleBogieEntity.class.getName());
    }
}
