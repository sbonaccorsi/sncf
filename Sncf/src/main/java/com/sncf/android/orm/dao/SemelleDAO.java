package com.sncf.android.orm.dao;

import android.content.Context;

import com.sncf.android.entity.SemelleEntity;
import com.sncf.android.orm.DAO;
import com.sncf.android.provider.SncfContent;

/**
 * Created by favre on 17/01/2014.
 */
public class SemelleDAO extends DAO<SemelleEntity>
{

    public SemelleDAO(Context context) {
        super(context, SncfContent.SemelleTable.CONTENT_URI, SemelleEntity.class.getName());
    }
}
