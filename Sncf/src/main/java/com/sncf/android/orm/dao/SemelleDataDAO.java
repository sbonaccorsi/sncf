package com.sncf.android.orm.dao;

import android.content.Context;
import android.net.Uri;

import com.sncf.android.entity.SemelleDataEntity;
import com.sncf.android.orm.DAO;
import com.sncf.android.provider.SncfContent;

/**
 * Created by favre on 24/01/2014.
 */
public class SemelleDataDAO extends DAO<SemelleDataEntity>
{
    public SemelleDataDAO(Context context) {
        super(context, SncfContent.t_sem_data.CONTENT_URI, SemelleDataEntity.class.getName());
    }
}
