package com.sncf.android.orm.dao;

import android.content.Context;
import android.net.Uri;

import com.sncf.android.entity.FunctionEntity;
import com.sncf.android.orm.DAO;
import com.sncf.android.provider.SncfContent;

/**
 * Created by favre on 23/01/2014.
 */
public class FunctionDAO extends DAO<FunctionEntity>
{
    public FunctionDAO(Context context) {
        super(context, SncfContent.t_gr_fonctions.CONTENT_URI, FunctionEntity.class.getName());
    }
}
