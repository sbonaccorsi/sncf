package com.sncf.android.orm.dao;

import android.content.Context;
import android.net.Uri;

import com.sncf.android.entity.RestrictionEntity;
import com.sncf.android.orm.DAO;
import com.sncf.android.provider.SncfContent;

/**
 * Created by favre on 23/01/2014.
 */
public class RestrictionDAO extends DAO<RestrictionEntity>
{
    public RestrictionDAO(Context context) {
        super(context, SncfContent.t_gr_restriction_mvc.CONTENT_URI, RestrictionEntity.class.getName());
    }
}
