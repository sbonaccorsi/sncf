package com.sncf.android.orm.dao;

import android.content.Context;
import android.net.Uri;

import com.sncf.android.entity.SiteEntity;
import com.sncf.android.orm.DAO;
import com.sncf.android.provider.SncfContent;

/**
 * Created by favre on 23/01/2014.
 */
public class SiteDAO extends DAO<SiteEntity>
{
    public SiteDAO(Context context) {
        super(context, SncfContent.t_sncf_sites.CONTENT_URI, SiteEntity.class.getName());
    }
}
