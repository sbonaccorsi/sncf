package com.sncf.android.orm.criteria.operators;

public interface IOperator
{
	public	String	toString();
}