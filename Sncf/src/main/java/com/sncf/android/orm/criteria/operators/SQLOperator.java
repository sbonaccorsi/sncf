package com.sncf.android.orm.criteria.operators;


public abstract class SQLOperator implements IOperator
{
	private	String		sql;
	private	Object[]	values;
	
	/////////////////////////////////////////////////
	///////////////////	CONSTRUCTORS
	
	public	SQLOperator(String sql, Object... values)				{createStructure(sql, values);}
	
	public	void	createStructure(String sql, Object... values)
	{
		this.setSql(sql);
		this.setValues(values);
	}
	
	////////////////////////////////////////////////////
	//////////////////	GETTERS & SETTERS

	public 	String		getSql() 					{return sql;			}	
	public 	Object[]	getValues() 				{return values;			}
	
	public 	void 		setSql(String sql) 			{this.sql = sql;		}		
	public 	void 		setValues(Object[] values) 	{this.values = values;	}

	/////////////////////////////////////////////////////
	/////////////////	METHODES
	
	public	String	toString()
	{
		StringBuilder query 	=	new StringBuilder();
		
		query.append(sql);

		for (Object value : values)
		{
			int index	=	query.indexOf("?");
			
			query.replace(index, index + 1, "");
			query.insert(index, value.toString());
		}
		
		return query.toString();
	}
	
}
