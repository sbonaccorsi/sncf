package com.sncf.android.orm.dao;

import android.content.Context;
import android.net.Uri;

import com.sncf.android.entity.LevelFiveEntity;
import com.sncf.android.entity.LevelSixEntity;
import com.sncf.android.orm.DAO;
import com.sncf.android.provider.SncfContent;

/**
 * Created by favre on 23/01/2014.
 */
public class LevelSixDAO extends DAO<LevelSixEntity>
{
    public LevelSixDAO(Context context) {
        super(context, SncfContent.t_gr_niveau_6.CONTENT_URI, LevelSixEntity.class.getName());
    }
}
