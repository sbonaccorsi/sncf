package com.sncf.android.orm;

import android.content.ContentProviderOperation;
import android.content.ContentValues;
import android.content.Context;
import android.content.OperationApplicationException;
import android.database.Cursor;
import android.net.Uri;
import android.os.RemoteException;
import android.util.Log;

import com.sncf.android.provider.SncfProvider;

import java.util.ArrayList;

/**
 * Created by favre on 17/01/2014.
 */
public class DAL {

    public static boolean create(Context context, Uri uri, ContentValues values)
    {
        //throw new UnsupportedOperationException();

        try                 { context.getContentResolver().insert(uri, values);                 return true;    }
        catch(Exception e)  { Log.e("DAL", e.toString());                                       return false;   }
    }

    public static boolean applyBatch(Context context, ArrayList<ContentProviderOperation> operations)
    {

        try { context.getContentResolver().applyBatch(SncfProvider.AUTHORITY, operations);      return true;    }
        catch (RemoteException e)               { Log.e("DAL", e.toString());                   return false;   }
        catch (OperationApplicationException e) { Log.e("DAL", e.toString());                   return false;   }
    }

    public static Cursor read(Context context, Uri uri, String criteria)
    {
        //throw new UnsupportedOperationException();

        try                 { return context.getContentResolver().query(uri, null, criteria, null, null);       }
        catch (Exception e) { Log.e("DAL", e.toString()); return null;                                          }
    }

    public static boolean update(Context context, Uri uri, ContentValues values, String criteria)
    {
        //throw new UnsupportedOperationException();

        try                 { context.getContentResolver().update(uri, values, criteria, null); return true;    }
        catch (Exception e) { Log.e("DAL", e.toString());                                       return false;   }
    }

    public static boolean delete(Context context, Uri uri, String criteria)
    {
        //throw new UnsupportedOperationException();

        try                 { context.getContentResolver().delete(uri, criteria, null);         return true;    }
        catch (Exception e) { Log.e("DAL", e.toString());                                       return false;   }
    }

}
