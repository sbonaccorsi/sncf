package com.sncf.android.orm.criteria.operators;

import com.sncf.android.utils.ValueHelper;


public class LikeSqlPredicate extends SQLOperator
{

	public LikeSqlPredicate(Object value) 	{ super("like ?", ValueHelper.betweenDoubleQuote(value.toString())); }

}
