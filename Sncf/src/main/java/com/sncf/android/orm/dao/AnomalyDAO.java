package com.sncf.android.orm.dao;

import android.content.Context;

import com.sncf.android.entity.AnomalyEntity;
import com.sncf.android.orm.DAO;
import com.sncf.android.provider.SncfContent;

/**
 * Created by favre on 23/01/2014.
 */
public class AnomalyDAO extends DAO<AnomalyEntity>
{
    public AnomalyDAO(Context context) {
        super(context, SncfContent.t_gr_anomalies.CONTENT_URI, AnomalyEntity.class.getName());
    }
}
