package com.sncf.android.orm.criteria;

import java.util.ArrayList;

public class SQLCriteria implements ICriteria 
{
	private ArrayList<IPredicate>	predicates;
	
	////////////////////////////////////////////////////////
	/////////	Constructors
	
	public SQLCriteria()			{createStructure();}
	
	public void createStructure()	{predicates = new ArrayList<IPredicate>();}

	/////////////////////////////////////////////////////////
	/////////	Methodes
	
	public  boolean		add(IPredicate predicate)		{return this.predicates.add(predicate);		}
	public 	boolean		remove(IPredicate predicate) 	{return this.predicates.remove(predicate);	}
	public  IPredicate	remove(int index)				{return this.predicates.remove(index);		}
    public void         removeAll()                     {this.predicates.clear();                   }
    public int          size()                          {return this.predicates.size();             }
    public boolean      contain(IPredicate predicate)   {return this.predicates.contains(predicate);}

    public	String	toString()
	{
		StringBuilder query = new StringBuilder();
		
		//query.append("where ");
		
		for (IPredicate predicate : predicates)
			query.append(predicate.toString() + " ");
		
		return query.toString();	
	}

	@Override
	public boolean evaluate(Object obj) {
		// TODO Auto-generated method stub
		return false;
	}
	
	
}
