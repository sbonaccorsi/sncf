package com.sncf.android.orm.criteria.aggregators;

import java.util.ArrayList;
import java.util.Objects;

import com.sncf.android.orm.criteria.IPredicate;
import com.sncf.android.orm.criteria.SQLPredicate;
import com.sncf.android.orm.criteria.operators.SQLOperator;
import com.sncf.android.utils.LogSncf;


public abstract class Aggregator implements IAggregator 
{
	private ArrayList<IPredicate>	predicates;
	private	String					type;	
	
	///////////////////////////////////////////////////////////
	////////////////	Constructors

	public	Aggregator(String type, ArrayList<IPredicate>	predicates)			{createStructure(type, predicates);}
	
	public void createStructure(String type, ArrayList<IPredicate>	predicates)	
	{	
		this.type	=	type;
		
		if (predicates == null)
			this.predicates 	= 	new ArrayList<IPredicate>();
		else
			this.predicates 	=   predicates;
	}

	/////////////////////////////////////////////////////////////
	//////////////	Methodes
	
	public boolean 		add(IPredicate predicate) 		{return this.predicates.add(predicate);		}
	public boolean 		remove(IPredicate predicate)	{return this.predicates.remove(predicate);	}
	public IPredicate 	remove(int index) 				{return this.predicates.remove(index);		}
    public void         removeAll()                     {this.predicates.clear();                   }
    public int          size()                          {return this.predicates.size();             }
    public boolean      contain(IPredicate predicate)   {return this.predicates.contains(predicate);}

	public 	String		toString()
	{
		StringBuilder	query = new StringBuilder();

		for(IPredicate predicate : predicates)
			query.append(predicate.toString() + " " + type + " ");
		
		int lastAnd = query.lastIndexOf(type + " ");
        if (lastAnd != -1)
		    query.delete(lastAnd, lastAnd + type.length() + 1);

		return query.toString();
	}
}
