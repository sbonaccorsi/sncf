package com.sncf.android.orm.criteria.operators;

public class GreaterThanSqlPredicate extends SQLOperator
{

	public GreaterThanSqlPredicate(Object value) 	{ super("> ?", value); }

}
