package com.sncf.android.orm.dao;

import android.content.Context;
import android.net.Uri;

import com.sncf.android.entity.SeriesEntity;
import com.sncf.android.orm.DAO;
import com.sncf.android.provider.SncfContent;

/**
 * Created by favre on 23/01/2014.
 */
public class SeriesDAO extends DAO<SeriesEntity>
{
    public SeriesDAO(Context context) {
        super(context, SncfContent.t_sncf_series.CONTENT_URI, SeriesEntity.class.getName());
    }
}
