package com.sncf.android.orm.dao;

import android.content.Context;
import android.net.Uri;

import com.sncf.android.entity.InterventionEntity;
import com.sncf.android.orm.DAO;
import com.sncf.android.provider.SncfContent;

/**
 * Created by favre on 23/01/2014.
 */
public class InterventionDAO extends DAO<InterventionEntity>
{
    public InterventionDAO(Context context) {
        super(context, SncfContent.t_gr_interventions.CONTENT_URI, InterventionEntity.class.getName());
    }
}
