package com.sncf.android.orm;

import android.content.ContentProviderOperation;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;

import com.sncf.android.orm.criteria.ICriteria;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by favre on 17/01/2014.
 */
public class DAO<T extends Entity> {

    protected Uri       uri;
    protected Class     clazz;
    protected Context   context;
    protected String    className;

    public DAO(Context context, Uri uri, String className) { createStructure(context, uri, className); }

    public void createStructure(Context context, Uri uri, String className)
    {
        this.context    = context;
        this.uri        = uri;
        this.className  = className;
    }

    public boolean create(T entity)
    {
        return DAL.create(this.context, this.uri, entity.toContentValues());
    }

    public boolean createAll(Collection<T> entities)
    {
        ArrayList<ContentProviderOperation> operations  = new ArrayList<ContentProviderOperation>();

        for (T entity : entities)
        {
            ContentProviderOperation.Builder    builder     = ContentProviderOperation.newInsert(this.uri);
            builder.withValues(entity.toContentValues());
            operations.add(builder.build());
        }

        return DAL.applyBatch(this.context, operations);
    }

    public Collection<T> read(ICriteria<T> criteria)
    {
        Cursor      cursor;
        Collection  returnValues    = new ArrayList();

        if (criteria == null)   cursor = DAL.read(this.context, this.uri, null);
        else                    cursor = DAL.read(this.context, this.uri, criteria.toString());

        if((cursor != null) && (cursor.moveToFirst())) {
            do
            {
                T entity = null;
                try
                {
                    this.clazz = Class.forName(this.className);
                    entity = (T)clazz.newInstance();
                }
                catch (InstantiationException e)    { Log.e("DAO", e.toString());   }
                catch (IllegalAccessException e)    { Log.e("DAO", e.toString());   }
                catch (ClassNotFoundException e)    { Log.e("DAO", e.toString());   }

                ((IEntity) entity).buildEntity(cursor);
                returnValues.add(entity);
            }
            while (cursor.moveToNext());
        }
        if (cursor != null) cursor.close();
        return returnValues;
    }

    public boolean update(T entity)
    {
        return DAL.update(this.context, this.uri, entity.toContentValues(),
                ((IEntity) entity).getPrimaryKeyName() + "=" + entity.getPrimaryKeyValue());
    }

    public boolean delete(T entity)
    {
        return DAL.delete(this.context, this.uri, ((IEntity)entity).getPrimaryKeyName() + "=" + ((IEntity)entity).getPrimaryKeyValue());
    }
}
