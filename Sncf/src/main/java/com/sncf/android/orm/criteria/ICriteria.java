package com.sncf.android.orm.criteria;

import android.support.v7.appcompat.R;

/**
 * Querying is a fundamental part of persistence.
 * Criteria is used to define dynamic queries through the construction of object-based query definition objects.
 * 
 * Is an aggregation of many Predicate.
 * @author favre
 * @see IPredicate
 */
public interface ICriteria<T> 
{
	/**
	 * <pre>
	 * @param predicate		predicate to add.
	 * @return <b><code>true</code></b>		if success.
	 * <b><code>false</code></b>		otherwise.
	 * </pre>
	 * @see IPredicate
	 */
	public	boolean			add(IPredicate<T> predicate);
	
	/**
	 * <pre>
	 * @param predicate		predicate to remove.
	 * @return <b><code>true</code></b>		if success.
	 * <b><code>false</code></b>		otherwise.
	 * </pre>
	 * @see IPredicate
	 */
	public	boolean			remove(IPredicate<T> predicate);
	
	/**
	 * <pre>
	 * @param obj	Object to evaluate.
	 * @return <b><code>true</code></b>		if success.
	 * <b><code>false</code></b>		otherwise.
	 * </pre>
	 */
	public boolean			evaluate(T obj);
	
	/**
	 * <pre>
	 * @param index		index of the predicate to remove.
	 * @return <b><code>true</code></b>		if success.
	 * <b><code>false</code></b>		otherwise.
	 * </pre>
	 */
	public	IPredicate<T>	remove(int index);
    public void             removeAll();
	public	String			toString();
    public int              size();
    public boolean          contain(IPredicate predicate);
}
