package com.sncf.android.orm.dao;

import android.content.Context;
import android.net.Uri;

import com.sncf.android.entity.LocationMicroDjEntity;
import com.sncf.android.orm.DAO;
import com.sncf.android.provider.SncfContent;

/**
 * Created by favre on 23/01/2014.
 */
public class LocationMicroDjDAO extends DAO<LocationMicroDjEntity>
{
    public LocationMicroDjDAO(Context context) {
        super(context, SncfContent.t_gr_localisations_mdj.CONTENT_URI, LocationMicroDjEntity.class.getName());
    }
}
