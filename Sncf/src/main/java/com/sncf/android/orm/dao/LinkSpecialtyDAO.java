package com.sncf.android.orm.dao;

import android.content.Context;
import android.net.Uri;

import com.sncf.android.entity.LinkSpecialtyEntity;
import com.sncf.android.orm.DAO;
import com.sncf.android.provider.SncfContent;

/**
 * Created by favre on 23/01/2014.
 */
public class LinkSpecialtyDAO extends DAO<LinkSpecialtyEntity>
{
    public LinkSpecialtyDAO(Context context) {
        super(context, SncfContent.t_sncf_link_specialities.CONTENT_URI, LinkSpecialtyEntity.class.getName());
    }
}
