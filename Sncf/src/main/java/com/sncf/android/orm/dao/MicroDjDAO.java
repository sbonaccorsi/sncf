package com.sncf.android.orm.dao;

import android.content.Context;
import android.net.Uri;

import com.sncf.android.entity.MicroDjEntity;
import com.sncf.android.orm.DAO;
import com.sncf.android.provider.SncfContent;

/**
 * Created by favre on 23/01/2014.
 */
public class MicroDjDAO extends DAO<MicroDjEntity>
{
    public MicroDjDAO(Context context) {
        super(context, SncfContent.t_gr_micro_dj.CONTENT_URI, MicroDjEntity.class.getName());
    }
}
