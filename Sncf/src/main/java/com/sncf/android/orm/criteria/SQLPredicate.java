package com.sncf.android.orm.criteria;

import com.sncf.android.orm.criteria.operators.IOperator;

public class SQLPredicate implements IPredicate 
{
	private String 		name;
	private	IOperator	operator;
	
	///////////////////////////////////
	//////////	CONSTRUCTORS
	
	public	SQLPredicate(String name, IOperator operator)					{createStructure(name, operator);}
	
	public	void	createStructure(String name, IOperator operator)
	{
		this.name 		= name;
		this.operator 	= operator;
	}
	
	////////////////////////////////////
	///////////	METHODES

	public	String	toString()	{return name + " " + operator.toString();}
	
	////////////////////////////////////
	///////////	Getters & Setters
	
	public String 		getName()						{return this.name;			}
	public IOperator 	getOperator()					{return this.operator;		}
	
	public void 		setName(String name)			{this.name 		= name;		}
	public void 		setOperator(IOperator operator) {this.operator 	= operator;	}

	@Override
	public boolean evaluate(Object obj) {
		// TODO Auto-generated method stub
		return false;
	}
}
