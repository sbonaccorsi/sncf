package com.sncf.android.orm;

import android.database.Cursor;

/**
 * Created by favre on 23/01/2014.
 */
public abstract class Entity implements IEntity
{
    protected int              primaryKey;

    @Override
    public int getPrimaryKeyValue() { return this.primaryKey;   }
}
