package com.sncf.android.orm.dao;

import android.content.Context;
import android.net.Uri;

import com.sncf.android.entity.RameEntity;
import com.sncf.android.orm.DAO;
import com.sncf.android.provider.SncfContent;

/**
 * Created by favre on 23/01/2014.
 */
public class RameDAO extends DAO<RameEntity>
{
    public RameDAO(Context context) {
        super(context, SncfContent.t_sncf_rames.CONTENT_URI, RameEntity.class.getName());
    }
}
