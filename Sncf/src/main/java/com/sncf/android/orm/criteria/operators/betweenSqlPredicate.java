package com.sncf.android.orm.criteria.operators;

public class BetweenSqlPredicate extends SQLOperator
{

	public BetweenSqlPredicate(Object[] values) 	{ super("BETWEEN ? AND ?", values); }

}
