package com.sncf.android.orm.criteria.operators;

public class EqualSqlPredicate extends SQLOperator
{

	public EqualSqlPredicate(Object value) 	{ super("= ?", value); }

}
