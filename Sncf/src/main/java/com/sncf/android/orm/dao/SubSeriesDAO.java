package com.sncf.android.orm.dao;

import android.content.Context;

import com.sncf.android.entity.SeriesEntity;
import com.sncf.android.entity.SubSeriesEntity;
import com.sncf.android.orm.DAO;
import com.sncf.android.provider.SncfContent;

/**
 * Created by favre on 23/01/2014.
 */
public class SubSeriesDAO extends DAO<SubSeriesEntity>
{
    public SubSeriesDAO(Context context) {
        super(context, SncfContent.t_sncf_subseries.CONTENT_URI, SubSeriesEntity.class.getName());
    }
}
