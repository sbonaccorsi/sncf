package com.sncf.android.orm;

import android.content.Context;
import android.net.Uri;
import android.util.Log;

import com.sncf.android.entity.CompoEntity;
import com.sncf.android.entity.InterventionEntity;
import com.sncf.android.entity.RameEntity;
import com.sncf.android.entity.SemelleBogieEntity;
import com.sncf.android.entity.SemelleControlesEntity;
import com.sncf.android.entity.SemelleDataEntity;
import com.sncf.android.orm.criteria.ICriteria;
import com.sncf.android.orm.criteria.SQLCriteria;
import com.sncf.android.orm.criteria.SQLPredicate;
import com.sncf.android.orm.criteria.operators.EqualSqlPredicate;
import com.sncf.android.orm.dao.CompoDAO;
import com.sncf.android.orm.dao.InterventionDAO;
import com.sncf.android.orm.dao.RameDAO;
import com.sncf.android.orm.dao.SemelleBogieDAO;
import com.sncf.android.orm.dao.SemelleControleDAO;
import com.sncf.android.orm.dao.SemelleDataDAO;
import com.sncf.android.provider.SncfContent;
import com.sncf.android.utils.CriteriaHelper;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by favre on 31/01/2014.
 */
public class ORM
{

    public static <T extends Entity> boolean createAll(Context context, Uri uri, Collection<T> entities, Class<T> entityClass)
    {
        DAO<T>      dao     = new DAO<T>(context, uri, entityClass.getName());

        if (entities != null)
        {
            //Log.i("ORM", entityClass.getName() + " Start...");
            boolean ret =  dao.createAll(entities);

            //Log.i("ORM", entityClass.getName() + "  ...End");

            return ret;
        }
        else                    return false;
    }

    public static <T extends Entity> T createEntityByPredicate(SQLPredicate predicate, DAO<T> dao)
    {
        T          entity                      = null;
        ICriteria criteria                    = new SQLCriteria();
        criteria.add(predicate);
        Collection<T> collection               = dao.read(criteria);
        if (collection != null && collection.size() != 0) entity = (T) collection.toArray()[0];
        return entity;
    }

    public static <T extends Entity> Collection<T> createEntitiesByPredicate(SQLPredicate predicate, DAO<T> dao) {
        ICriteria       criteria                    = new SQLCriteria();
        criteria.add(predicate);
        Collection<T> collection               = dao.read(criteria);
        return collection;
    }

    public static SemelleControlesEntity getSemelleControl(Context context, ICriteria criteria)
    {
        SemelleControleDAO      dao     = new SemelleControleDAO(context);
        SemelleControlesEntity  entity  = (SemelleControlesEntity)dao.read(criteria).toArray()[0];


        return mapSemelleControleEntity(context, entity);
    }

    public static SemelleControlesEntity mapSemelleControleEntity(Context context, SemelleControlesEntity entity)
    {
        Collection<SemelleBogieEntity>  bogies = ORM.createEntitiesByPredicate(
                CriteriaHelper.equal(SncfContent.t_sem_bogie.Columns.ID_CONTROL.getName(), entity.idControl),
                new SemelleBogieDAO(context));

        Collection<CompoEntity>         compos = new ArrayList<CompoEntity>();

        for(SemelleBogieEntity bogie : bogies)
        {
            bogie.datas = ORM.createEntitiesByPredicate(
                    CriteriaHelper.equal(SncfContent.t_sem_data.Columns.ID_BOGIE.getName(), bogie.idBogie),
                    new SemelleDataDAO(context));

            CompoEntity compo = createEntityByPredicate(CriteriaHelper.equal(
                    SncfContent.t_sncf_compos.Columns.ID_COMPOS.getName(), bogie.idCompo),
                    new CompoDAO(context));

            if (compos.contains(compo))
            {
                for (CompoEntity c : compos)
                    if (c.equals(compo)) c.bogies.add(bogie);
            }
            else
            {
                Log.i("ORM", "Add new Compo : " + compo.name);
                compo.bogies = new ArrayList<SemelleBogieEntity>();
                compo.bogies.add(bogie);
                compos.add(compo);
            }
        }

        entity.compos = compos;


        entity.rame                 = createEntityByPredicate(
                CriteriaHelper.equal(SncfContent.t_sncf_rames.Columns.ID_RAME.getName(), entity.idRame),
                new RameDAO(context));

        entity.intervention         = createEntityByPredicate(
                CriteriaHelper.equal(SncfContent.t_gr_interventions.Columns.ID_INTERVENTION.getName(), entity.idIntervention),
                new InterventionDAO(context));

        return entity;
    }

}
