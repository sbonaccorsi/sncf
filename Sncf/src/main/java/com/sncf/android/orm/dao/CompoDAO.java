package com.sncf.android.orm.dao;

import android.content.Context;
import android.net.Uri;

import com.sncf.android.entity.CompoEntity;
import com.sncf.android.orm.DAO;
import com.sncf.android.provider.SncfContent;

/**
 * Created by favre on 23/01/2014.
 */
public class CompoDAO extends DAO<CompoEntity>
{
    public CompoDAO(Context context) {
        super(context, SncfContent.t_sncf_compos.CONTENT_URI, CompoEntity.class.getName());
    }
}
