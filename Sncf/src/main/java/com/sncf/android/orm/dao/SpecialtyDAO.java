package com.sncf.android.orm.dao;

import android.content.Context;
import android.net.Uri;

import com.sncf.android.entity.SpecialtyEntity;
import com.sncf.android.orm.DAO;
import com.sncf.android.provider.SncfContent;

/**
 * Created by favre on 23/01/2014.
 */
public class SpecialtyDAO extends DAO<SpecialtyEntity>
{
    public SpecialtyDAO(Context context) {
        super(context, SncfContent.t_gr_specialities.CONTENT_URI, SpecialtyEntity.class.getName());
    }
}
