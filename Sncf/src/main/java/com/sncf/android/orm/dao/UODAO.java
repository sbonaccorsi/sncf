package com.sncf.android.orm.dao;

import android.content.Context;
import android.net.Uri;

import com.sncf.android.entity.UOEntity;
import com.sncf.android.orm.DAO;
import com.sncf.android.provider.SncfContent;

/**
 * Created by favre on 23/01/2014.
 */
public class UODAO extends DAO<UOEntity>
{
    public UODAO(Context context) {
        super(context, SncfContent.t_sncf_uo.CONTENT_URI, UOEntity.class.getName());
    }
}
