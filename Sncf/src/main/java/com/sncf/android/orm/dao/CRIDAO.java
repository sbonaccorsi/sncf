package com.sncf.android.orm.dao;

import android.content.Context;
import android.net.Uri;

import com.sncf.android.entity.CRIEntity;
import com.sncf.android.orm.DAO;
import com.sncf.android.provider.SncfContent;

/**
 * Created by favre on 23/01/2014.
 */
public class CRIDAO extends DAO<CRIEntity>
{
    public CRIDAO(Context context) {
        super(context, SncfContent.t_gr_cri.CONTENT_URI, CRIEntity.class.getName());
    }
}
