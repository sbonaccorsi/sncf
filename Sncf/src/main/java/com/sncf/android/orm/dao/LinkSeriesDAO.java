package com.sncf.android.orm.dao;

import android.content.Context;

import com.sncf.android.entity.LinkSeriesEntity;
import com.sncf.android.orm.DAO;
import com.sncf.android.provider.SncfContent;

/**
 * Created by favre on 23/01/2014.
 */
public class LinkSeriesDAO extends DAO<LinkSeriesEntity>
{
    public LinkSeriesDAO(Context context) {
        super(context, SncfContent.t_sncf_link_series.CONTENT_URI, LinkSeriesDAO.class.getName());
    }
}
