package com.sncf.android.orm.dao;

import android.content.Context;
import android.net.Uri;

import com.sncf.android.entity.STFEntity;
import com.sncf.android.orm.DAO;
import com.sncf.android.provider.SncfContent;

/**
 * Created by favre on 23/01/2014.
 */
public class STFDAO extends DAO<STFEntity>
{
    public STFDAO(Context context) {
        super(context, SncfContent.t_sncf_stf.CONTENT_URI, STFEntity.class.getName());
    }
}
