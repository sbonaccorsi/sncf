package com.sncf.android.orm.dao;

import android.content.Context;
import android.net.Uri;

import com.sncf.android.entity.RepairEntity;
import com.sncf.android.orm.DAO;
import com.sncf.android.provider.SncfContent;

/**
 * Created by favre on 23/01/2014.
 */
public class RepairDAO extends DAO<RepairEntity>
{
    public RepairDAO(Context context) {
        super(context, SncfContent.t_gr_reparations.CONTENT_URI, RepairEntity.class.getName());
    }
}
