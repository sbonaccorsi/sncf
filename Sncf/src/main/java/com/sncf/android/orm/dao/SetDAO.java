package com.sncf.android.orm.dao;

import android.content.Context;
import android.net.Uri;

import com.sncf.android.entity.SetEntity;
import com.sncf.android.orm.DAO;
import com.sncf.android.provider.SncfContent;

/**
 * Created by favre on 23/01/2014.
 */
public class SetDAO extends DAO<SetEntity>
{
    public SetDAO(Context context) {
        super(context, SncfContent.t_gr_ensembles.CONTENT_URI, SetEntity.class.getName());
    }
}
