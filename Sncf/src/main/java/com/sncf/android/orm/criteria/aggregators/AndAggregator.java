package com.sncf.android.orm.criteria.aggregators;

import java.util.ArrayList;

import com.sncf.android.orm.criteria.IPredicate;


public class AndAggregator extends Aggregator
{

	public AndAggregator() 									{super("AND", null);		}
	public AndAggregator(ArrayList<IPredicate>	predicates) {super ("AND", predicates);	}
	
	public AndAggregator(IPredicate... predicates)
	{
		super("AND", null);
		
		for(IPredicate predicate : predicates)
			this.add(predicate);
	}
	@Override
	public boolean evaluate(Object obj) {
		// TODO Auto-generated method stub
		return false;
	}

}
