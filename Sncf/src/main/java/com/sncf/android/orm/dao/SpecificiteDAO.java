package com.sncf.android.orm.dao;

import android.content.Context;

import com.sncf.android.entity.STFEntity;
import com.sncf.android.entity.SpecificiteEntity;
import com.sncf.android.orm.DAO;
import com.sncf.android.provider.SncfContent;

/**
 * Created by favre on 23/01/2014.
 */
public class SpecificiteDAO extends DAO<SpecificiteEntity>
{
    public SpecificiteDAO(Context context) {
        super(context, SncfContent.t_gr_specificite.CONTENT_URI, SpecificiteEntity.class.getName());
    }
}
