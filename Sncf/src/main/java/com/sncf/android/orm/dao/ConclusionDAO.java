package com.sncf.android.orm.dao;

import android.content.Context;
import android.net.Uri;

import com.sncf.android.entity.ConclusionEntity;
import com.sncf.android.orm.DAO;
import com.sncf.android.provider.SncfContent;

/**
 * Created by favre on 23/01/2014.
 */
public class ConclusionDAO extends DAO<ConclusionEntity>
{
    public ConclusionDAO(Context context) {
        super(context, SncfContent.t_gr_conclusions.CONTENT_URI, ConclusionEntity.class.getName());
    }
}
