package com.sncf.android.orm.criteria.aggregators;

import java.util.ArrayList;

import com.sncf.android.orm.criteria.IPredicate;


public class OrAggregator extends Aggregator
{

	public OrAggregator() 									{super("OR", null);			}
	public OrAggregator(ArrayList<IPredicate>	predicates) {super("OR", predicates);	}
	
	public OrAggregator(IPredicate...	predicates) 
	{
		super("OR", null);
		
		for(IPredicate predicate : predicates)
			this.add(predicate);
	}
	@Override
	public boolean evaluate(Object obj) {
		// TODO Auto-generated method stub
		return false;
	}
}
