package com.sncf.android.orm;

import android.content.ContentValues;
import android.database.Cursor;

/**
 * Created by favre on 17/01/2014.
 */
public interface IEntity {

    public void             buildEntity(Cursor cursor);
    public ContentValues    toContentValues();
    public int              getPrimaryKeyValue();
    public String           getPrimaryKeyName();
}
