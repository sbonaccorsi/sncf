package com.sncf.android.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.sncf.android.R;
import com.sncf.android.application.SncfApplication;
import com.sncf.android.base.BaseActivity;
import com.sncf.android.fragment.FragmentFilter;
import com.sncf.android.fragment.FragmentNavigation;
import com.sncf.android.fragment.FragmentNavigationDrawer;
import com.sncf.android.fragment.FragmentParameters;
import com.sncf.android.importExport.Loader;
import com.sncf.android.orm.criteria.SQLPredicate;
import com.sncf.android.orm.criteria.operators.BetweenSqlPredicate;
import com.sncf.android.provider.SncfContent;
import com.sncf.android.utils.DateHelper;
import com.sncf.android.utils.LogSncf;

public class MainActivity extends BaseActivity implements FragmentNavigationDrawer.NavigationDrawerCallbacks,
        FragmentNavigation.OnOpenRightPane, Loader.OnDataLoaderListener
{

    private static final String                 KEY_PARAM_SAVE_INSTANCE_CURRENT_POSITION_DRAWER = "savepositiondrawer";

    private DrawerLayout                        mDrawerLayout;
    private FragmentNavigationDrawer            mNavigationDrawerFragment;
    private int                                 mCurrentMenuId;
    private int                                 mCurrentPositionNavigationDrawer = -1;

    @Override
    protected int getContentView() {
        return R.layout.activity_main;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (SncfApplication.IS_UI_TABLET) {
            if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                return;
            }
        }
        if (savedInstanceState != null) {
            mCurrentPositionNavigationDrawer = savedInstanceState.getInt(KEY_PARAM_SAVE_INSTANCE_CURRENT_POSITION_DRAWER);
        }

        this.createDialogSynchro();

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mNavigationDrawerFragment = (FragmentNavigationDrawer)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);

        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));
    }

    private void createDialogSynchro() {
        AlertDialog.Builder adb = new AlertDialog.Builder(this);
        adb.setTitle(getString(R.string.title_dialog_synchro));
        adb.setMessage(getString(R.string.message_dialo_synchro));
        adb.setNegativeButton(getString(R.string.button_dialog_no), null);
        adb.setPositiveButton(getString(R.string.button_dialog_yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                showProgressDialog(getString(R.string.title_dialog_loading), getString(R.string.message_dialog_loading), false, null);
                Loader loader = new Loader(getBaseContext(), MainActivity.this);
                loader.executeAsync();
            }
        });
        adb.setCancelable(true);
        adb.show();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(KEY_PARAM_SAVE_INSTANCE_CURRENT_POSITION_DRAWER, mCurrentPositionNavigationDrawer);
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        int idContainer;
        if (SncfApplication.IS_UI_TABLET)
            idContainer = R.id.container_navigation_fragment;
        else
            idContainer = R.id.container_fragment;

        Fragment fragment = getSupportFragmentManager().findFragmentById(idContainer);

        switch (position) {
            case 1:
            case 0:
            case 2:
                if (SncfApplication.IS_UI_TABLET)
                    this.showFragmentNavigation();
                if (!(fragment instanceof FragmentNavigation)) {
                    LogSncf.e("MainActivity", "Create FragmentNavigation");
                    Bundle param = new Bundle();
                    param.putInt(FragmentNavigation.KEY_PARAM_POSITION_NAVIGATION, position);
                    fragment = Fragment.instantiate(this, FragmentNavigation.class.getName(), param);

                    if (SncfApplication.IS_UI_TABLET)
                        removeFragment(R.id.container_content_fragment);
                }
                else {
                    ((FragmentNavigation)fragment).updateNavigationFragmentData(position);
                    LogSncf.e("MainActivity", "Update FragmentNavigation");
                }
                break;
            case 3:
                if (SncfApplication.IS_UI_TABLET)
                    idContainer = this.hideFragmentNavigation();
                if (!(fragment instanceof FragmentFilter)) {
                    fragment = Fragment.instantiate(this, FragmentFilter.class.getName());
                }
                break;
            case 4:
                if (SncfApplication.IS_UI_TABLET)
                    idContainer = this.hideFragmentNavigation();
                if (!(fragment instanceof FragmentParameters)) {
                    fragment = Fragment.instantiate(this, FragmentParameters.class.getName());
                }
                break;
        }
        mCurrentMenuId = this.getMenuIdByPositionDrawer(position);

        if (mCurrentPositionNavigationDrawer != position) {
            mCurrentPositionNavigationDrawer = position;
            supportInvalidateOptionsMenu();
            replaceFragment(idContainer, fragment);
        }
    }

    private int getMenuIdByPositionDrawer(int position) {
        if (position == 0)
            return R.id.id_menu_restriction;
        else if (position == 1)
            return R.id.id_menu_cri;
        else if (position == 2)
            return R.id.id_menu_semelle;
        else if (position == 3)
            return R.id.id_menu_filter;
        else
            return R.id.id_main_menu;
    }

    private void showFragmentNavigation() {
        if (findViewById(R.id.container_navigation_fragment) != null)
            findViewById(R.id.container_navigation_fragment).setVisibility(View.VISIBLE);
    }

    private int hideFragmentNavigation() {
        if (findViewById(R.id.container_navigation_fragment) != null) {
            findViewById(R.id.container_navigation_fragment).setVisibility(View.GONE);
            removeFragment(R.id.container_navigation_fragment);
        }
        return R.id.container_content_fragment;
    }

    @Override
    public void openRightPane(Fragment fragment) {
        if (SncfApplication.IS_UI_TABLET)
            replaceFragment(R.id.container_content_fragment, fragment);
        else {
            addFragmentToBackStack(R.id.container_fragment, fragment);
            mNavigationDrawerFragment.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
            mNavigationDrawerFragment.displayDrawerToggle(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        }
    }

    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle(getResources().getStringArray(R.array.array_list_menu_drawer)[mCurrentPositionNavigationDrawer]);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            getMenuInflater().inflate(R.menu.main, menu);
            if (mCurrentMenuId == R.id.id_menu_semelle)
                getMenuInflater().inflate(R.menu.menu_semelle, menu);
            else if (mCurrentMenuId == R.id.id_menu_cri)
                getMenuInflater().inflate(R.menu.menu_cri, menu);
            else if (mCurrentMenuId == R.id.id_menu_filter)
                getMenuInflater().inflate(R.menu.menu_filter, menu);
            else if (mCurrentMenuId == R.id.id_menu_restriction)
                getMenuInflater().inflate(R.menu.menu_restriction, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        FragmentNavigation navigation   = null;
        switch (item.getItemId()) {
            case R.id.action_add_cri: startActivityByAction(AddReportActivity.ACTION_ADD_CRI);      return true;
            case R.id.action_add_semelle: startActivityByAction(AddReportActivity.ACTION_ADD_WEAR); return true;
            case R.id.action_cancel_filter:
                int idContainer;
                if (SncfApplication.IS_UI_TABLET)       idContainer     = R.id.container_content_fragment;
                else                                    idContainer     = R.id.container_fragment;
                Fragment                                fragment        = getSupportFragmentManager().findFragmentById(idContainer);
                if (fragment instanceof FragmentFilter) ((FragmentFilter)fragment).cancelFilters();
                return true;

            case R.id.action_filter_period_intervention: DateHelper.pickPeriod(this, getString(R.string.title_dialog_start_period),
                    getString(R.string.title_dialog_end_period), "yyyy-MM-dd", mListenerPeriodIntervention);
                return true;
            case R.id.action_filter_erase_cri_filter:
                navigation = this.restoreToFragmentNavigation();
                navigation.clearFilter();
                return true;

            case R.id.action_filter_period_put: DateHelper.pickPeriod(this, getString(R.string.title_dialog_start_period),
                    getString(R.string.title_dialog_end_period), "yyyy-MM-dd", mListenerPeriodPut);
                return true;
            case R.id.action_filter_erase_rest_filter:
                navigation = this.restoreToFragmentNavigation();
                navigation.clearFilter();
                return true;
            case R.id.action_refresh:
                this.createDialogSynchro();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

        if (!SncfApplication.IS_UI_TABLET) {
            mNavigationDrawerFragment.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
            mNavigationDrawerFragment.displayDrawerToggle(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container_fragment);
            if (fragment instanceof FragmentFilter) {
                if (((FragmentFilter)fragment).onBackPressed())
                    super.onBackPressed();
            }
            else
                super.onBackPressed();
        }
        else {
            super.onBackPressed();
        }
    }

    //////////////////////////////////////
    /// OnDataLoaderListener Methods

    @Override
    public void onSuccess() {
        hideProgressDialog();
        LogSncf.e("MainActivity", "Loader Success");

    }

    @Override
    public void onFailure() {
        hideProgressDialog();
        LogSncf.e("MainActivity", "Loader Failure");
    }

    private FragmentNavigation restoreToFragmentNavigation() {
        Fragment fragmentNav;
        if (!SncfApplication.IS_UI_TABLET) {
            fragmentNav = getSupportFragmentManager().findFragmentById(R.id.container_fragment);
            if (!(fragmentNav instanceof FragmentNavigation)) {
                onBackPressed();
                return (FragmentNavigation) getSupportFragmentManager().findFragmentById(R.id.container_fragment);
            }
            return (FragmentNavigation) fragmentNav;
        }
        return (FragmentNavigation) getSupportFragmentManager().findFragmentById(R.id.container_navigation_fragment);
    }


    /**
     * DatePicker listener for period intervention filter on screen CRI
     * */
    private DateHelper.OnPickPeriodListner mListenerPeriodIntervention = new DateHelper.OnPickPeriodListner() {
        @Override
        public void pickStartPeriod(String dateStart) { }
        @Override
        public void pickEndPeriod(String dateStart, String dateEnd) {
            BetweenSqlPredicate between = new BetweenSqlPredicate(new String[]{"'" + dateStart + " 00:00:00.0'", "'" + dateEnd + " 00:00:00.0'"});
            SQLPredicate predicate = new SQLPredicate(SncfContent.t_gr_cri.Columns.DATE_INTERVENTION.getName(), between);
            FragmentNavigation navigation = restoreToFragmentNavigation();
            navigation.updatePredicatePeriodIntervention(predicate);
        }
    };
    //#############################################################################################################################

    /**
     * DatePicker listener for period put filter on screen Restriction
     * */
    private DateHelper.OnPickPeriodListner mListenerPeriodPut = new DateHelper.OnPickPeriodListner() {
        @Override
        public void pickStartPeriod(String dateStart) {}
        @Override
        public void pickEndPeriod(String dateStart, String dateEnd) {
            BetweenSqlPredicate between = new BetweenSqlPredicate(new String[]{"'" + dateStart + " 00:00:00.0'", "'" + dateEnd + " 00:00:00.0'"});
            SQLPredicate predicate = new SQLPredicate(SncfContent.t_gr_restriction_mvc.Columns.DATE_POSE.getName(), between);
            FragmentNavigation navigation = restoreToFragmentNavigation();
            navigation.updatePredicatePeriodPut(predicate);
        }
    };
    //#############################################################################################################################

    /**
     * DatePicker listener for period amortization filter on screen Restriction
     * */
    private DateHelper.OnPickPeriodListner mListenerPeriodAmortization = new DateHelper.OnPickPeriodListner() {
        @Override
        public void pickStartPeriod(String dateStart) {}
        @Override
        public void pickEndPeriod(String dateStart, String dateEnd) {
            BetweenSqlPredicate between = new BetweenSqlPredicate(new String[]{"'" + dateStart + " 00:00:00.0'", "'" + dateEnd + " 00:00:00.0'"});
            SQLPredicate predicate = new SQLPredicate(SncfContent.t_gr_restriction_mvc.Columns.DATE_AMORTISSEMENT.getName(), between);
            FragmentNavigation navigation = restoreToFragmentNavigation();
            navigation.updatePredicatePeriodAmortization(predicate);
        }
    };
    //#############################################################################################################################
}
