package com.sncf.android.activity;

import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Menu;
import android.view.MenuItem;

import com.google.gson.annotations.SerializedName;
import com.sncf.android.R;
import com.sncf.android.application.SncfApplication;
import com.sncf.android.base.BaseActivity;
import com.sncf.android.fragment.FragmentAddCRI;
import com.sncf.android.fragment.FragmentAddWear;
import com.sncf.android.utils.LogSncf;

/**
 * Created by zzzwist on 18/01/14.
 */
public class AddReportActivity extends BaseActivity {

    public static final String              ACTION_ADD_CRI = "com.sncf.android.activity.AddReportActivity.AddCRI";
    public static final String              ACTION_ADD_WEAR = "com.sncf.android.activity.AddReportActivity.AddWear";

    @Override
    protected int getContentView() {
        return R.layout.activity_add_report;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (SncfApplication.IS_UI_TABLET) {
            if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                return;
            }
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        String action = this.getAction();

        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container_add_report_fragment);
        String tag = null;
        if (fragment == null) {
            if (action.equals(ACTION_ADD_CRI)) { fragment = Fragment.instantiate(this, FragmentAddCRI.class.getName()); tag = FragmentAddCRI.FRAGMENT_TAG; }
            else if (action.equals(ACTION_ADD_WEAR)) { fragment = Fragment.instantiate(this, FragmentAddWear.class.getName()); }

            addFragment(R.id.container_add_report_fragment, fragment, tag);
        }
    }

    private String getAction() {
        return getIntent().getAction();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
