package com.sncf.android.base;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;

import com.sncf.android.application.SncfApplication;

/**
 * Created by zzzwist on 24/12/13.
 */
public abstract class BaseActivity extends ActionBarActivity {

    protected ProgressDialog                    mProgressDialog;

    protected abstract int getContentView();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getContentView());
    }

    protected void addFragmentToBackStack(int idContainer, Fragment fragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .addToBackStack(null)
                .add(idContainer, fragment)
                .commit();
    }

    protected void addFragment(int idContainer, Fragment fragment, String tag) {
        getSupportFragmentManager()
                .beginTransaction()
                .add(idContainer, fragment, tag)
                .commit();
    }

    protected void replaceFragment(int idContainer, Fragment fragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(idContainer, fragment)
                .commit();
    }

    protected void removeFragment(int idContainer) {

        Fragment fragment = getSupportFragmentManager().findFragmentById(idContainer);
        if (fragment != null) {
            getSupportFragmentManager().beginTransaction().remove(fragment).commit();
        }
    }

    protected void startActivityByAction(String action) {
        Intent intent  = new Intent(action);
        startActivity(intent);
    }

    protected void showProgressDialog(String title, String message, boolean cancelable, DialogInterface.OnCancelListener cancelListener) {
        if (cancelListener != null)
            mProgressDialog = ProgressDialog.show(this, title, message, true, cancelable, cancelListener);
        else
            mProgressDialog = ProgressDialog.show(this, title, message, true, cancelable);
    }

    protected void hideProgressDialog() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }
}
