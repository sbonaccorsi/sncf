package com.sncf.android.base;

import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

/**
 * Created by zzzwist on 26/12/13.
 */
public abstract class BaseListFragment extends ListFragment implements AdapterView.OnItemClickListener {

    abstract protected int                  getContentLayout();

    protected ListView                      mListView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(getContentLayout(), container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mListView = getListView();
        mListView.setOnItemClickListener(this);
    }

    public void clearChoiceListView() {
        if (mListView != null) {
            mListView.clearChoices();
            mListView.requestLayout();
        }
    }

    public abstract class BaseHolder {
    }
}
