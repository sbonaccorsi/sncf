package com.sncf.android.base;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by zzzwist on 26/01/14.
 */
public abstract class AbstractPagerFragment extends BaseFragment implements ViewPager.OnPageChangeListener {

    protected ViewPager             mViewPager;
    protected ViewPagerAdapter      mPagerAdapter;

    protected abstract int          getIdViewPager();
    protected abstract int          getOffscreenPageLimit();

    protected abstract Fragment     adapterGetItem(int position);
    protected abstract int          adapterGetCount();
    protected abstract void         adapterDestroyItem(ViewGroup container, int position, Object object);

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mViewPager = (ViewPager) view.findViewById(getIdViewPager());
        mViewPager.setOffscreenPageLimit(getOffscreenPageLimit());
        mViewPager.setOnPageChangeListener(this);


        //mViewPager.setAdapter(mPagerAdapter);
    }

    protected class ViewPagerAdapter extends FragmentStatePagerAdapter {

        public SparseArray<Fragment>            mArray;
        public FragmentManager                  mFragmentManager;

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
            mArray = new SparseArray<Fragment>();
            mFragmentManager = fm;
        }

        @Override
        public Fragment getItem(int i) {
            return adapterGetItem(i);
        }

        @Override
        public int getCount() {
            return adapterGetCount();
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            return super.instantiateItem(container, position);
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
//            super.destroyItem(container, position, object);
//            adapterDestroyItem(container, position, object);
        }
    }
}
