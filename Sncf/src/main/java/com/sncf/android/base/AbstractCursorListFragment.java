package com.sncf.android.base;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

/**
 * Created by zzzwist on 26/12/13.
 */
public abstract class AbstractCursorListFragment extends BaseListFragment implements LoaderManager.LoaderCallbacks<Cursor> {

    protected ListCursorAdapter             mAdapter;

    protected abstract List<Integer>        getLoaderCallbacksIds();
    protected abstract int                  getLoaderCallbackId();
    protected abstract int                  getItemId(Cursor cursor, int position);
    protected abstract BaseHolder           initializeHolder(final View view, int position);
    protected abstract void                 configureView(final BaseHolder holder, final View view, Cursor cursor, int position);

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initializeLoaderManager();
    }

    protected void initializeLoaderManager() {
        if (getLoaderCallbacksIds() != null) {
            for (Integer id : getLoaderCallbacksIds()) {
                getLoaderManager().initLoader(id, null, this);
            }
        }
        else {
            getLoaderManager().initLoader(getLoaderCallbackId(), null, this);
        }
    }

    public class ListCursorAdapter extends SimpleCursorAdapter {

        public ListCursorAdapter(Context context, int layout, Cursor c, String[] from, int[] to, int flags) {
            super(context, layout, c, from, to, flags);
        }

        @Override
        public long getItemId(int position) {
            return AbstractCursorListFragment.this.getItemId(getCursor(), position);
        }

        @Override
        public int getCount() {
            return super.getCount();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            View view = super.getView(position, convertView, parent);
            Cursor cursor = getCursor();
            BaseHolder holder = initializeHolder(view, position);
            configureView(holder, view, cursor, position);
            return view;
        }
    }
}
