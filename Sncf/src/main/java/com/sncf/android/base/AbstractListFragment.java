package com.sncf.android.base;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.List;

/**
 * Created by zzzwist on 26/12/13.
 */
public abstract class AbstractListFragment extends BaseListFragment {

    abstract protected View                 initializeView(View convertView);
    abstract protected void                 configureView(int position, BaseHolder baseHolder, View convertView);

    protected ListBaseAdapter               mAdapter;

    public class ListBaseAdapter extends BaseAdapter {

        private List<?>                     mData;

        public ListBaseAdapter(List<?> data) {
            mData = data;
        }

        @Override
        public int getCount() {
            return mData.size();
        }

        @Override
        public Object getItem(int position) {
            return mData.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            convertView = initializeView(convertView);
            BaseHolder holder = (BaseHolder) convertView.getTag();
            configureView(position, holder, convertView);
            return convertView;
        }
    }
}
