package com.sncf.android.utils;

import android.util.Log;

import com.google.common.util.concurrent.ThreadFactoryBuilder;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Created by favre on 01/02/2014.
 */
public class Parallel {
    private static final int NUM_CORES = Runtime.getRuntime().availableProcessors();

    private static final ExecutorService forPool = Executors.newFixedThreadPool(NUM_CORES * 2, new ThreadFactoryBuilder().setNameFormat("Parallel.For").build());

    public static <T> void For(final Iterable<T> elements, final Operation<T> operation)
    {
        try                             { forPool.invokeAll(createCallables(elements, operation));  }
        catch (InterruptedException e)  { Log.e("Parallel", e.toString()); e.printStackTrace();     }
    }

    public static <T> Collection<Callable<Void>> createCallables(final Iterable<T> elements, final Operation<T> operation)
    {
        List<Callable<Void>> callables = new LinkedList<Callable<Void>>();
        for (final T elem : elements)
        {
            callables.add(new Callable<Void>() {
                @Override
                public Void call() {

                    operation.perform(elem);
                    return null;
                }
            });
        }


        return callables;
    }

    public static interface Operation<T> {
        public void perform(T pParameter);
    }
}
