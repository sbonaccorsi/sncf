package com.sncf.android.utils;

import android.util.Log;

import com.sncf.android.application.SncfApplication;

/**
 * Created by zzzwist on 11/01/14.
 */
public class LogSncf {

    public static void e(String tag, String msg) {
        if (SncfApplication.IS_DEBUG_MODE)
            Log.e(tag, "" + msg);
    }

    public static void d(String tag, String msg) {
        if (SncfApplication.IS_DEBUG_MODE)
            Log.d(tag, "" + msg);
    }

    public static void w(String tag, String msg) {
        if (SncfApplication.IS_DEBUG_MODE)
            Log.w(tag, "" + msg);
    }
}
