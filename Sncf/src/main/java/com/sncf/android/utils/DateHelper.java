package com.sncf.android.utils;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.CalendarView;
import android.widget.DatePicker;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by zzzwist on 01/02/14.
 */
public class DateHelper {

    public static String formatDate(String date, String inputPattern, String outputPattern, Locale locale) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(inputPattern, locale);
        try {
            Date d = dateFormat.parse(date);
            date = (new SimpleDateFormat(outputPattern)).format(d);
            return date;
        }
        catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Date getDateObjectFromStringDate(String date, String inputPattern, Locale locale) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(inputPattern, locale);
        try {
            Date d = dateFormat.parse(date);
            return d;
        }
        catch (ParseException e) {
            return null;
        }
    }

    public static String getCurrentDateToString(String inputPattern) {
        SimpleDateFormat format = new SimpleDateFormat(inputPattern);
        return format.format(new Date());
    }

    public static Date getCurrentDateToDate(String inputPattern) {
        SimpleDateFormat format = new SimpleDateFormat(inputPattern);
        String date = format.format(new Date());
        try {
            return format.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static int getDaysNumberSinceDateToToday(Date older) {
        Date today = DateHelper.getCurrentDateToDate("dd/MM/yyyy");
        return (int)( (today.getTime() - older.getTime()) / (1000 * 60 * 60 * 24));
    }

    public interface OnPickPeriodListner {
        public void pickStartPeriod(String dateStart);
        public void pickEndPeriod(String dateStart, String dateEnd);
    }

    public static void pickPeriod(final Context context, final String titleStartPeriod, final String titleEndPeriod, String outputPattern, final OnPickPeriodListner listener) {
        listenerPickPeriod = listener;
        outputPatternDate = outputPattern;
        titleEndPeriodD = titleEndPeriod;
        contextD = context;
        DatePickerDialog dialogStart = createPicker(titleStartPeriod, context, dateSetListenerStart);
        dialogStart.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {

            }
        });
        dialogStart.show();
    }

    private static OnPickPeriodListner      listenerPickPeriod;
    private static String                   outputPatternDate;
    private static String                   dateStart;
    private static String                   titleEndPeriodD;
    private static Context                  contextD;

    private static DatePickerDialog createPicker(String title, Context context, DatePickerDialog.OnDateSetListener listener) {
        Calendar calendar = Calendar.getInstance();
        DatePickerDialog dialog = new DatePickerDialog(context, listener, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        dialog.setTitle(title);
        return dialog;
    }

    private static DatePickerDialog.OnDateSetListener dateSetListenerStart = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            if (view.isShown()) {
                Calendar calendar = Calendar.getInstance();
                calendar.set(year, monthOfYear, dayOfMonth);
                String format = formatDate(String.format("%d/%d/%d", dayOfMonth, monthOfYear + 1, year), "dd/MM/yyyy", outputPatternDate, Locale.getDefault());
                dateStart = format;
                if (listenerPickPeriod != null)     listenerPickPeriod.pickStartPeriod(format);

                DatePickerDialog dialogEnd = createPicker(titleEndPeriodD, contextD, dateSetListenerEnd);
                dialogEnd.show();
            }
        }
    };

    private static DatePickerDialog.OnDateSetListener dateSetListenerEnd = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            if (view.isShown()) {
                Calendar calendar = Calendar.getInstance();
                calendar.set(year, monthOfYear, dayOfMonth);
                String format = formatDate(String.format("%d/%d/%d", dayOfMonth, monthOfYear + 1, year), "dd/MM/yyyy", outputPatternDate, Locale.getDefault());
                if (listenerPickPeriod != null)     listenerPickPeriod.pickEndPeriod(dateStart, format);
            }
        }
    };
}
