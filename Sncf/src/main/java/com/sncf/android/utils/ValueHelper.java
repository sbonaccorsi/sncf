package com.sncf.android.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ValueHelper
{
	public static String betweenDoubleQuote(String value)	{return "\"" 	+ value + "\"";	}
	public static String betweenSimpleQuote(String value)	{return "'" 	+ value + "'";	}


    public static <E extends Object> List<List<E>> split(Collection<E> input, int size)
    {
        List<List<E>> master = new ArrayList<List<E>>();
        if (input != null && input.size() > 0) {
            List<E> col = new ArrayList<E>(input);
            boolean done = false;
            int startIndex = 0;
            int endIndex = col.size() > size ? size : col.size();
            while (!done) {
                master.add(col.subList(startIndex, endIndex));
                if (endIndex == col.size()) {
                    done = true;
                }
                else {
                    startIndex = endIndex;
                    endIndex = col.size() > (endIndex + size) ? (endIndex + size) : col.size();
                }
            }
        }
        return master;
    }
}
