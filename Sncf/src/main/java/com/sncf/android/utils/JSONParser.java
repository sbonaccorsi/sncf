package com.sncf.android.utils;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.stream.JsonReader;

import java.io.StringReader;

/**
 * Created by zzzwist on 24/01/14.
 */
public class JSONParser {

    public static <GenericData> GenericData deserializeData(String content, Class<GenericData> jclass) {

        GenericData genericData;
        try {
            JsonReader reader = new JsonReader(new StringReader(content));
            reader.setLenient(true);
            Gson gson = new Gson();
            genericData = gson.fromJson(reader, jclass);
        }
        catch (JsonSyntaxException e) {
            e.printStackTrace();
            return null;
        }
        return genericData;
    }

    public static <GenericData> String SerializeData(GenericData data, Class<GenericData> jclass) {

        try
        {
            Gson gson = new Gson();
            String json = gson.toJson(data, jclass);
            return json;
        }
        catch (Exception e) { LogSncf.e("JSONParser", e.toString()); e.printStackTrace();};
        return null;
    }
}
