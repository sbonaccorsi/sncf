package com.sncf.android.utils;

import com.sncf.android.orm.DAO;
import com.sncf.android.orm.Entity;
import com.sncf.android.orm.criteria.ICriteria;
import com.sncf.android.orm.criteria.SQLCriteria;
import com.sncf.android.orm.criteria.SQLPredicate;

import java.util.Collection;

/**
 * Created by sbonaccorsi on 01/03/2014.
 */
public class EntityCreator {

    public static <GenericDAO extends Entity> GenericDAO buildEntity(DAO<GenericDAO> dao, SQLPredicate predicate) {
        ICriteria criteria = new SQLCriteria();
        criteria.add(predicate);

        LogSncf.e("EntityCreator", "Request sql = " + criteria.toString());

        Collection<GenericDAO> entities = dao.read(criteria);
        if (entities == null || entities.size() == 0) return null;
        return (GenericDAO) entities.toArray()[0];
    }
}
