package com.sncf.android.utils;

import android.os.Environment;
import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

/**
 * Created by favre on 02/01/14.
 *
 * This is a utility class. Help to file manipulation
 *
 * @author favre
 * @version 1.0
 */
public class FileHelper
{

    /**
     * Method that write data in file
     *
     * @param file  The file which is writable
     * @param data  data which are to write
     */
    public static void writeInFile(File file, Object data)
    {
        try
        {
            OutputStream        os  = new FileOutputStream(file, true);
            OutputStreamWriter  osw = new OutputStreamWriter(os);
            BufferedWriter      bw  = new BufferedWriter(osw);

            bw.write(data.toString());
            bw.close();
        }
        catch (FileNotFoundException e) { LogSncf.e("FileHelper", e.toString()); }
        catch (IOException e)           { LogSncf.e("FileHelper", e.toString()); }
    }

    /**
     * Method that write data in a new line of file
     *
     * @param file  The file which is writable
     * @param data  data which are to write
     */
    public static void writeNewLineInFile(File file, Object data)
    {
        try
        {
            OutputStream        os  = new FileOutputStream(file, true);
            OutputStreamWriter  osw = new OutputStreamWriter(os);
            BufferedWriter      bw  = new BufferedWriter(osw);

            bw.newLine();
            bw.write(data.toString());
            bw.close();
        }
        catch (FileNotFoundException e) { LogSncf.e("FileHelper", e.toString()); }
        catch (IOException e)           { LogSncf.e("FileHelper", e.toString()); }
    }

    /**
     * Method that read a file
     *
     * @param file The file to read
     * @return All the data read in file
     */
    public static String readFile(File file)
    {
        StringBuilder       sb  = new StringBuilder();
        try
        {
            InputStream         is  = new FileInputStream(file);
            InputStreamReader   isr = new InputStreamReader(is);
            BufferedReader      br  = new BufferedReader(isr);
            String              str = "";

            while( (str = br.readLine()) != null) {
                sb.append(str + "\n");
            }

            br.close();
        }
        catch (FileNotFoundException e) { Log.e("FileHelper", e.toString()); }
        catch (IOException e)           { Log.e("FileHelper", e.toString()); }

        return sb.toString();
    }

    /**
     * Method that clear a file
     *
     * @param file The file to clear
     */
    public static void clearFile(File file)
    {
        try
        {
            OutputStream os = new FileOutputStream(file);
            os.close();
        }
        catch (FileNotFoundException e) { LogSncf.e("FileHelper", e.toString()); }
        catch (IOException e)           { LogSncf.e("FileHelper", e.toString()); }
    }

    /**
     * Method that check if external storage is writable
     *
     * @return True if external storage is writable. False otherwise
     */
    public static boolean isExternalStorageWritable()
    {
        String state = Environment.getExternalStorageState();

        if (Environment.MEDIA_MOUNTED.equals(state))    return true;
        else                                            return false;
    }

    /**
     * Method that check if external storage is readable
     *
     * @return True if external storage is readable. False otherwise
     */
    public static boolean isExternalStorageReadable()
    {
        String state = Environment.getExternalStorageState();

        if (Environment.MEDIA_MOUNTED.equals(state) ||
            Environment.MEDIA_MOUNTED_READ_ONLY.equals(state))  return true;
        else                                                    return false;
    }

    /**
     * Method that return a file contain in a external storage directory
     *
     * @param directory The external storage directory
     * @param filename  The name of file
     * @return return a file contain in a external storage directory
     */
    public static File getStorageDir(String directory, String filename)
    {
        File file = new File(Environment.getExternalStoragePublicDirectory(directory), filename);

        if (!file.getParentFile().mkdirs()) LogSncf.e("FileHelper", "Directory not created");

        return file;
    }

    public static File getAlbumStorage(String filename)     { return getStorageDir(Environment.DIRECTORY_PICTURES,  filename); }
    public static File getMusicStorage(String filename)     { return getStorageDir(Environment.DIRECTORY_MUSIC,     filename); }
    public static File getMoviesStorage(String filename)    { return getStorageDir(Environment.DIRECTORY_MOVIES,    filename); }
    public static File getDocumentStorage(String filename)  { return getStorageDir(Environment.DIRECTORY_DOCUMENTS, filename); }
}

