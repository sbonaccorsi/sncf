package com.sncf.android.utils;

import com.sncf.android.orm.criteria.SQLPredicate;
import com.sncf.android.orm.criteria.operators.EqualSqlPredicate;
import com.sncf.android.orm.dao.SemelleBogieDAO;
import com.sncf.android.provider.SncfContent;

/**
 * Created by favre on 07/02/2014.
 */
public class CriteriaHelper
{
    public static SQLPredicate equal(String data, Object value)
    {
        return new SQLPredicate(data, new EqualSqlPredicate(value));
    }
}
