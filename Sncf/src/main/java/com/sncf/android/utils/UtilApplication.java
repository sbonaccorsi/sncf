package com.sncf.android.utils;

import android.content.Context;
import android.content.pm.ApplicationInfo;

/**
 * Created by zzzwist on 11/01/14.
 */
public class UtilApplication {

    public static boolean isDebugMode(Context context) {
        return (0 != (context.getApplicationInfo().flags & ApplicationInfo.FLAG_DEBUGGABLE));
    }
}
