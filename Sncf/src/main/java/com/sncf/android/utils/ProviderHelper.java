package com.sncf.android.utils;

import android.database.Cursor;

import com.sncf.android.orm.DAO;
import com.sncf.android.orm.Entity;
import com.sncf.android.orm.criteria.ICriteria;
import com.sncf.android.orm.criteria.SQLCriteria;
import com.sncf.android.orm.criteria.SQLPredicate;
import com.sncf.android.provider.SncfContent;

import java.util.Collection;

/**
 * Created by favre on 24/01/2014.
 */
public class ProviderHelper
{
    public static String getString(Cursor cursor, String ColumnName)
    {
        if (!cursor.isNull(cursor.getColumnIndex(ColumnName)))
                return cursor.getString(cursor.getColumnIndex(ColumnName));

        return null;
    }

    public static Integer getInt(Cursor cursor, String ColumnName)
    {
        if (!cursor.isNull(cursor.getColumnIndex(ColumnName)))
            return cursor.getInt(cursor.getColumnIndex(ColumnName));

        return null;
    }

    public static Boolean getBoolean (Cursor cursor, String ColumnName)
    {
        if (!cursor.isNull(cursor.getColumnIndex(ColumnName)))
            return cursor.getInt(cursor.getColumnIndex(ColumnName)) == 0 ? false : true;

        return null;
    }
}
