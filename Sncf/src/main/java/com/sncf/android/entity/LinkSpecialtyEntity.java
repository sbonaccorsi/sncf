package com.sncf.android.entity;

import android.content.ContentValues;
import android.database.Cursor;

import com.google.gson.annotations.SerializedName;
import com.sncf.android.data.JsonModel;
import com.sncf.android.orm.Entity;
import com.sncf.android.provider.SncfContent;
import com.sncf.android.utils.ProviderHelper;

/**
 * Created by favre on 23/01/2014.
 */
public class LinkSpecialtyEntity extends Entity
{
    @SerializedName(JsonModel.t_sncf_link_specialities.ID)
    public Integer      idLinkSpecialty;

    @SerializedName(JsonModel.t_sncf_link_specialities.ID_UO)
    public Integer      uoId;

    @SerializedName(JsonModel.t_sncf_link_specialities.ID_SITE)
    public Integer      siteId;

    @SerializedName(JsonModel.t_sncf_link_specialities.ID_SPECIALITY)
    public Integer      specialtyId;

    @SerializedName(JsonModel.t_sncf_link_specialities.SPECIALITY_GRIFFE)
    public String       griffeSpecialty;

    public LinkSpecialtyEntity()                {                           }
    public LinkSpecialtyEntity(Cursor cursor)   { this.buildEntity(cursor); }

    @Override
    public void buildEntity(Cursor cursor) {

        this.primaryKey         = cursor.getInt(cursor.getColumnIndex(SncfContent.t_sncf_link_specialities.Columns.ID.getName()));

        this.idLinkSpecialty    = ProviderHelper.getInt(cursor,     SncfContent.t_sncf_link_specialities.Columns.ID_LINK_SPECIALITY.getName());
        this.uoId               = ProviderHelper.getInt(cursor,     SncfContent.t_sncf_link_specialities.Columns.ID_UO.getName());
        this.siteId             = ProviderHelper.getInt(cursor,     SncfContent.t_sncf_link_specialities.Columns.ID_SITE.getName());
        this.specialtyId        = ProviderHelper.getInt(cursor,     SncfContent.t_sncf_link_specialities.Columns.ID_SPECIALITY.getName());
        this.griffeSpecialty    = ProviderHelper.getString(cursor,  SncfContent.t_sncf_link_specialities.Columns.SPECIALITY_GRIFFE.getName());
    }

    @Override
    public ContentValues toContentValues() {

        ContentValues returnValue = new ContentValues();

        returnValue.put(SncfContent.t_sncf_link_specialities.Columns.ID_LINK_SPECIALITY.getName(),  this.idLinkSpecialty);
        returnValue.put(SncfContent.t_sncf_link_specialities.Columns.ID_UO.getName(),               this.uoId);
        returnValue.put(SncfContent.t_sncf_link_specialities.Columns.ID_SITE.getName(),             this.siteId);
        returnValue.put(SncfContent.t_sncf_link_specialities.Columns.ID_SPECIALITY.getName(),       this.specialtyId);
        returnValue.put(SncfContent.t_sncf_link_specialities.Columns.SPECIALITY_GRIFFE.getName(),   this.griffeSpecialty);

        return returnValue;
    }

    @Override
    public String getPrimaryKeyName() {
        return SncfContent.t_sncf_link_specialities.Columns.ID.getName();
    }
}
