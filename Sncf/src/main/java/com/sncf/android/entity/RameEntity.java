package com.sncf.android.entity;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.google.gson.annotations.SerializedName;
import com.sncf.android.data.JsonModel;
import com.sncf.android.orm.DAL;
import com.sncf.android.orm.Entity;
import com.sncf.android.provider.SncfContent;
import com.sncf.android.utils.ProviderHelper;

/**
 * Created by favre on 23/01/2014.
 */
public class RameEntity extends Entity
{
    @SerializedName(JsonModel.t_sncf_rames.ID)
    public Integer      idRame;

    @SerializedName(JsonModel.t_sncf_rames.ID_STF)
    public Integer      stfId;

    @SerializedName(JsonModel.t_sncf_rames.ID_SERIE)
    public Integer      seriesId;

    @SerializedName(JsonModel.t_sncf_rames.ID_UNDER_SERIE)
    public Integer      subSeriesId;

    @SerializedName(JsonModel.t_sncf_rames.ID_VARIANTE)
    public Integer      varianteId;

    @SerializedName(JsonModel.t_sncf_rames.EAB)
    public String       eab;

    @SerializedName(JsonModel.t_sncf_rames.CS_GRIFFE)
    public String       csGriffe;

    public RameEntity()                 {                           }
    public RameEntity(Cursor cursor)    { this.buildEntity(cursor); }

    @Override
    public void buildEntity(Cursor cursor) {

        this.primaryKey         = cursor.getInt(cursor.getColumnIndex(SncfContent.t_sncf_rames.Columns.ID.getName()));

        this.idRame             = ProviderHelper.getInt(cursor,     SncfContent.t_sncf_rames.Columns.ID_RAME.getName());
        this.stfId              = ProviderHelper.getInt(cursor,     SncfContent.t_sncf_rames.Columns.ID_STF.getName());
        this.seriesId           = ProviderHelper.getInt(cursor,     SncfContent.t_sncf_rames.Columns.ID_SERIE.getName());
        this.subSeriesId        = ProviderHelper.getInt(cursor,     SncfContent.t_sncf_rames.Columns.ID_UNDER_SERIE.getName());
        this.varianteId         = ProviderHelper.getInt(cursor,     SncfContent.t_sncf_rames.Columns.ID_VARIANTE.getName());
        this.eab                = ProviderHelper.getString(cursor,  SncfContent.t_sncf_rames.Columns.EAB.getName());
        this.csGriffe           = ProviderHelper.getString(cursor,  SncfContent.t_sncf_rames.Columns.CS_GRIFFE.getName());
    }

    @Override
    public ContentValues toContentValues() {

        ContentValues returnValue = new ContentValues();

        returnValue.put(SncfContent.t_sncf_rames.Columns.ID_RAME.getName(),         this.idRame);
        returnValue.put(SncfContent.t_sncf_rames.Columns.ID_STF.getName(),          this.stfId);
        returnValue.put(SncfContent.t_sncf_rames.Columns.ID_SERIE.getName(),        this.seriesId);
        returnValue.put(SncfContent.t_sncf_rames.Columns.ID_UNDER_SERIE.getName(),  this.subSeriesId);
        returnValue.put(SncfContent.t_sncf_rames.Columns.ID_VARIANTE.getName(),     this.varianteId);
        returnValue.put(SncfContent.t_sncf_rames.Columns.EAB.getName(),             this.eab);
        returnValue.put(SncfContent.t_sncf_rames.Columns.CS_GRIFFE.getName(),       this.csGriffe);

        return returnValue;
    }

    @Override
    public String getPrimaryKeyName() {
        return SncfContent.t_sncf_rames.Columns.ID.getName();
    }
}
