package com.sncf.android.entity;

import android.content.ContentValues;
import android.database.Cursor;

import com.google.gson.annotations.SerializedName;
import com.sncf.android.data.JsonModel;
import com.sncf.android.orm.Entity;
import com.sncf.android.provider.SncfContent;
import com.sncf.android.utils.ProviderHelper;

/**
 * Created by favre on 23/01/2014.
 */
public class RepairEntity extends Entity
{
    @SerializedName(JsonModel.t_gr_reparations.ID)
    public Integer      idRepair;

    @SerializedName(JsonModel.t_gr_reparations.REPARATION_CODE)
    public Integer      code;

    @SerializedName(JsonModel.t_gr_reparations.REPARATION_NOM)
    public String       name;

    @SerializedName(JsonModel.t_gr_reparations.VALIDE)
    public Boolean      isValid;

    public RepairEntity()               {                           }
    public RepairEntity(Cursor cursor)  { this.buildEntity(cursor); }

    @Override
    public void buildEntity(Cursor cursor) {
        primaryKey = ProviderHelper.getInt(cursor, SncfContent.t_gr_reparations.Columns.ID.getName());
        idRepair = ProviderHelper.getInt(cursor, SncfContent.t_gr_reparations.Columns.ID_REPARATION.getName());
        code = ProviderHelper.getInt(cursor, SncfContent.t_gr_reparations.Columns.REPARATION_CODE.getName());
        name = ProviderHelper.getString(cursor, SncfContent.t_gr_reparations.Columns.REPARATION_NOM.getName());
        isValid = ProviderHelper.getBoolean(cursor, SncfContent.t_gr_reparations.Columns.VALIDE.getName());
    }

    @Override
    public ContentValues toContentValues() {
        ContentValues values = new ContentValues();
        values.put(SncfContent.t_gr_reparations.Columns.ID_REPARATION.getName(), idRepair);
        values.put(SncfContent.t_gr_reparations.Columns.REPARATION_CODE.getName(), code);
        values.put(SncfContent.t_gr_reparations.Columns.REPARATION_NOM.getName(), name);
        values.put(SncfContent.t_gr_reparations.Columns.VALIDE.getName(), isValid);
        return values;
    }

    @Override
    public String getPrimaryKeyName() {
        return SncfContent.t_gr_reparations.Columns.ID.getName();
    }
}
