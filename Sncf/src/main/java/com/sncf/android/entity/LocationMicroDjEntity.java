package com.sncf.android.entity;

import android.content.ContentValues;
import android.database.Cursor;

import com.google.gson.annotations.SerializedName;
import com.sncf.android.data.JsonModel;
import com.sncf.android.orm.Entity;
import com.sncf.android.provider.SncfContent;
import com.sncf.android.utils.ProviderHelper;

/**
 * Created by favre on 23/01/2014.
 */
public class LocationMicroDjEntity extends Entity
{
    @SerializedName(JsonModel.t_gr_localisations_mdj.ID)
    public Integer idLocationMicroDj;

    @SerializedName(JsonModel.t_gr_localisations_mdj.LOCALISATION_MDJ)
    public String name;

    public LocationMicroDjEntity()              {                           }
    public LocationMicroDjEntity(Cursor cursor) { this.buildEntity(cursor); }

    @Override
    public void buildEntity(Cursor cursor) {

        this.primaryKey         = cursor.getInt(cursor.getColumnIndex(SncfContent.t_gr_localisations_mdj.Columns.ID.getName()));

        this.idLocationMicroDj  = ProviderHelper.getInt(cursor,     SncfContent.t_gr_localisations_mdj.Columns.ID_LOCALISATIONS_MDJ.getName());
        this.name               = ProviderHelper.getString(cursor,  SncfContent.t_gr_localisations_mdj.Columns.LOCALISATION_MDJ.getName());


    }

    @Override
    public ContentValues toContentValues() {

        ContentValues contentValues = new ContentValues();

        contentValues.put(SncfContent.t_gr_localisations_mdj.Columns.ID_LOCALISATIONS_MDJ.getName(), this.idLocationMicroDj);
        contentValues.put(SncfContent.t_gr_localisations_mdj.Columns.LOCALISATION_MDJ.getName(), this.name);
         return contentValues;
    }

    @Override
    public String getPrimaryKeyName() {
        return SncfContent.t_gr_localisations_mdj.Columns.ID.getName();
    }
}
