package com.sncf.android.entity;

import android.content.ContentValues;
import android.database.Cursor;

import com.google.gson.annotations.SerializedName;
import com.sncf.android.data.JsonModel;
import com.sncf.android.orm.Entity;
import com.sncf.android.provider.SncfContent;
import com.sncf.android.utils.ProviderHelper;

/**
 * Created by favre on 23/01/2014.
 */
public class LevelFourEntity extends Entity
{
    @SerializedName(JsonModel.t_gr_niveau_4.ID)
    public Integer      idLevelFour;

    @SerializedName(JsonModel.t_gr_niveau_4.NIVEAU_4)
    public Integer      id;

    @SerializedName(JsonModel.t_gr_niveau_4.ENSEMBLE)
    public Integer      setId;

    @SerializedName(JsonModel.t_gr_niveau_4.NIVEAU_3)
    public Integer      levelThreeId;

    @SerializedName(JsonModel.t_gr_niveau_4.FONCTION)
    public Integer      functionId;

    @SerializedName(JsonModel.t_gr_niveau_4.CODE_SERIE)
    public String       seriesCode;

    @SerializedName(JsonModel.t_gr_niveau_4.LIBELLE_NIVEAU)
    public String       name;

    public LevelFourEntity()              {                           }
    public LevelFourEntity(Cursor cursor) { this.buildEntity(cursor); }

    @Override
    public void buildEntity(Cursor cursor) {

        this.primaryKey     = cursor.getInt(cursor.getColumnIndex(SncfContent.t_gr_niveau_4.Columns.ID.getName()));

        this.idLevelFour    = ProviderHelper.getInt(cursor,     SncfContent.t_gr_niveau_4.Columns.ID_NIVEAU_4.getName());
        this.id             = ProviderHelper.getInt(cursor,     SncfContent.t_gr_niveau_4.Columns.NIVEAU_4.getName());
        this.setId          = ProviderHelper.getInt(cursor,     SncfContent.t_gr_niveau_4.Columns.ENSEMBLE.getName());
        this.levelThreeId   = ProviderHelper.getInt(cursor,     SncfContent.t_gr_niveau_4.Columns.NIVEAU_3.getName());
        this.functionId     = ProviderHelper.getInt(cursor,     SncfContent.t_gr_niveau_4.Columns.FONCTION.getName());
        this.seriesCode     = ProviderHelper.getString(cursor,  SncfContent.t_gr_niveau_4.Columns.CODE_SERIE.getName());
        this.name           = ProviderHelper.getString(cursor,  SncfContent.t_gr_niveau_4.Columns.LIBELLE_NIVEAU.getName());
    }

    @Override
    public ContentValues toContentValues() {

        ContentValues returnValue = new ContentValues();

        returnValue.put(SncfContent.t_gr_niveau_4.Columns.ID_NIVEAU_4.getName(),    this.idLevelFour);
        returnValue.put(SncfContent.t_gr_niveau_4.Columns.NIVEAU_4.getName(),       this.id);
        returnValue.put(SncfContent.t_gr_niveau_4.Columns.ENSEMBLE.getName(),       this.setId);
        returnValue.put(SncfContent.t_gr_niveau_4.Columns.NIVEAU_3.getName(),       this.levelThreeId);
        returnValue.put(SncfContent.t_gr_niveau_4.Columns.FONCTION.getName(),       this.functionId);
        returnValue.put(SncfContent.t_gr_niveau_4.Columns.CODE_SERIE.getName(),     this.seriesCode);
        returnValue.put(SncfContent.t_gr_niveau_4.Columns.LIBELLE_NIVEAU.getName(), this.name);

        return returnValue;
    }

    @Override
    public String getPrimaryKeyName() {
        return SncfContent.t_gr_niveau_4.Columns.ID.getName();
    }
}
