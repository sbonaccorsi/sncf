package com.sncf.android.entity;

import android.content.ContentValues;
import android.database.Cursor;

import com.google.gson.annotations.SerializedName;
import com.sncf.android.data.JsonModel;
import com.sncf.android.orm.Entity;
import com.sncf.android.provider.SncfContent;
import com.sncf.android.utils.ProviderHelper;

/**
 * Created by favre on 23/01/2014.
 */
public class UOEntity extends Entity
{
    @SerializedName(JsonModel.t_sncf_uo.ID)
    public Integer      idUO;

    @SerializedName(JsonModel.t_sncf_uo.UO_COURT)
    public String       shortName;

    @SerializedName(JsonModel.t_sncf_uo.UO_LONG)
    public String       name;

    @SerializedName(JsonModel.t_sncf_uo.ID_STF)
    public Integer      idSTF;

    public UOEntity()               {                           }
    public UOEntity(Cursor cursor)  { this.buildEntity(cursor); }

    @Override
    public void buildEntity(Cursor cursor) {
        primaryKey = ProviderHelper.getInt(cursor, SncfContent.t_sncf_uo.Columns.ID.getName());
        idUO = ProviderHelper.getInt(cursor, SncfContent.t_sncf_uo.Columns.ID_UO.getName());
        shortName = ProviderHelper.getString(cursor, SncfContent.t_sncf_uo.Columns.UO_COURT.getName());
        name = ProviderHelper.getString(cursor, SncfContent.t_sncf_uo.Columns.UO_LONG.getName());
        idSTF = ProviderHelper.getInt(cursor, SncfContent.t_sncf_uo.Columns.ID_STF.getName());
    }

    @Override
    public ContentValues toContentValues() {
        ContentValues values = new ContentValues();
        values.put(SncfContent.t_sncf_uo.Columns.ID_UO.getName(), idUO);
        values.put(SncfContent.t_sncf_uo.Columns.UO_COURT.getName(), shortName);
        values.put(SncfContent.t_sncf_uo.Columns.UO_LONG.getName(), name);
        values.put(SncfContent.t_sncf_uo.Columns.ID_STF.getName(), idSTF);
        return values;
    }

    @Override
    public String getPrimaryKeyName() {
        return SncfContent.t_sncf_uo.Columns.ID.getName();
    }
}
