package com.sncf.android.entity;

import android.content.ContentValues;
import android.database.Cursor;

import com.google.gson.annotations.SerializedName;
import com.sncf.android.data.JsonModel;
import com.sncf.android.orm.Entity;
import com.sncf.android.provider.SncfContent;
import com.sncf.android.utils.ProviderHelper;

/**
 * Created by favre on 23/01/2014.
 */
public class SubSeriesEntity extends Entity
{
    @SerializedName(JsonModel.t_sncf_subserie.ID)
    public Integer          id_sub_serie;

    @SerializedName(JsonModel.t_sncf_subserie.ID_SERIE)
    public Integer          id_serie;

    @SerializedName(JsonModel.t_sncf_subserie.SOUS_SERIE)
    public String           sous_serie;

    @SerializedName(JsonModel.t_sncf_subserie.CS_HL)
    public String           cs_hl;

    @SerializedName(JsonModel.t_sncf_subserie.CS_PRG)
    public String           cs_prg;

    @SerializedName(JsonModel.t_sncf_subserie.LCN)
    public String           lcn;

    @SerializedName(JsonModel.t_sncf_subserie.SOUS_SERIE_TANDEM)
    public String           sous_serie_tandem;

    public SubSeriesEntity()               {                           }
    public SubSeriesEntity(Cursor cursor)  { this.buildEntity(cursor); }

    @Override
    public void buildEntity(Cursor cursor) {
        primaryKey          = ProviderHelper.getInt(cursor, SncfContent.t_sncf_subseries.Columns.ID.getName());
        id_sub_serie        = ProviderHelper.getInt(cursor, SncfContent.t_sncf_subseries.Columns.ID_SUBSERIE.getName());
        id_serie            = ProviderHelper.getInt(cursor, SncfContent.t_sncf_subseries.Columns.ID_SERIE.getName());
        sous_serie          = ProviderHelper.getString(cursor, SncfContent.t_sncf_subseries.Columns.SUBSERIE.getName());
        cs_hl               = ProviderHelper.getString(cursor, SncfContent.t_sncf_subseries.Columns.CS_HL.getName());
        cs_prg              = ProviderHelper.getString(cursor, SncfContent.t_sncf_subseries.Columns.CS_PRG.getName());
        lcn                 = ProviderHelper.getString(cursor, SncfContent.t_sncf_subseries.Columns.LCN.getName());
        sous_serie_tandem   = ProviderHelper.getString(cursor, SncfContent.t_sncf_subseries.Columns.SUB_SERIE_TANDEM.getName());
    }

    @Override
    public ContentValues toContentValues() {
        ContentValues values = new ContentValues();
        values.put(SncfContent.t_sncf_subseries.Columns.ID_SUBSERIE.getName(), id_sub_serie);
        values.put(SncfContent.t_sncf_subseries.Columns.ID_SERIE.getName(), id_serie);
        values.put(SncfContent.t_sncf_subseries.Columns.SUBSERIE.getName(), sous_serie);
        values.put(SncfContent.t_sncf_subseries.Columns.CS_HL.getName(), cs_hl);
        values.put(SncfContent.t_sncf_subseries.Columns.CS_PRG.getName(), cs_prg);
        values.put(SncfContent.t_sncf_subseries.Columns.LCN.getName(), lcn);
        values.put(SncfContent.t_sncf_subseries.Columns.SUB_SERIE_TANDEM.getName(), sous_serie_tandem);
        return values;
    }

    @Override
    public String getPrimaryKeyName() {
        return SncfContent.t_sncf_subseries.Columns.ID.getName();
    }
}
