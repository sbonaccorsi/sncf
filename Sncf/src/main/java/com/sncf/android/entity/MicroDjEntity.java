package com.sncf.android.entity;

import android.content.ContentValues;
import android.database.Cursor;

import com.google.gson.annotations.SerializedName;
import com.sncf.android.data.JsonModel;
import com.sncf.android.orm.Entity;
import com.sncf.android.provider.SncfContent;
import com.sncf.android.utils.ProviderHelper;

/**
 * Created by favre on 23/01/2014.
 */
public class MicroDjEntity extends Entity
{
    @SerializedName(JsonModel.t_gr_micro_dj.ID)
    public Integer      idMicroDj;

    @SerializedName(JsonModel.t_gr_micro_dj.TYPE_MDJ)
    public Integer      type;

    @SerializedName(JsonModel.t_gr_micro_dj.ID_LOCALISATION)
    public Integer      locationId;

    @SerializedName(JsonModel.t_gr_micro_dj.LIBELLE)
    public String       name;

    public MicroDjEntity()              {                           }
    public MicroDjEntity(Cursor cursor) { this.buildEntity(cursor); }

    @Override
    public void buildEntity(Cursor cursor) {

        this.primaryKey         = cursor.getInt(cursor.getColumnIndex(SncfContent.t_gr_micro_dj.Columns.ID.getName()));

        this.idMicroDj          = ProviderHelper.getInt(cursor,     SncfContent.t_gr_micro_dj.Columns.ID_MICRO_DJ.getName());
        this.type               = ProviderHelper.getInt(cursor,     SncfContent.t_gr_micro_dj.Columns.TYPE_MDJ.getName());
        this.name               = ProviderHelper.getString(cursor,  SncfContent.t_gr_micro_dj.Columns.LIBELLE.getName());
        this.locationId         = ProviderHelper.getInt(cursor,     SncfContent.t_gr_micro_dj.Columns.ID_LOCALISATION.getName());


    }

    @Override
    public ContentValues toContentValues() {

        ContentValues returnValue = new ContentValues();

        returnValue.put(SncfContent.t_gr_micro_dj.Columns.ID_MICRO_DJ.getName(),        this.idMicroDj);
        returnValue.put(SncfContent.t_gr_micro_dj.Columns.TYPE_MDJ.getName(),           this.type);
        returnValue.put(SncfContent.t_gr_micro_dj.Columns.LIBELLE.getName(),            this.name);
        returnValue.put(SncfContent.t_gr_micro_dj.Columns.ID_LOCALISATION.getName(),    this.locationId);

        return returnValue;
    }

    @Override
    public String getPrimaryKeyName() {
        return SncfContent.t_gr_micro_dj.Columns.ID.getName();
    }
}
