package com.sncf.android.entity;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.google.gson.annotations.SerializedName;
import com.sncf.android.data.JsonModel;
import com.sncf.android.orm.DAL;
import com.sncf.android.orm.Entity;
import com.sncf.android.provider.SncfContent;
import com.sncf.android.utils.ProviderHelper;

/**
 * Created by zzzwist on 24/01/14.
 */
public class SemelleDataEntity extends Entity {

    @SerializedName(JsonModel.t_sem_data.ID)
    public Integer          idData;

    @SerializedName(JsonModel.t_sem_data.ID_BOGIE)
    public Integer          idBogie;

    @SerializedName(JsonModel.t_sem_data.NUM_ROUE)
    public Integer          numWheel;

    @SerializedName(JsonModel.t_sem_data.CONFORMITE)
    public Boolean          conformity;

    @SerializedName(JsonModel.t_sem_data.REMPLACEMENT)
    public Boolean          replacement;

    @SerializedName(JsonModel.t_sem_data.CTRL_VISUEL)
    public Boolean          controlVisual;

    @SerializedName(JsonModel.t_sem_data.MESURE)
    public Integer          measure;

    @SerializedName(JsonModel.t_sem_data.VALIDE_MESURE)
    public Boolean          validateMeasure;


    public SemelleDataEntity()              {                           }
    public SemelleDataEntity(Cursor cursor) { this.buildEntity(cursor); }

    public static SemelleDataEntity createFromBogieId(Context context, int id_bogie) {

        //TODO
        //Maybe more than one data when you get the data by bogie id
        //Because a bogie have 4 wheel so 4 wear

        Cursor cursor = DAL.read(context, SncfContent.t_sem_data.CONTENT_URI,
                SncfContent.t_sem_data.Columns.ID_BOGIE.getName() + "=" + id_bogie);
        SemelleDataEntity semelleDataEntity = null;
        if (cursor.moveToFirst()) {
            semelleDataEntity = new SemelleDataEntity(cursor);
        }
        cursor.close();
        return semelleDataEntity;
    }

    @Override
    public void buildEntity(Cursor cursor) {
        primaryKey          = ProviderHelper.getInt(cursor, SncfContent.t_sem_data.Columns.ID.getName());
        idData              = ProviderHelper.getInt(cursor, SncfContent.t_sem_data.Columns.ID_DATA.getName());
        idBogie             = ProviderHelper.getInt(cursor, SncfContent.t_sem_data.Columns.ID_BOGIE.getName());
        numWheel            = ProviderHelper.getInt(cursor, SncfContent.t_sem_data.Columns.NUM_ROUE.getName());
        conformity          = ProviderHelper.getBoolean(cursor, SncfContent.t_sem_data.Columns.CONFORMITE.getName());
        replacement         = ProviderHelper.getBoolean(cursor, SncfContent.t_sem_data.Columns.REMPLACEMENT.getName());
        controlVisual       = ProviderHelper.getBoolean(cursor, SncfContent.t_sem_data.Columns.CTRL_VISUEL.getName());
        measure             = ProviderHelper.getInt(cursor, SncfContent.t_sem_data.Columns.MESURE.getName());
        validateMeasure     = ProviderHelper.getBoolean(cursor, SncfContent.t_sem_data.Columns.VALIDE_MESURE.getName());
    }

    @Override
    public ContentValues toContentValues() {
        ContentValues values = new ContentValues();
        values.put(SncfContent.t_sem_data.Columns.ID_DATA.getName(), idData);
        values.put(SncfContent.t_sem_data.Columns.ID_BOGIE.getName(), idBogie);
        values.put(SncfContent.t_sem_data.Columns.NUM_ROUE.getName(), numWheel);
        values.put(SncfContent.t_sem_data.Columns.CONFORMITE.getName(), conformity);
        values.put(SncfContent.t_sem_data.Columns.REMPLACEMENT.getName(), replacement);
        values.put(SncfContent.t_sem_data.Columns.CTRL_VISUEL.getName(), controlVisual);
        values.put(SncfContent.t_sem_data.Columns.MESURE.getName(), measure);
        values.put(SncfContent.t_sem_data.Columns.VALIDE_MESURE.getName(), validateMeasure);
        return values;
    }

    @Override
    public String getPrimaryKeyName() {
        return SncfContent.t_sem_data.Columns.ID.getName();
    }
}
