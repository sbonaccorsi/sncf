package com.sncf.android.entity;

import android.content.ContentValues;
import android.database.Cursor;

import com.google.gson.annotations.SerializedName;
import com.sncf.android.data.JsonModel;
import com.sncf.android.orm.Entity;
import com.sncf.android.provider.SncfContent;
import com.sncf.android.utils.ProviderHelper;

/**
 * Created by favre on 23/01/2014.
 */
public class STFEntity extends Entity
{
    @SerializedName(JsonModel.t_sncf_stf.ID)
    public Integer      idSTF;

    @SerializedName(JsonModel.t_sncf_stf.STF)
    public String       name;

    public STFEntity()              {                           }
    public STFEntity(Cursor cursor) { this.buildEntity(cursor); }

    @Override
    public void buildEntity(Cursor cursor) {
        primaryKey = ProviderHelper.getInt(cursor, SncfContent.t_sncf_stf.Columns.ID.getName());
        idSTF = ProviderHelper.getInt(cursor, SncfContent.t_sncf_stf.Columns.ID_STF.getName());
        name = ProviderHelper.getString(cursor, SncfContent.t_sncf_stf.Columns.STF.getName());
    }

    @Override
    public ContentValues toContentValues() {
        ContentValues values = new ContentValues();
        values.put(SncfContent.t_sncf_stf.Columns.ID_STF.getName(), idSTF);
        values.put(SncfContent.t_sncf_stf.Columns.STF.getName(), name);
        return values;
    }

    @Override
    public String getPrimaryKeyName() {
        return SncfContent.t_sncf_stf.Columns.ID.getName();
    }
}
