package com.sncf.android.entity;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.google.gson.annotations.SerializedName;
import com.sncf.android.data.JsonModel;
import com.sncf.android.orm.Entity;
import com.sncf.android.provider.SncfContent;
import com.sncf.android.utils.ProviderHelper;

/**
 * Created by zzzwist on 27/12/13.
 */
public class RestrictionEntity extends Entity
{
    @SerializedName(JsonModel.t_gr_restriction_mvc.ID_REST)
    public Integer              id_restriction;

    @SerializedName(JsonModel.t_gr_restriction_mvc.NUM_REST)
    public Integer              num_restriction;

    @SerializedName(JsonModel.t_gr_restriction_mvc.SPECIFICITE)
    public String               specificite;

    @SerializedName(JsonModel.t_gr_restriction_mvc.TYPE_REST)
    public Integer              type_restriction;

    @SerializedName(JsonModel.t_gr_restriction_mvc.LIB_REDUIT)
    public String               lib_reduit;

    @SerializedName(JsonModel.t_gr_restriction_mvc.SITE_POSEUR)
    public String               site_poseur;

    @SerializedName(JsonModel.t_gr_restriction_mvc.NUM_RAME)
    public String               num_rame;

    @SerializedName(JsonModel.t_gr_restriction_mvc.DATE_POSE)
    public String               date_pose;

    @SerializedName(JsonModel.t_gr_restriction_mvc.PRIORITE)
    public Integer              priorite;

    @SerializedName(JsonModel.t_gr_restriction_mvc.COMMENTAIRE)
    public String               commentaire;

    @SerializedName(JsonModel.t_gr_restriction_mvc.LOCALISATION)
    public String               localisation;

    @SerializedName(JsonModel.t_gr_restriction_mvc.AMORTI)
    public Boolean              amorti;

    @SerializedName(JsonModel.t_gr_restriction_mvc.DATE_AMORTISSEMENT)
    public String               date_amortissement;

    @SerializedName(JsonModel.t_gr_restriction_mvc.LOG_AMORTISSEMENT)
    public String               log_amortissement;

    @SerializedName(JsonModel.t_gr_restriction_mvc.LOG_POSEUR)
    public String               log_poseur;

    @SerializedName(JsonModel.t_gr_restriction_mvc.COMMENTAIRE_M)
    public String               commentaire_m;

    @SerializedName(JsonModel.t_gr_restriction_mvc.DATE_JOURNALISATION)
    public String               date_journalisation;

    @SerializedName(JsonModel.t_gr_restriction_mvc.SERIE_ID)
    public Integer              serie_id;

    @SerializedName(JsonModel.t_gr_restriction_mvc.ID_RAME)
    public Integer              id_rame;

    @SerializedName(JsonModel.t_gr_restriction_mvc.DATE_IMPORT)
    public String               date_import;

    @SerializedName(JsonModel.t_gr_restriction_mvc.DATE_MAJ)
    public String               date_maj;

    public RestrictionEntity()               {                           }
    public RestrictionEntity(Cursor cursor)  { this.buildEntity(cursor); }

    @Override
    public void buildEntity(Cursor cursor) {
        primaryKey = ProviderHelper.getInt(cursor, SncfContent.t_gr_restriction_mvc.Columns.ID.getName());
        id_restriction = ProviderHelper.getInt(cursor, SncfContent.t_gr_restriction_mvc.Columns.ID_REST.getName());
        num_restriction = ProviderHelper.getInt(cursor, SncfContent.t_gr_restriction_mvc.Columns.NUM_REST.getName());
        specificite = ProviderHelper.getString(cursor, SncfContent.t_gr_restriction_mvc.Columns.SPECIFICITE.getName());
        type_restriction = ProviderHelper.getInt(cursor, SncfContent.t_gr_restriction_mvc.Columns.TYPE_REST.getName());
        lib_reduit = ProviderHelper.getString(cursor, SncfContent.t_gr_restriction_mvc.Columns.LIB_REDUIT.getName());
        site_poseur = ProviderHelper.getString(cursor, SncfContent.t_gr_restriction_mvc.Columns.SITE_POSEUR.getName());
        num_rame = ProviderHelper.getString(cursor, SncfContent.t_gr_restriction_mvc.Columns.NUM_RAME.getName());
        date_pose = ProviderHelper.getString(cursor, SncfContent.t_gr_restriction_mvc.Columns.DATE_POSE.getName());
        priorite = ProviderHelper.getInt(cursor, SncfContent.t_gr_restriction_mvc.Columns.PRIORITE.getName());
        commentaire = ProviderHelper.getString(cursor, SncfContent.t_gr_restriction_mvc.Columns.COMMENTAIRE.getName());
        localisation = ProviderHelper.getString(cursor, SncfContent.t_gr_restriction_mvc.Columns.LOCALISATION.getName());
        amorti = ProviderHelper.getBoolean(cursor, SncfContent.t_gr_restriction_mvc.Columns.AMORTI.getName());
        date_amortissement = ProviderHelper.getString(cursor, SncfContent.t_gr_restriction_mvc.Columns.DATE_AMORTISSEMENT.getName());
        log_amortissement = ProviderHelper.getString(cursor, SncfContent.t_gr_restriction_mvc.Columns.LOG_AMORTISSEMENT.getName());
        log_poseur = ProviderHelper.getString(cursor, SncfContent.t_gr_restriction_mvc.Columns.LOG_POSEUR.getName());
        commentaire_m = ProviderHelper.getString(cursor, SncfContent.t_gr_restriction_mvc.Columns.COMMENTAIRE_M.getName());
        date_journalisation = ProviderHelper.getString(cursor, SncfContent.t_gr_restriction_mvc.Columns.DATE_JOURNALISATION.getName());
        serie_id = ProviderHelper.getInt(cursor, SncfContent.t_gr_restriction_mvc.Columns.SERIE_ID.getName());
        id_rame = ProviderHelper.getInt(cursor, SncfContent.t_gr_restriction_mvc.Columns.ID_RAME.getName());
        date_import = ProviderHelper.getString(cursor, SncfContent.t_gr_restriction_mvc.Columns.DATE_IMPORT.getName());
        date_maj = ProviderHelper.getString(cursor, SncfContent.t_gr_restriction_mvc.Columns.DATE_MAJ.getName());
    }

    @Override
    public ContentValues toContentValues() {
        ContentValues contentValues = new ContentValues();
        contentValues.put(SncfContent.t_gr_restriction_mvc.Columns.ID_REST.getName(), id_restriction);
        contentValues.put(SncfContent.t_gr_restriction_mvc.Columns.NUM_REST.getName(), num_restriction);
        contentValues.put(SncfContent.t_gr_restriction_mvc.Columns.SPECIFICITE.getName(), specificite);
        contentValues.put(SncfContent.t_gr_restriction_mvc.Columns.TYPE_REST.getName(), type_restriction);
        contentValues.put(SncfContent.t_gr_restriction_mvc.Columns.LIB_REDUIT.getName(), lib_reduit);
        contentValues.put(SncfContent.t_gr_restriction_mvc.Columns.SITE_POSEUR.getName(), site_poseur);
        contentValues.put(SncfContent.t_gr_restriction_mvc.Columns.NUM_RAME.getName(), num_rame);
        contentValues.put(SncfContent.t_gr_restriction_mvc.Columns.DATE_POSE.getName(), date_pose);
        contentValues.put(SncfContent.t_gr_restriction_mvc.Columns.PRIORITE.getName(), priorite);
        contentValues.put(SncfContent.t_gr_restriction_mvc.Columns.COMMENTAIRE.getName(), commentaire);
        contentValues.put(SncfContent.t_gr_restriction_mvc.Columns.LOCALISATION.getName(), localisation);
        contentValues.put(SncfContent.t_gr_restriction_mvc.Columns.AMORTI.getName(), amorti);
        contentValues.put(SncfContent.t_gr_restriction_mvc.Columns.DATE_AMORTISSEMENT.getName(), date_amortissement);
        contentValues.put(SncfContent.t_gr_restriction_mvc.Columns.LOG_AMORTISSEMENT.getName(), log_amortissement);
        contentValues.put(SncfContent.t_gr_restriction_mvc.Columns.LOG_POSEUR.getName(), log_poseur);
        contentValues.put(SncfContent.t_gr_restriction_mvc.Columns.COMMENTAIRE_M.getName(), commentaire_m);
        contentValues.put(SncfContent.t_gr_restriction_mvc.Columns.DATE_JOURNALISATION.getName(), date_journalisation);
        contentValues.put(SncfContent.t_gr_restriction_mvc.Columns.SERIE_ID.getName(), serie_id);
        contentValues.put(SncfContent.t_gr_restriction_mvc.Columns.ID_RAME.getName(), id_rame);
        contentValues.put(SncfContent.t_gr_restriction_mvc.Columns.DATE_IMPORT.getName(), date_import);
        contentValues.put(SncfContent.t_gr_restriction_mvc.Columns.DATE_MAJ.getName(), date_maj);
        return contentValues;
    }

    @Override
    public String getPrimaryKeyName() {
        return SncfContent.t_gr_restriction_mvc.Columns.ID.getName();
    }
}
