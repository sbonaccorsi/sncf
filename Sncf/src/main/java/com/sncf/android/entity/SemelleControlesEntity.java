package com.sncf.android.entity;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.google.gson.annotations.SerializedName;
import com.sncf.android.data.JsonModel;
import com.sncf.android.orm.DAL;
import com.sncf.android.orm.Entity;
import com.sncf.android.provider.SncfContent;
import com.sncf.android.utils.ProviderHelper;

import java.util.Collection;

/**
 * Created by zzzwist on 24/01/14.
 */
public class SemelleControlesEntity extends Entity {

    @SerializedName(JsonModel.t_sem_controles.ID)
    public Integer          idControl;

    @SerializedName(JsonModel.t_sem_controles.DATE_CTRL)
    public String           dateControl;

    @SerializedName(JsonModel.t_sem_controles.ID_RAME)
    public Integer          idRame;

    @SerializedName(JsonModel.t_sem_controles.ID_INTERVENTION)
    public Integer          idIntervention;

    @SerializedName(JsonModel.t_sem_controles.ID_SITE)
    public Integer          idSite;

    @SerializedName(JsonModel.t_sem_controles.CONTROLER_NAME)
    public String           ControlerName;


    public Collection<CompoEntity>  compos;
    public RameEntity               rame;
    public InterventionEntity       intervention;

    public SemelleControlesEntity()              {                           }
    public SemelleControlesEntity(Cursor cursor) { this.buildEntity(cursor); }

    public static SemelleControlesEntity createFromIdControl(Context context, int id_control) {
        Cursor cursor = DAL.read(context, SncfContent.t_sem_controles.CONTENT_URI,
                SncfContent.t_sem_controles.Columns.ID_CONTROLE.getName() + "=" + id_control);
        SemelleControlesEntity semelleControlesEntity = null;
        if (cursor.moveToFirst()) {
            semelleControlesEntity = new SemelleControlesEntity(cursor);
        }
        cursor.close();
        return semelleControlesEntity;
    }

    @Override
    public void buildEntity(Cursor cursor) {
        primaryKey = ProviderHelper.getInt(cursor, SncfContent.t_sem_controles.Columns.ID.getName());
        idControl = ProviderHelper.getInt(cursor, SncfContent.t_sem_controles.Columns.ID_CONTROLE.getName());
        dateControl = ProviderHelper.getString(cursor, SncfContent.t_sem_controles.Columns.DATE_CTRL.getName());
        idRame = ProviderHelper.getInt(cursor, SncfContent.t_sem_controles.Columns.ID_RAME.getName());
        idIntervention = ProviderHelper.getInt(cursor, SncfContent.t_sem_controles.Columns.ID_INTERVENTION.getName());
        idSite = ProviderHelper.getInt(cursor, SncfContent.t_sem_controles.Columns.ID_SITE.getName());
        ControlerName = ProviderHelper.getString(cursor, SncfContent.t_sem_controles.Columns.CONTROLER_NAME.getName());
    }

    @Override
    public ContentValues toContentValues() {
        ContentValues values = new ContentValues();
        values.put(SncfContent.t_sem_controles.Columns.ID_CONTROLE.getName(), idControl);
        values.put(SncfContent.t_sem_controles.Columns.DATE_CTRL.getName(), dateControl);
        values.put(SncfContent.t_sem_controles.Columns.ID_RAME.getName(), idRame);
        values.put(SncfContent.t_sem_controles.Columns.ID_INTERVENTION.getName(), idIntervention);
        values.put(SncfContent.t_sem_controles.Columns.ID_SITE.getName(), idSite);
        values.put(SncfContent.t_sem_controles.Columns.CONTROLER_NAME.getName(), ControlerName);
        return values;
    }

    @Override
    public String getPrimaryKeyName() {
        return SncfContent.t_sem_controles.Columns.ID.getName();
    }
}
