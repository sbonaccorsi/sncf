package com.sncf.android.entity;

import android.content.ContentValues;
import android.database.Cursor;
import com.google.gson.annotations.SerializedName;
import com.sncf.android.data.JsonModel;
import com.sncf.android.orm.Entity;
import com.sncf.android.provider.SncfContent;
import com.sncf.android.utils.ProviderHelper;

/**
 * Created by favre on 23/01/2014.
 */
public class AnomalyEntity extends Entity
{
    @SerializedName(JsonModel.t_gr_anomalies.ID)
    public Integer  idAnomaly;

    @SerializedName(JsonModel.t_gr_anomalies.ANOM_CODE)
    public Integer  code;

    @SerializedName(JsonModel.t_gr_anomalies.ANOM_NOM)
    public String   name;

    @SerializedName(JsonModel.t_gr_anomalies.VALIDE)
    public Boolean  isValid;

    public AnomalyEntity()              {                           }
    public AnomalyEntity(Cursor cursor) { this.buildEntity(cursor); }

    @Override
    public void buildEntity(Cursor cursor) {

        this.primaryKey = cursor.getInt(cursor.getColumnIndex(SncfContent.t_gr_anomalies.Columns.ID.getName()));

        this.idAnomaly  = ProviderHelper.getInt(cursor,     SncfContent.t_gr_anomalies.Columns.ID_ANOMALY.getName());
        this.code       = ProviderHelper.getInt(cursor,     SncfContent.t_gr_anomalies.Columns.ANOM_CODE.getName());
        this.name       = ProviderHelper.getString(cursor,  SncfContent.t_gr_anomalies.Columns.ANOM_NOM.getName());
        this.isValid    = ProviderHelper.getBoolean(cursor, SncfContent.t_gr_anomalies.Columns.VALIDE.getName());
    }

    @Override
    public ContentValues toContentValues() {

        ContentValues returnValue = new ContentValues();

        returnValue.put(SncfContent.t_gr_anomalies.Columns.ID_ANOMALY.getName(),    this.idAnomaly);
        returnValue.put(SncfContent.t_gr_anomalies.Columns.ANOM_CODE.getName(),     this.code);
        returnValue.put(SncfContent.t_gr_anomalies.Columns.ANOM_NOM.getName(),      this.name);
        returnValue.put(SncfContent.t_gr_anomalies.Columns.VALIDE.getName(),        this.isValid);

        return returnValue;
    }

    @Override
    public String getPrimaryKeyName() {
        return SncfContent.t_gr_anomalies.Columns.ID.getName();
    }
}
