package com.sncf.android.entity;

import com.google.gson.annotations.SerializedName;
import com.sncf.android.data.JsonModel;

import java.util.ArrayList;

/**
 * Created by zzzwist on 24/01/14.
 */
public class ParentModelEntity {

    @SerializedName(JsonModel.ParentModel.t_sncf_stf.T_SNCF_STF)
    public ArrayList<STFEntity>                 t_sncf_stf;

    @SerializedName(JsonModel.ParentModel.t_sncf_uo.T_SNCF_UO)
    public ArrayList<UOEntity>                  t_sncf_uo;

    @SerializedName(JsonModel.ParentModel.t_sncf_sites.T_SNCF_SITES)
    public ArrayList<SiteEntity>                t_sncf_sites;

    @SerializedName(JsonModel.ParentModel.t_gr_specialities.T_GR_SPECIALITIES)
    public ArrayList<SpecialtyEntity>           t_gr_specialties;

    @SerializedName(JsonModel.ParentModel.t_sncf_link_specialities.T_SNCF_LINK_SPECIALITIES)
    public ArrayList<LinkSpecialtyEntity>       t_sncf_link_specialties;

    @SerializedName(JsonModel.ParentModel.t_sncf_link_series.T_SNCF_LINK_SERIES)
    public ArrayList<LinkSeriesEntity>          t_sncf_link_series;

    @SerializedName(JsonModel.ParentModel.t_sncf_rames.T_SNCF_RAMES)
    public ArrayList<RameEntity>                t_sncf_rames;

    @SerializedName(JsonModel.ParentModel.t_sncf_compos.T_SNCF_COMPOS)
    public ArrayList<CompoEntity>               t_sncf_compos;

    @SerializedName(JsonModel.ParentModel.t_gr_interventions.T_GR_INTERVENTIONS)
    public ArrayList<InterventionEntity>        t_gr_interventions;

    @SerializedName(JsonModel.ParentModel.t_gr_fonctions.T_GR_FONCTIONS)
    public ArrayList<FunctionEntity>            t_gr_functions;

    @SerializedName(JsonModel.ParentModel.t_gr_ensembles.T_GR_ENSEMBLES)
    public ArrayList<SetEntity>                 t_gr_sets;

    @SerializedName(JsonModel.ParentModel.t_gr_niveau_3.T_GR_NIVEAU_3)
    public ArrayList<LevelThreeEntity>          t_gr_level_3;

    @SerializedName(JsonModel.ParentModel.t_gr_niveau_4.T_GR_NIVEAU_4)
    public ArrayList<LevelFourEntity>           t_gr_level_4;

    @SerializedName(JsonModel.ParentModel.t_gr_niveau_5.T_GR_NIVEAU_5)
    public ArrayList<LevelFiveEntity>           t_gr_level_5;

    @SerializedName(JsonModel.ParentModel.t_gr_niveau_6.T_GR_NIVEAU_6)
    public ArrayList<LevelSixEntity>            t_gr_level_6;

    @SerializedName(JsonModel.ParentModel.t_gr_anomalies.T_GR_ANOMALIES)
    public ArrayList<AnomalyEntity>             t_gr_anomalies;

    @SerializedName(JsonModel.ParentModel.t_gr_reparations.T_GR_REPARATIONS)
    public ArrayList<RepairEntity>              t_gr_repairs;

    @SerializedName(JsonModel.ParentModel.t_gr_conclusions.T_GR_CONCLUSION)
    public ArrayList<ConclusionEntity>          t_gr_conclusions;

    @SerializedName(JsonModel.ParentModel.t_gr_micro_dj.T_GR_MICRO_DJ)
    public ArrayList<MicroDjEntity>             t_gr_microdjs;

    @SerializedName(JsonModel.ParentModel.t_gr_localisations_mdj.T_GR_LOCALISATIONS_MDJ)
    public ArrayList<LocationMicroDjEntity>     t_gr_locations_microdjs;

    @SerializedName(JsonModel.ParentModel.t_sncf_series.T_SNCF_SERIES)
    public ArrayList<SeriesEntity>              t_sncf_series;

    @SerializedName(JsonModel.ParentModel.t_gr_cri.T_GR_CRI)
    public ArrayList<CRIEntity>                 t_gr_cris;

    @SerializedName(JsonModel.ParentModel.t_sem_controles.T_SEM_CONTROLES)
    public ArrayList<SemelleControlesEntity>    t_sem_controles;

    @SerializedName(JsonModel.ParentModel.t_sem_bogie.T_SEM_BOGIE)
    public ArrayList<SemelleBogieEntity>        t_sem_bogie;

    @SerializedName(JsonModel.ParentModel.t_sem_data.T_SEM_DATA)
    public ArrayList<SemelleDataEntity>         t_sem_data;

    @SerializedName(JsonModel.ParentModel.t_gr_restriction_mvc.T_GR_RESTRICTION_MVC)
    public ArrayList<RestrictionEntity>         t_gr_restrictions;

    @SerializedName(JsonModel.ParentModel.t_gr_specificite.T_GR_SPECIFICITE)
    public ArrayList<SpecificiteEntity>         t_gr_specificities;
}
