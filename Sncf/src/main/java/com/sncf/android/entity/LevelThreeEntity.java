package com.sncf.android.entity;

import android.content.ContentValues;
import android.database.Cursor;

import com.google.gson.annotations.SerializedName;
import com.sncf.android.data.JsonModel;
import com.sncf.android.orm.Entity;
import com.sncf.android.provider.SncfContent;
import com.sncf.android.utils.ProviderHelper;

/**
 * Created by favre on 23/01/2014.
 */
public class LevelThreeEntity extends Entity
{
    @SerializedName(JsonModel.t_gr_niveau_3.ID)
    public Integer      idLevelThree;

    @SerializedName(JsonModel.t_gr_niveau_3.NIVEAU_3)
    public Integer      id;

    @SerializedName(JsonModel.t_gr_niveau_3.ENSEMBLE)
    public Integer      setId;

    @SerializedName(JsonModel.t_gr_niveau_3.FONCTION)
    public Integer      functionId;

    @SerializedName(JsonModel.t_gr_niveau_3.CODE_SERIE)
    public String       seriesCode;

    @SerializedName(JsonModel.t_gr_niveau_3.LIBELLE_NIVEAU)
    public String       name;

    public LevelThreeEntity()                 {                           }
    public LevelThreeEntity(Cursor cursor)    { this.buildEntity(cursor); }

    @Override
    public void buildEntity(Cursor cursor) {

        this.primaryKey     = cursor.getInt(cursor.getColumnIndex(SncfContent.t_gr_niveau_3.Columns.ID.getName()));

        this.idLevelThree   = ProviderHelper.getInt(cursor,     SncfContent.t_gr_niveau_3.Columns.ID_NIVEAU_3.getName());
        this.id             = ProviderHelper.getInt(cursor,     SncfContent.t_gr_niveau_3.Columns.NIVEAU_3.getName());
        this.setId          = ProviderHelper.getInt(cursor,     SncfContent.t_gr_niveau_3.Columns.ENSEMBLE.getName());
        this.functionId     = ProviderHelper.getInt(cursor,     SncfContent.t_gr_niveau_3.Columns.FONCTION.getName());
        this.seriesCode     = ProviderHelper.getString(cursor,  SncfContent.t_gr_niveau_3.Columns.CODE_SERIE.getName());
        this.name           = ProviderHelper.getString(cursor,  SncfContent.t_gr_niveau_3.Columns.LIBELLE_NIVEAU.getName());

    }

    @Override
    public ContentValues toContentValues() {

        ContentValues returnValue = new ContentValues();

        returnValue.put(SncfContent.t_gr_niveau_3.Columns.ID_NIVEAU_3.getName(),    this.idLevelThree);
        returnValue.put(SncfContent.t_gr_niveau_3.Columns.NIVEAU_3.getName(),       this.id);
        returnValue.put(SncfContent.t_gr_niveau_3.Columns.ENSEMBLE.getName(),       this.setId);
        returnValue.put(SncfContent.t_gr_niveau_3.Columns.FONCTION.getName(),       this.functionId);
        returnValue.put(SncfContent.t_gr_niveau_3.Columns.CODE_SERIE.getName(),     this.seriesCode);
        returnValue.put(SncfContent.t_gr_niveau_3.Columns.LIBELLE_NIVEAU.getName(), this.name);

        return returnValue;
    }

    @Override
    public String getPrimaryKeyName() {
        return SncfContent.t_gr_niveau_3.Columns.ID.getName();
    }
}
