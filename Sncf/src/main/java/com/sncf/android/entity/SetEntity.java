package com.sncf.android.entity;

import android.content.ContentValues;
import android.database.Cursor;

import com.google.gson.annotations.SerializedName;
import com.sncf.android.data.JsonModel;
import com.sncf.android.orm.Entity;
import com.sncf.android.provider.SncfContent;
import com.sncf.android.utils.ProviderHelper;

/**
 * Created by favre on 23/01/2014.
 */
public class SetEntity extends Entity
{
    @SerializedName(JsonModel.t_gr_ensembles.ID)
    public Integer      idSet;

    @SerializedName(JsonModel.t_gr_ensembles.FONCTION)
    public Integer      function;

    @SerializedName(JsonModel.t_gr_ensembles.ENSEMBLE)
    public Integer      set;

    @SerializedName(JsonModel.t_gr_ensembles.CODE_SERIE)
    public String       seriesCode;

    @SerializedName(JsonModel.t_gr_ensembles.LIBELLE_NIVEAU)
    public String       name;

    public SetEntity()              {                           }
    public SetEntity(Cursor cursor) { this.buildEntity(cursor); }

    @Override
    public void buildEntity(Cursor cursor) {
        primaryKey = ProviderHelper.getInt(cursor, SncfContent.t_gr_ensembles.Columns.ID.getName());
        idSet = ProviderHelper.getInt(cursor, SncfContent.t_gr_ensembles.Columns.ID_ENSEMBLE.getName());
        function = ProviderHelper.getInt(cursor, SncfContent.t_gr_ensembles.Columns.FONCTION.getName());
        set = ProviderHelper.getInt(cursor, SncfContent.t_gr_ensembles.Columns.ENSEMBLE.getName());
        seriesCode = ProviderHelper.getString(cursor, SncfContent.t_gr_ensembles.Columns.CODE_SERIE.getName());
        name = ProviderHelper.getString(cursor, SncfContent.t_gr_ensembles.Columns.LIBELLE_NIVEAU.getName());
    }

    @Override
    public ContentValues toContentValues() {
        ContentValues values = new ContentValues();
        values.put(SncfContent.t_gr_ensembles.Columns.ID_ENSEMBLE.getName(), idSet);
        values.put(SncfContent.t_gr_ensembles.Columns.FONCTION.getName(), function);
        values.put(SncfContent.t_gr_ensembles.Columns.ENSEMBLE.getName(), set);
        values.put(SncfContent.t_gr_ensembles.Columns.CODE_SERIE.getName(), seriesCode);
        values.put(SncfContent.t_gr_ensembles.Columns.LIBELLE_NIVEAU.getName(), name);
        return values;
    }

    @Override
    public String getPrimaryKeyName() {
        return SncfContent.t_gr_ensembles.Columns.ID.getName();
    }
}
