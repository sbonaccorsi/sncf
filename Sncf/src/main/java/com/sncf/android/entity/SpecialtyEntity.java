package com.sncf.android.entity;

import android.content.ContentValues;
import android.database.Cursor;

import com.google.gson.annotations.SerializedName;
import com.sncf.android.data.JsonModel;
import com.sncf.android.orm.Entity;
import com.sncf.android.provider.SncfContent;
import com.sncf.android.utils.ProviderHelper;

/**
 * Created by favre on 23/01/2014.
 */
public class SpecialtyEntity extends Entity
{
    @SerializedName(JsonModel.t_gr_specialities.ID)
    public Integer          idSpeciality;

    @SerializedName(JsonModel.t_gr_specialities.SPECIALITY)
    public String           name;

    public SpecialtyEntity()                {                           }
    public SpecialtyEntity(Cursor cursor)   { this.buildEntity(cursor); }

    @Override
    public void buildEntity(Cursor cursor) {
        primaryKey = ProviderHelper.getInt(cursor, SncfContent.t_gr_specialities.Columns.ID.getName());
        idSpeciality = ProviderHelper.getInt(cursor, SncfContent.t_gr_specialities.Columns.ID_SPECIALITY.getName());
        name = ProviderHelper.getString(cursor, SncfContent.t_gr_specialities.Columns.SPECIALITY.getName());
    }

    @Override
    public ContentValues toContentValues() {
        ContentValues values = new ContentValues();
        values.put(SncfContent.t_gr_specialities.Columns.ID_SPECIALITY.getName(), idSpeciality);
        values.put(SncfContent.t_gr_specialities.Columns.SPECIALITY.getName(), name);
        return values;
    }

    @Override
    public String getPrimaryKeyName() {
        return SncfContent.t_gr_specialities.Columns.ID.getName();
    }
}
