package com.sncf.android.entity;

import android.content.ContentValues;
import android.database.Cursor;

import com.google.gson.annotations.SerializedName;
import com.sncf.android.data.JsonModel;
import com.sncf.android.orm.Entity;
import com.sncf.android.provider.SncfContent;
import com.sncf.android.utils.ProviderHelper;

/**
 * Created by favre on 23/01/2014.
 */
public class SiteEntity extends Entity
{
    @SerializedName(JsonModel.t_sncf_sites.ID)
    public Integer      idSite;

    @SerializedName(JsonModel.t_sncf_sites.ID_UO)
    public Integer      uoId;

    @SerializedName(JsonModel.t_sncf_sites.SITE)
    public String       name;

    @SerializedName(JsonModel.t_sncf_sites.SPECIALISTE)
    public Boolean      isSpecialize;

    public SiteEntity()                 {                           }
    public SiteEntity(Cursor cursor)    { this.buildEntity(cursor); }

    @Override
    public void buildEntity(Cursor cursor) {
        primaryKey = ProviderHelper.getInt(cursor, SncfContent.t_sncf_sites.Columns.ID.getName());
        idSite = ProviderHelper.getInt(cursor, SncfContent.t_sncf_sites.Columns.ID_SITES.getName());
        uoId = ProviderHelper.getInt(cursor, SncfContent.t_sncf_sites.Columns.ID_UO.getName());
        name = ProviderHelper.getString(cursor, SncfContent.t_sncf_sites.Columns.SITE.getName());
        isSpecialize = ProviderHelper.getBoolean(cursor, SncfContent.t_sncf_sites.Columns.SPECIALISTE.getName());
    }

    @Override
    public ContentValues toContentValues() {
        ContentValues values = new ContentValues();
        values.put(SncfContent.t_sncf_sites.Columns.ID_SITES.getName(), idSite);
        values.put(SncfContent.t_sncf_sites.Columns.ID_UO.getName(), uoId);
        values.put(SncfContent.t_sncf_sites.Columns.SITE.getName(), name);
        values.put(SncfContent.t_sncf_sites.Columns.SPECIALISTE.getName(), isSpecialize);
        return values;
    }

    @Override
    public String getPrimaryKeyName() {
        return SncfContent.t_sncf_sites.Columns.ID.getName();
    }
}
