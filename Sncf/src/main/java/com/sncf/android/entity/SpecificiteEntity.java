package com.sncf.android.entity;

import android.content.ContentValues;
import android.database.Cursor;

import com.google.gson.annotations.SerializedName;
import com.sncf.android.data.JsonModel;
import com.sncf.android.orm.Entity;
import com.sncf.android.provider.SncfContent;
import com.sncf.android.utils.ProviderHelper;

/**
 * Created by favre on 23/01/2014.
 */
public class SpecificiteEntity extends Entity
{
    @SerializedName(JsonModel.t_gr_specificite.ID)
    public Integer      idSpecificite;

    @SerializedName(JsonModel.t_gr_specificite.SPECIFICITE)
    public String       name;

    public SpecificiteEntity()              {                           }
    public SpecificiteEntity(Cursor cursor) { this.buildEntity(cursor); }

    @Override
    public void buildEntity(Cursor cursor) {
        primaryKey = ProviderHelper.getInt(cursor, SncfContent.t_gr_specificite.Columns.ID.getName());
        idSpecificite = ProviderHelper.getInt(cursor, SncfContent.t_gr_specificite.Columns.ID_SPECIFICITE.getName());
        name = ProviderHelper.getString(cursor, SncfContent.t_gr_specificite.Columns.SPECIFICITE.getName());
    }

    @Override
    public ContentValues toContentValues() {
        ContentValues values = new ContentValues();
        values.put(SncfContent.t_gr_specificite.Columns.ID_SPECIFICITE.getName(), idSpecificite);
        values.put(SncfContent.t_gr_specificite.Columns.SPECIFICITE.getName(), name);
        return values;
    }

    @Override
    public String getPrimaryKeyName() {
        return SncfContent.t_gr_specificite.Columns.ID.getName();
    }
}
