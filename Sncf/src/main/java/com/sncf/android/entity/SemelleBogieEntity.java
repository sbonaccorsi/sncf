package com.sncf.android.entity;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.google.gson.annotations.SerializedName;
import com.sncf.android.data.JsonModel;
import com.sncf.android.orm.Entity;
import com.sncf.android.provider.SncfContent;
import com.sncf.android.utils.ProviderHelper;

import java.util.Collection;

/**
 * Created by zzzwist on 24/01/14.
 */
public class SemelleBogieEntity extends Entity {

    @SerializedName(JsonModel.t_sem_bogie.ID)
    public Integer          idBogie;

    @SerializedName(JsonModel.t_sem_bogie.ID_CONTROL)
    public Integer          idControl;

    @SerializedName(JsonModel.t_sem_bogie.ID_COMPO)
    public Integer          idCompo;

    @SerializedName(JsonModel.t_sem_bogie.BOGIE_IDENTIFIER)
    public Integer          bogieIdentifier;

    @SerializedName(JsonModel.t_sem_bogie.BOGIE_ISOLE)
    public Boolean          bogieIsole;

    @SerializedName(JsonModel.t_sem_bogie.BM_SOLE)
    public Boolean          bmSole;

    public Collection<SemelleDataEntity> datas;

    public SemelleBogieEntity() {}
    public SemelleBogieEntity(Cursor cursor) { this.buildEntity(cursor); }

    @Override
    public void buildEntity(Cursor cursor) {
        primaryKey = ProviderHelper.getInt(cursor, SncfContent.t_sem_bogie.Columns.ID.getName());
        idBogie = ProviderHelper.getInt(cursor, SncfContent.t_sem_bogie.Columns.ID_BOGIE.getName());
        idControl = ProviderHelper.getInt(cursor, SncfContent.t_sem_bogie.Columns.ID_CONTROL.getName());
        idCompo = ProviderHelper.getInt(cursor, SncfContent.t_sem_bogie.Columns.ID_COMPO.getName());
        bogieIdentifier = ProviderHelper.getInt(cursor, SncfContent.t_sem_bogie.Columns.BOGIE_IDENTIFIER.getName());
        bogieIsole = ProviderHelper.getBoolean(cursor, SncfContent.t_sem_bogie.Columns.BOGIE_ISOLE.getName());
        bmSole = ProviderHelper.getBoolean(cursor, SncfContent.t_sem_bogie.Columns.BM_SOLE.getName());
    }

    @Override
    public ContentValues toContentValues() {
        ContentValues values = new ContentValues();
        values.put(SncfContent.t_sem_bogie.Columns.ID_BOGIE.getName(), idBogie);
        values.put(SncfContent.t_sem_bogie.Columns.ID_CONTROL.getName(), idControl);
        values.put(SncfContent.t_sem_bogie.Columns.ID_COMPO.getName(), idCompo);
        values.put(SncfContent.t_sem_bogie.Columns.BOGIE_IDENTIFIER.getName(), bogieIdentifier);
        values.put(SncfContent.t_sem_bogie.Columns.BOGIE_ISOLE.getName(), bogieIsole);
        values.put(SncfContent.t_sem_bogie.Columns.BM_SOLE.getName(), bmSole);
        return values;
    }

    @Override
    public String getPrimaryKeyName() {
        return SncfContent.t_sem_bogie.Columns.ID.getName();
    }
}
