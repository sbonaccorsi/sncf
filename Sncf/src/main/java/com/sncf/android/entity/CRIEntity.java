package com.sncf.android.entity;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.google.gson.annotations.SerializedName;
import com.sncf.android.data.JsonModel;
import com.sncf.android.orm.Entity;
import com.sncf.android.provider.SncfContent;
import com.sncf.android.utils.ProviderHelper;

/**
 * Created by zzzwist on 30/12/13.
 */
public class CRIEntity extends Entity
{
    @SerializedName(JsonModel.t_gr_cri.ID)
    public Integer  idCRI;

    @SerializedName(JsonModel.t_gr_cri.NUM_CRI)
    public Integer  num;

    @SerializedName(JsonModel.t_gr_cri.IND_CRI)
    public Integer      ind;

    @SerializedName(JsonModel.t_gr_cri.ID_RAME)
    public Integer      rameId;

    @SerializedName(JsonModel.t_gr_cri.ID_RAME_UM)
    public Integer      rameUMId;

    @SerializedName(JsonModel.t_gr_cri.ID_VEHICULE)
    public Integer      compoId;

    @SerializedName(JsonModel.t_gr_cri.ID_CONCLUSION)
    public Integer      conclusionId;

    @SerializedName(JsonModel.t_gr_cri.ID_FONCTION)
    public Integer      functionId;

    @SerializedName(JsonModel.t_gr_cri.ID_ENSEMBLE)
    public Integer      setId;

    @SerializedName(JsonModel.t_gr_cri.ID_NIVEAU_3)
    public Integer      levelThreeId;

    @SerializedName(JsonModel.t_gr_cri.ID_NIVEAU_4)
    public Integer      levelFourId;

    @SerializedName(JsonModel.t_gr_cri.ID_NIVEAU_5)
    public Integer      levelFiveId;

    @SerializedName(JsonModel.t_gr_cri.ID_NIVEAU_6)
    public Integer      levelSixId;

    @SerializedName(JsonModel.t_gr_cri.ID_ANOMALIE)
    public Integer      anomalyId;

    @SerializedName(JsonModel.t_gr_cri.ID_REPARATION)
    public Integer      repairId;

    @SerializedName(JsonModel.t_gr_cri.NUM_CHANTIER)
    public Integer      numChantier;

    @SerializedName(JsonModel.t_gr_cri.ID_RESTRICTION)
    public Integer      restrictionId;

    @SerializedName(JsonModel.t_gr_cri.SERVEUR_GRIFFE)
    public Integer      griffeServer;

    @SerializedName(JsonModel.t_gr_cri.MDJ1)
    public Integer      MicroDj1;

    @SerializedName(JsonModel.t_gr_cri.MDJ2)
    public Integer      MicroDj2;

    @SerializedName(JsonModel.t_gr_cri.MDJ3)
    public Integer      MicroDj3;

    @SerializedName(JsonModel.t_gr_cri.MDJ4)
    public Integer      MicroDj4;

    @SerializedName(JsonModel.t_gr_cri.MDJ5)
    public Integer      MicroDj5;

    @SerializedName(JsonModel.t_gr_cri.MDJ6)
    public Integer      MicroDj6;

    @SerializedName(JsonModel.t_gr_cri.MDJ7)
    public Integer      MicroDj7;

    @SerializedName(JsonModel.t_gr_cri.MDJ8)
    public Integer      MicroDj8;

    @SerializedName(JsonModel.t_gr_cri.MDJ9)
    public Integer      MicroDj9;

    @SerializedName(JsonModel.t_gr_cri.MDJ10)
    public Integer      MicroDj10;

    @SerializedName(JsonModel.t_gr_cri.MDJ11)
    public Integer      MicroDj11;

    @SerializedName(JsonModel.t_gr_cri.MDJ12)
    public Integer      MicroDj12;

    @SerializedName(JsonModel.t_gr_cri.DATE_INTERVENTION)
    public String   interventionDate;

    @SerializedName(JsonModel.t_gr_cri.INTERVENANT)
    public String   intervenerName;

    @SerializedName(JsonModel.t_gr_cri.TYPE_OPERATION)
    public String   operationType;

    @SerializedName(JsonModel.t_gr_cri.LIBELLE)
    public String   label;

    @SerializedName(JsonModel.t_gr_cri.DEPECHE)
    public String   despatch;

    @SerializedName(JsonModel.t_gr_cri.BS)
    public String   BS;

    @SerializedName(JsonModel.t_gr_cri.COMMENTAIRE)
    public String   comment;

    @SerializedName(JsonModel.t_gr_cri.DATE_SIGNALEMENT)
    public String   signalDate;

    @SerializedName(JsonModel.t_gr_cri.CS_GRIFFE)
    public String   csGriffe;

    @SerializedName(JsonModel.t_gr_cri.SPECIALITE)
    public String   specialty;

    public CRIEntity()              {                           }
    public CRIEntity(Cursor cursor) { this.buildEntity(cursor); }


    @Override
    public void buildEntity(Cursor cursor) {

        this.primaryKey         = cursor.getInt(cursor.getColumnIndex(SncfContent.t_gr_cri.Columns.ID.getName()));

        this.idCRI              = ProviderHelper.getInt(cursor,     SncfContent.t_gr_cri.Columns.ID_CRI.getName()           );
        this.num                = ProviderHelper.getInt(cursor,     SncfContent.t_gr_cri.Columns.NUM_CRI.getName()          );
        this.ind                = ProviderHelper.getInt(cursor,     SncfContent.t_gr_cri.Columns.IND_CRI.getName()          );
        this.rameId             = ProviderHelper.getInt(cursor,     SncfContent.t_gr_cri.Columns.ID_RAME.getName()          );
        this.rameUMId           = ProviderHelper.getInt(cursor,     SncfContent.t_gr_cri.Columns.ID_RAME_UM.getName()       );
        this.compoId            = ProviderHelper.getInt(cursor,     SncfContent.t_gr_cri.Columns.ID_VEHICULE.getName()      );
        this.conclusionId       = ProviderHelper.getInt(cursor,     SncfContent.t_gr_cri.Columns.ID_CONCLUSION.getName()    );
        this.functionId         = ProviderHelper.getInt(cursor,     SncfContent.t_gr_cri.Columns.ID_FONCTION.getName()      );
        this.setId              = ProviderHelper.getInt(cursor,     SncfContent.t_gr_cri.Columns.ID_ENSEMBLE.getName()      );
        this.levelThreeId       = ProviderHelper.getInt(cursor,     SncfContent.t_gr_cri.Columns.ID_NIVEAU_3.getName()      );
        this.levelFourId        = ProviderHelper.getInt(cursor,     SncfContent.t_gr_cri.Columns.ID_NIVEAU_4.getName()      );
        this.levelFiveId        = ProviderHelper.getInt(cursor,     SncfContent.t_gr_cri.Columns.ID_NIVEAU_5.getName()      );
        this.levelSixId         = ProviderHelper.getInt(cursor,     SncfContent.t_gr_cri.Columns.ID_NIVEAU_6.getName()      );
        this.anomalyId          = ProviderHelper.getInt(cursor,     SncfContent.t_gr_cri.Columns.ID_ANOMALIE.getName()      );
        this.repairId           = ProviderHelper.getInt(cursor,     SncfContent.t_gr_cri.Columns.ID_REPARATION.getName()    );
        this.numChantier        = ProviderHelper.getInt(cursor,     SncfContent.t_gr_cri.Columns.NUM_CHANTIER.getName()     );
        this.restrictionId      = ProviderHelper.getInt(cursor,     SncfContent.t_gr_cri.Columns.ID_RESTRICTION.getName()   );
        this.griffeServer       = ProviderHelper.getInt(cursor,     SncfContent.t_gr_cri.Columns.SERVEUR_GRIFFE.getName()   );
        this.MicroDj1           = ProviderHelper.getInt(cursor,     SncfContent.t_gr_cri.Columns.MDJ1.getName()             );
        this.MicroDj2           = ProviderHelper.getInt(cursor,     SncfContent.t_gr_cri.Columns.MDJ2.getName()             );
        this.MicroDj3           = ProviderHelper.getInt(cursor,     SncfContent.t_gr_cri.Columns.MDJ3.getName()             );
        this.MicroDj4           = ProviderHelper.getInt(cursor,     SncfContent.t_gr_cri.Columns.MDJ4.getName()             );
        this.MicroDj5           = ProviderHelper.getInt(cursor,     SncfContent.t_gr_cri.Columns.MDJ5.getName()             );
        this.MicroDj6           = ProviderHelper.getInt(cursor,     SncfContent.t_gr_cri.Columns.MDJ6.getName()             );
        this.MicroDj7           = ProviderHelper.getInt(cursor,     SncfContent.t_gr_cri.Columns.MDJ7.getName()             );
        this.MicroDj8           = ProviderHelper.getInt(cursor,     SncfContent.t_gr_cri.Columns.MDJ8.getName()             );
        this.MicroDj9           = ProviderHelper.getInt(cursor,     SncfContent.t_gr_cri.Columns.MDJ9.getName()             );
        this.MicroDj10          = ProviderHelper.getInt(cursor,     SncfContent.t_gr_cri.Columns.MDJ10.getName()            );
        this.MicroDj11          = ProviderHelper.getInt(cursor,     SncfContent.t_gr_cri.Columns.MDJ11.getName()            );
        this.MicroDj12          = ProviderHelper.getInt(cursor,     SncfContent.t_gr_cri.Columns.MDJ12.getName()            );
        this.interventionDate   = ProviderHelper.getString(cursor,  SncfContent.t_gr_cri.Columns.DATE_INTERVENTION.getName());
        this.intervenerName     = ProviderHelper.getString(cursor,  SncfContent.t_gr_cri.Columns.INTERVENANT.getName()      );
        this.operationType      = ProviderHelper.getString(cursor,  SncfContent.t_gr_cri.Columns.TYPE_OPERATION.getName()   );
        this.label              = ProviderHelper.getString(cursor,  SncfContent.t_gr_cri.Columns.LIBELLE.getName()          );
        this.despatch           = ProviderHelper.getString(cursor,  SncfContent.t_gr_cri.Columns.DEPECHE.getName()          );
        this.BS                 = ProviderHelper.getString(cursor,  SncfContent.t_gr_cri.Columns.BS.getName()               );
        this.comment            = ProviderHelper.getString(cursor,  SncfContent.t_gr_cri.Columns.COMMENTAIRE.getName()      );
        this.signalDate         = ProviderHelper.getString(cursor,  SncfContent.t_gr_cri.Columns.DATE_SIGNALEMENT.getName() );
        this.csGriffe           = ProviderHelper.getString(cursor,  SncfContent.t_gr_cri.Columns.CS_GRIFFE.getName()        );
        this.specialty          = ProviderHelper.getString(cursor,  SncfContent.t_gr_cri.Columns.SPECIALITE.getName()       );

    }

    @Override
    public ContentValues toContentValues() {

        ContentValues returnValue = new ContentValues();

        returnValue.put(SncfContent.t_gr_cri.Columns.ID_CRI.getName(),              this.idCRI              );
        returnValue.put(SncfContent.t_gr_cri.Columns.NUM_CRI.getName(),             this.num                );
        returnValue.put(SncfContent.t_gr_cri.Columns.IND_CRI.getName(),             this.ind                );
        returnValue.put(SncfContent.t_gr_cri.Columns.ID_RAME.getName(),             this.rameId             );
        returnValue.put(SncfContent.t_gr_cri.Columns.ID_RAME_UM.getName(),          this.rameUMId           );
        returnValue.put(SncfContent.t_gr_cri.Columns.ID_VEHICULE.getName(),         this.compoId            );
        returnValue.put(SncfContent.t_gr_cri.Columns.ID_CONCLUSION.getName(),       this.conclusionId       );
        returnValue.put(SncfContent.t_gr_cri.Columns.ID_FONCTION.getName(),         this.functionId         );
        returnValue.put(SncfContent.t_gr_cri.Columns.ID_ENSEMBLE.getName(),         this.setId              );
        returnValue.put(SncfContent.t_gr_cri.Columns.ID_NIVEAU_3.getName(),         this.levelThreeId       );
        returnValue.put(SncfContent.t_gr_cri.Columns.ID_NIVEAU_4.getName(),         this.levelFourId        );
        returnValue.put(SncfContent.t_gr_cri.Columns.ID_NIVEAU_5.getName(),         this.levelFiveId        );
        returnValue.put(SncfContent.t_gr_cri.Columns.ID_NIVEAU_6.getName(),         this.levelSixId         );
        returnValue.put(SncfContent.t_gr_cri.Columns.ID_ANOMALIE.getName(),         this.anomalyId          );
        returnValue.put(SncfContent.t_gr_cri.Columns.ID_REPARATION.getName(),       this.repairId           );
        returnValue.put(SncfContent.t_gr_cri.Columns.NUM_CHANTIER.getName(),        this.numChantier        );
        returnValue.put(SncfContent.t_gr_cri.Columns.ID_RESTRICTION.getName(),      this.restrictionId      );
        returnValue.put(SncfContent.t_gr_cri.Columns.SERVEUR_GRIFFE.getName(),      this.griffeServer       );
        returnValue.put(SncfContent.t_gr_cri.Columns.MDJ1.getName(),                this.MicroDj1           );
        returnValue.put(SncfContent.t_gr_cri.Columns.MDJ2.getName(),                this.MicroDj2           );
        returnValue.put(SncfContent.t_gr_cri.Columns.MDJ3.getName(),                this.MicroDj3           );
        returnValue.put(SncfContent.t_gr_cri.Columns.MDJ4.getName(),                this.MicroDj4           );
        returnValue.put(SncfContent.t_gr_cri.Columns.MDJ5.getName(),                this.MicroDj5           );
        returnValue.put(SncfContent.t_gr_cri.Columns.MDJ6.getName(),                this.MicroDj6           );
        returnValue.put(SncfContent.t_gr_cri.Columns.MDJ7.getName(),                this.MicroDj7           );
        returnValue.put(SncfContent.t_gr_cri.Columns.MDJ8.getName(),                this.MicroDj8           );
        returnValue.put(SncfContent.t_gr_cri.Columns.MDJ9.getName(),                this.MicroDj9           );
        returnValue.put(SncfContent.t_gr_cri.Columns.MDJ10.getName(),               this.MicroDj10          );
        returnValue.put(SncfContent.t_gr_cri.Columns.MDJ11.getName(),               this.MicroDj11          );
        returnValue.put(SncfContent.t_gr_cri.Columns.MDJ12.getName(),               this.MicroDj12          );
        returnValue.put(SncfContent.t_gr_cri.Columns.DATE_INTERVENTION.getName(),   this.interventionDate   );
        returnValue.put(SncfContent.t_gr_cri.Columns.INTERVENANT.getName(),         this.intervenerName     );
        returnValue.put(SncfContent.t_gr_cri.Columns.TYPE_OPERATION.getName(),      this.operationType      );
        returnValue.put(SncfContent.t_gr_cri.Columns.LIBELLE.getName(),             this.label              );
        returnValue.put(SncfContent.t_gr_cri.Columns.DEPECHE.getName(),             this.despatch           );
        returnValue.put(SncfContent.t_gr_cri.Columns.BS.getName(),                  this.BS                 );
        returnValue.put(SncfContent.t_gr_cri.Columns.COMMENTAIRE.getName(),         this.comment            );
        returnValue.put(SncfContent.t_gr_cri.Columns.DATE_SIGNALEMENT.getName(),    this.signalDate         );
        returnValue.put(SncfContent.t_gr_cri.Columns.CS_GRIFFE.getName(),           this.csGriffe           );
        returnValue.put(SncfContent.t_gr_cri.Columns.SPECIALITE.getName(),          this.specialty          );



        return returnValue;
    }

    @Override
    public String getPrimaryKeyName() {
        return SncfContent.t_gr_cri.Columns.ID.getName();
    }
}
