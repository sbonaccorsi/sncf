package com.sncf.android.entity;

import android.content.ContentValues;
import android.database.Cursor;

import com.google.gson.annotations.SerializedName;
import com.sncf.android.data.JsonModel;
import com.sncf.android.orm.Entity;
import com.sncf.android.provider.SncfContent;
import com.sncf.android.utils.ProviderHelper;

/**
 * Created by favre on 23/01/2014.
 */
public class LinkSeriesEntity extends Entity
{

    @SerializedName(JsonModel.t_sncf_link_series.ID)
    public Integer      idLinkSeries;

    @SerializedName(JsonModel.t_sncf_link_series.ID_STF)
    public Integer      stfId;

    @SerializedName(JsonModel.t_sncf_link_series.ID_SERIE)
    public Integer      seriesId;

    @SerializedName(JsonModel.t_sncf_link_series.ID_UNDER_SERIE)
    public Integer      subSeriesId;

    @SerializedName(JsonModel.t_sncf_link_series.CS_GRIFFE)
    public String       csGriffe;

    public LinkSeriesEntity()               {                           }
    public LinkSeriesEntity(Cursor cursor)  { this.buildEntity(cursor); }

    @Override
    public void buildEntity(Cursor cursor) {

        this.primaryKey     = cursor.getInt(cursor.getColumnIndex(SncfContent.t_sncf_link_series.Columns.ID.getName()));

        this.idLinkSeries   = ProviderHelper.getInt(cursor,     SncfContent.t_sncf_link_series.Columns.ID_LINK_SERIE.getName());
        this.stfId          = ProviderHelper.getInt(cursor,     SncfContent.t_sncf_link_series.Columns.ID_STF.getName());
        this.seriesId       = ProviderHelper.getInt(cursor,     SncfContent.t_sncf_link_series.Columns.ID_SERIE.getName());
        this.subSeriesId    = ProviderHelper.getInt(cursor,     SncfContent.t_sncf_link_series.Columns.ID_UNDER_SERIE.getName());
        this.csGriffe       = ProviderHelper.getString(cursor,  SncfContent.t_sncf_link_series.Columns.CS_GRIFFE.getName());
    }

    @Override
    public ContentValues toContentValues() {

        ContentValues returnValue = new ContentValues();

        returnValue.put(SncfContent.t_sncf_link_series.Columns.ID_LINK_SERIE.getName(),     this.idLinkSeries);
        returnValue.put(SncfContent.t_sncf_link_series.Columns.ID_STF.getName(),            this.stfId);
        returnValue.put(SncfContent.t_sncf_link_series.Columns.ID_SERIE.getName(),          this.seriesId);
        returnValue.put(SncfContent.t_sncf_link_series.Columns.ID_UNDER_SERIE.getName(),    this.subSeriesId);
        returnValue.put(SncfContent.t_sncf_link_series.Columns.CS_GRIFFE.getName(),         this.csGriffe);

        return returnValue;
    }

    @Override
    public String getPrimaryKeyName() {
        return SncfContent.t_sncf_link_series.Columns.ID.getName();
    }
}
