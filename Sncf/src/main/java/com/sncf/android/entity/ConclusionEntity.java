package com.sncf.android.entity;

import android.content.ContentValues;
import android.database.Cursor;

import com.google.gson.annotations.SerializedName;
import com.sncf.android.data.JsonModel;
import com.sncf.android.orm.Entity;
import com.sncf.android.provider.SncfContent;
import com.sncf.android.utils.ProviderHelper;

/**
 * Created by favre on 23/01/2014.
 */
public class ConclusionEntity extends Entity
{

    @SerializedName(JsonModel.t_gr_conclusions.ID)
    public Integer  idConclusion;

    @SerializedName(JsonModel.t_gr_conclusions.CONCLUSION_CRI)
    public String   name;

    @SerializedName(JsonModel.t_gr_conclusions.VALIDE)
    public Boolean  isValid;

    public ConclusionEntity()               {                           }
    public ConclusionEntity(Cursor cursor)  { this.buildEntity(cursor); }

    @Override
    public void buildEntity(Cursor cursor) {
        this.primaryKey = cursor.getInt(cursor.getColumnIndex(SncfContent.t_gr_conclusions.Columns.ID.getName()));

        this.idConclusion   = ProviderHelper.getInt(cursor,     SncfContent.t_gr_conclusions.Columns.ID_CONCLUSION.getName());
        this.name           = ProviderHelper.getString(cursor,  SncfContent.t_gr_conclusions.Columns.CONCLUSION_CRI.getName());
        this.isValid        = ProviderHelper.getBoolean(cursor, SncfContent.t_gr_conclusions.Columns.VALIDE.getName());
    }

    @Override
    public ContentValues toContentValues() {

        ContentValues returnValue = new ContentValues();
        returnValue.put(SncfContent.t_gr_conclusions.Columns.ID_CONCLUSION.getName(),   idConclusion);
        returnValue.put(SncfContent.t_gr_conclusions.Columns.CONCLUSION_CRI.getName(),  this.name);
        returnValue.put(SncfContent.t_gr_conclusions.Columns.VALIDE.getName(),          this.isValid);

        return returnValue;
    }

    @Override
    public String getPrimaryKeyName() {
        return SncfContent.t_gr_conclusions.Columns.ID.getName();
    }
}
