package com.sncf.android.entity;

import android.content.ContentValues;
import android.database.Cursor;

import com.google.gson.annotations.SerializedName;
import com.sncf.android.data.JsonModel;
import com.sncf.android.orm.Entity;
import com.sncf.android.provider.SncfContent;
import com.sncf.android.utils.ProviderHelper;

/**
 * Created by favre on 23/01/2014.
 */
public class InterventionEntity extends Entity
{

    @SerializedName(JsonModel.t_gr_interventions.ID)
    public Integer idIntervention;

    @SerializedName(JsonModel.t_gr_interventions.INTERVENTION)
    public String name;

    public InterventionEntity()                 {                           }
    public InterventionEntity(Cursor cursor)    { this.buildEntity(cursor); }

    @Override
    public void buildEntity(Cursor cursor) {

        this.primaryKey     = cursor.getInt(cursor.getColumnIndex(SncfContent.t_gr_interventions.Columns.ID.getName()));

        this.idIntervention = ProviderHelper.getInt(cursor,     SncfContent.t_gr_interventions.Columns.ID_INTERVENTION.getName());
        this.name           = ProviderHelper.getString(cursor,  SncfContent.t_gr_interventions.Columns.INTERVENTION.getName());
    }

    @Override
    public ContentValues toContentValues() {

        ContentValues returnValue = new ContentValues();

        returnValue.put(SncfContent.t_gr_interventions.Columns.ID_INTERVENTION.getName(),   this.idIntervention);
        returnValue.put(SncfContent.t_gr_interventions.Columns.INTERVENTION.getName(),      this.name);

        return returnValue;
    }

    @Override
    public String getPrimaryKeyName() {
        return SncfContent.t_gr_interventions.Columns.ID.getName();
    }
}
