package com.sncf.android.entity;

import android.content.ContentValues;
import android.database.Cursor;

import com.google.gson.annotations.SerializedName;
import com.sncf.android.data.JsonModel;
import com.sncf.android.orm.Entity;
import com.sncf.android.provider.SncfContent;
import com.sncf.android.utils.ProviderHelper;

/**
 * Created by favre on 23/01/2014.
 */
public class FunctionEntity extends Entity
{

    @SerializedName(JsonModel.t_gr_fonctions.ID)
    public Integer  functionId;

    @SerializedName(JsonModel.t_gr_fonctions.FONCTION)
    public Integer  id;

    @SerializedName(JsonModel.t_gr_fonctions.CODE_SERIE)
    public String   seriesCode;

    @SerializedName(JsonModel.t_gr_fonctions.LIBELLE_NIVEAU)
    public String   name;

    public FunctionEntity()                 {                           }
    public FunctionEntity(Cursor cursor)    { this.buildEntity(cursor); }

    @Override
    public void buildEntity(Cursor cursor) {

        this.primaryKey = cursor.getInt(cursor.getColumnIndex(SncfContent.t_gr_fonctions.Columns.ID.getName()));

        this.functionId = ProviderHelper.getInt(cursor,     SncfContent.t_gr_fonctions.Columns.ID_FONCTION.getName());
        this.id         = ProviderHelper.getInt(cursor,     SncfContent.t_gr_fonctions.Columns.FONCTION.getName());
        this.seriesCode = ProviderHelper.getString(cursor,  SncfContent.t_gr_fonctions.Columns.CODE_SERIE.getName());
        this.name       = ProviderHelper.getString(cursor,  SncfContent.t_gr_fonctions.Columns.LIBELLE_NIVEAU.getName());
    }

    @Override
    public ContentValues toContentValues() {

        ContentValues returnValue = new ContentValues();

        returnValue.put(SncfContent.t_gr_fonctions.Columns.ID_FONCTION.getName(),       this.functionId);
        returnValue.put(SncfContent.t_gr_fonctions.Columns.FONCTION.getName(),          this.id);
        returnValue.put(SncfContent.t_gr_fonctions.Columns.CODE_SERIE.getName(),        this.seriesCode);
        returnValue.put(SncfContent.t_gr_fonctions.Columns.LIBELLE_NIVEAU.getName(),    this.name);

        return returnValue;
    }

    @Override
    public String getPrimaryKeyName() {
        return SncfContent.t_gr_fonctions.Columns.ID.getName();
    }
}
