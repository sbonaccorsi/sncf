package com.sncf.android.entity;

import android.content.ContentValues;
import android.database.Cursor;

import com.google.gson.annotations.SerializedName;
import com.sncf.android.data.JsonModel;
import com.sncf.android.orm.Entity;
import com.sncf.android.provider.SncfContent;
import com.sncf.android.utils.ProviderHelper;

import java.util.ArrayList;

/**
 * Created by favre on 23/01/2014.
 */
public class SeriesEntity extends Entity
{
    @SerializedName(JsonModel.t_sncf_series.ID)
    public Integer  idSeries;

    @SerializedName(JsonModel.t_sncf_series.SERIE)
    public String   serie;

    @SerializedName(JsonModel.t_sncf_series.t_sncf_subseries.T_SNCF_SUBSERIES)
    public ArrayList<SubSeriesEntity>   sub_series;

    public SeriesEntity()               {                           }
    public SeriesEntity(Cursor cursor)  { this.buildEntity(cursor); }

    @Override
    public void buildEntity(Cursor cursor) {
        primaryKey = ProviderHelper.getInt(cursor, SncfContent.t_sncf_series.Columns.ID.getName());
        idSeries = ProviderHelper.getInt(cursor, SncfContent.t_sncf_series.Columns.ID_SERIE.getName());
        serie = ProviderHelper.getString(cursor, SncfContent.t_sncf_series.Columns.SERIE.getName());
    }

    @Override
    public ContentValues toContentValues() {
        ContentValues values = new ContentValues();
        values.put(SncfContent.t_sncf_series.Columns.ID_SERIE.getName(), idSeries);
        values.put(SncfContent.t_sncf_series.Columns.SERIE.getName(), serie);
        return values;
    }

    @Override
    public String getPrimaryKeyName() {
        return SncfContent.t_sncf_series.Columns.ID.getName();
    }
}
