package com.sncf.android.entity;

import android.content.ContentValues;
import android.database.Cursor;

import com.google.gson.annotations.SerializedName;
import com.sncf.android.data.JsonModel;
import com.sncf.android.orm.Entity;
import com.sncf.android.provider.SncfContent;
import com.sncf.android.utils.ProviderHelper;

import java.util.Collection;

/**
 * Created by favre on 23/01/2014.
 */
public class CompoEntity extends Entity
{
    @SerializedName(JsonModel.t_sncf_compos.ID)
    public Integer  idCompo;

    @SerializedName(JsonModel.t_sncf_compos.ID_RAME)
    public Integer  rameId;

    @SerializedName(JsonModel.t_sncf_compos.NUM_ORDER)
    public Integer  orderNum;

    @SerializedName(JsonModel.t_sncf_compos.NUM_RAME)
    public String   rameNum;

    @SerializedName(JsonModel.t_sncf_compos.NUM_VEHICULE)
    public String   name;

    @SerializedName(JsonModel.t_sncf_compos.CS_GRIFFE)
    public String   csGriffe;

    public Collection<SemelleBogieEntity> bogies;

    public CompoEntity()                {                           }
    public CompoEntity(Cursor cursor)   { this.buildEntity(cursor); }

    @Override
    public void buildEntity(Cursor cursor) {

        this.primaryKey = cursor.getInt(cursor.getColumnIndex(SncfContent.t_sncf_compos.Columns.ID.getName()));

        this.idCompo    = ProviderHelper.getInt(cursor,     SncfContent.t_sncf_compos.Columns.ID_COMPOS.getName());
        this.rameId     = ProviderHelper.getInt(cursor,     SncfContent.t_sncf_compos.Columns.ID_RAME.getName());
        this.orderNum   = ProviderHelper.getInt(cursor,     SncfContent.t_sncf_compos.Columns.NUM_ORDER.getName());
        this.rameNum    = ProviderHelper.getString(cursor,  SncfContent.t_sncf_compos.Columns.NUM_RAME.getName());
        this.name       = ProviderHelper.getString(cursor,  SncfContent.t_sncf_compos.Columns.NUM_VEHICULE.getName());
        this.csGriffe   = ProviderHelper.getString(cursor,  SncfContent.t_sncf_compos.Columns.CS_GRIFFE.getName());
    }

    @Override
    public ContentValues toContentValues() {

        ContentValues returnValues = new ContentValues();
        returnValues.put(SncfContent.t_sncf_compos.Columns.ID_COMPOS.getName(),     this.idCompo);
        returnValues.put(SncfContent.t_sncf_compos.Columns.ID_RAME.getName(),       this.rameId);
        returnValues.put(SncfContent.t_sncf_compos.Columns.NUM_ORDER.getName(),     this.orderNum);
        returnValues.put(SncfContent.t_sncf_compos.Columns.NUM_RAME.getName(),      this.rameNum);
        returnValues.put(SncfContent.t_sncf_compos.Columns.NUM_VEHICULE.getName(),  this.name);
        returnValues.put(SncfContent.t_sncf_compos.Columns.CS_GRIFFE.getName(),     this.csGriffe);

        return returnValues;
    }

    @Override
    public String getPrimaryKeyName() {
        return SncfContent.t_sncf_compos.Columns.ID.getName();
    }

    @Override
    public boolean equals(Object o) {

        CompoEntity compo = (CompoEntity)o;

        return this.idCompo.equals(compo.idCompo);
    }
}
