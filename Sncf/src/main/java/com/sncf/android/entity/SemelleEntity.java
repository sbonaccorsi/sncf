package com.sncf.android.entity;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.sncf.android.orm.Entity;
import com.sncf.android.orm.IEntity;
import com.sncf.android.provider.SncfContent;

/**
 * Created by zzzwist on 11/01/14.
 */
public class SemelleEntity extends Entity
{

    public  String           label;

    public SemelleEntity()              {                           }
    public SemelleEntity(Cursor cursor) { this.buildEntity(cursor); }

    @Override
    public void buildEntity(Cursor cursor)
    {
        primaryKey = cursor.getInt(cursor.getColumnIndex(SncfContent.SemelleTable.Columns.ID.getName()));

        if (!cursor.isNull(cursor.getColumnIndex(SncfContent.SemelleTable.Columns.LABEL.getName())))
            label = cursor.getString(cursor.getColumnIndex(SncfContent.SemelleTable.Columns.LABEL.getName()));
    }

    @Override
    public ContentValues toContentValues()
    {
        ContentValues returnValues = new ContentValues();

        returnValues.put(SncfContent.SemelleTable.Columns.LABEL.getName(), this.label);

        return returnValues;
    }

    @Override
    public String getPrimaryKeyName()   { return SncfContent.SemelleTable.Columns.ID.getName();   }
}
