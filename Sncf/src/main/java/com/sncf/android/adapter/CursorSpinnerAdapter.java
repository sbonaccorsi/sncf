package com.sncf.android.adapter;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by sbonaccorsi on 18/02/2014.
 */
public class CursorSpinnerAdapter extends SimpleCursorAdapter {

    public class Holder {
        public TextView mText;

        public Holder(View v) {
            mText = (TextView) v.findViewById(android.R.id.text1);
        }
    }

    public CursorSpinnerAdapter(Context context, int layout, Cursor c, String[] from, int[] to, int flags) {
        super(context, layout, c, from, to, flags);
    }

    @Override
    public int getCount() {
        if (getCursor().getCount() == 0)        return 0;
        else                                    return getCursor().getCount() + 1;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {

        Holder holder;

        if (position == 0) {
            View view               = super.getDropDownView(position, convertView, parent);
            if (view.getTag() == null) {
                holder              = new Holder(view);

                view.setTag(holder);
            }
            else holder             = (Holder) view.getTag();
            holder.mText.setText("Aucune selection");
            return view;
        }
        else return super.getDropDownView(position - 1, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder;
        if (position == 0) {
            View view                           = super.getView(position, convertView, parent);
            if (view.getTag() == null) {
                holder                          = new Holder(view);
                view.setTag(holder);
            }
            else holder                         = (Holder) view.getTag();
            holder.mText.setText("Aucune selection");
            return view;
        }
        else return super.getView(position - 1, convertView, parent);
    }
}
