package com.sncf.android.data;

/**
 * Created by zzzwist on 23/01/14.
 */
public interface JsonModel {

    public interface ParentModel {

        public interface t_sncf_stf {
            public static final String          T_SNCF_STF = "t_sncf_stf";
        }

        public interface t_sncf_uo {
            public static final String          T_SNCF_UO = "t_sncf_uo";
        }

        public interface t_sncf_sites {
            public static final String          T_SNCF_SITES = "t_sncf_sites";
        }

        public interface t_gr_specialities {
            public static final String          T_GR_SPECIALITIES = "t_gr_specialities";
        }

        public interface t_sncf_link_specialities {
            public static final String          T_SNCF_LINK_SPECIALITIES = "t_sncf_link_specialities";
        }

        public interface t_sncf_link_series {
            public static final String          T_SNCF_LINK_SERIES = "t_sncf_link_series";
        }

        public interface t_sncf_rames {
            public static final String          T_SNCF_RAMES = "t_sncf_rames";
        }

        public interface t_sncf_compos {
            public static final String          T_SNCF_COMPOS = "t_sncf_compos";
        }

        public interface t_gr_interventions {
            public static final String          T_GR_INTERVENTIONS = "t_gr_interventions";
        }

        public interface t_gr_fonctions {
            public static final String          T_GR_FONCTIONS = "t_gr_fonctions";
        }

        public interface t_gr_ensembles {
            public static final String          T_GR_ENSEMBLES = "t_gr_ensembles";
        }

        public interface t_gr_niveau_3 {
            public static final String          T_GR_NIVEAU_3 = "t_gr_niveau_3";
        }

        public interface t_gr_niveau_4 {
            public static final String          T_GR_NIVEAU_4 = "t_gr_niveau_4";
        }

        public interface t_gr_niveau_5 {
            public static final String          T_GR_NIVEAU_5 = "t_gr_niveau_5";
        }

        public interface t_gr_niveau_6 {
            public static final String          T_GR_NIVEAU_6 = "t_gr_niveau_6";
        }

        public interface t_gr_anomalies {
            public static final String          T_GR_ANOMALIES = "t_gr_anomalies";
        }

        public interface t_gr_reparations {
            public static final String          T_GR_REPARATIONS = "t_gr_reparations";
        }

        public interface t_gr_conclusions {
            public static final String          T_GR_CONCLUSION = "t_gr_conclusions";
        }

        public interface t_gr_micro_dj {
            public static final String          T_GR_MICRO_DJ = "t_gr_micro_dj";
        }

        public interface t_gr_localisations_mdj {
            public static final String          T_GR_LOCALISATIONS_MDJ = "t_gr_localisations_mdj";
        }

        public interface t_sncf_series {
            public static final String          T_SNCF_SERIES = "t_sncf_series";
        }

        public interface t_gr_cri {
            public static final String          T_GR_CRI = "t_gr_cri";
        }

        public interface t_sem_controles {
            public static final String          T_SEM_CONTROLES = "t_sem_controles";
        }

        public interface t_sem_bogie {
            public static final String          T_SEM_BOGIE = "t_sem_bogie";
        }

        public interface t_sem_data {
            public static final String          T_SEM_DATA = "t_sem_data";
        }

        public interface t_gr_restriction_mvc {
            public static final String          T_GR_RESTRICTION_MVC = "t_gr_restriction_mvc";
        }

        public interface t_gr_specificite {
            public static final String          T_GR_SPECIFICITE = "t_gr_specificite";
        }
    }

    public interface t_sncf_stf {
        public static final String              ID = "id";
        public static final String              STF = "stf";
    }

    public interface t_gr_specificite {
        public static final String              ID = "id";
        public static final String              SPECIFICITE = "specificite";
    }

    public interface t_sncf_uo {
        public static final String              ID = "id";
        public static final String              UO_COURT = "uo_court";
        public static final String              ID_STF = "id_stf";
        public static final String              UO_LONG = "uo_long";
    }

    public interface t_sncf_sites {
        public static final String              ID = "id";
        public static final String              ID_UO = "id_uo";
        public static final String              SITE = "site";
        public static final String              SPECIALISTE = "specialiste";
    }

    public interface t_gr_specialities {
        public static final String              ID = "id";
        public static final String              SPECIALITY = "speciality";
    }

    public interface t_sncf_link_specialities {
        public static final String              ID = "id";
        public static final String              ID_UO = "id_uo";
        public static final String              ID_SPECIALITY = "id_speciality";
        public static final String              ID_SITE = "id_site";
        public static final String              SPECIALITY_GRIFFE = "speciality_griffe";
    }

    public interface t_sncf_link_series {
        public static final String              ID = "id";
        public static final String              ID_SERIE = "id_serie";
        public static final String              ID_UNDER_SERIE = "id_under_serie";
        public static final String              ID_STF = "id_stf";
        public static final String              CS_GRIFFE = "cs_griffe";
    }

    public interface t_sncf_rames {
        public static final String              ID = "id";
        public static final String              ID_SERIE = "id_serie";
        public static final String              ID_UNDER_SERIE = "id_under_serie";
        public static final String              EAB = "eab";
        public static final String              CS_GRIFFE = "cs_griffe";
        public static final String              ID_STF = "id_stf";
        public static final String              ID_VARIANTE = "id_variante";
    }

    public interface t_sncf_compos {
        public static final String              ID = "id";
        public static final String              ID_RAME = "id_rame";
        public static final String              NUM_RAME = "num_rame";
        public static final String              NUM_VEHICULE = "num_vehicule";
        public static final String              CS_GRIFFE = "cs_griffe";
        public static final String              NUM_ORDER = "num_order";
    }

    public interface t_gr_interventions {
        public static final String              ID = "id";
        public static final String              INTERVENTION = "intervention";
    }

    public interface t_gr_fonctions {
        public static final String              ID = "id";
        public static final String              FONCTION = "fonction";
        public static final String              CODE_SERIE = "code_serie";
        public static final String              LIBELLE_NIVEAU = "libelle_niveau";
    }

    public interface t_gr_ensembles {
        public static final String              ID = "id";
        public static final String              FONCTION = "fonction";
        public static final String              ENSEMBLE = "ensemble";
        public static final String              CODE_SERIE = "code_serie";
        public static final String              LIBELLE_NIVEAU = "libelle_niveau";
    }

    public interface t_gr_niveau_3 {
        public static final String              ID = "id";
        public static final String              FONCTION = "fonction";
        public static final String              ENSEMBLE = "ensemble";
        public static final String              NIVEAU_3 = "niveau_3";
        public static final String              CODE_SERIE = "code_serie";
        public static final String              LIBELLE_NIVEAU = "libelle_niveau";
    }

    public interface t_gr_niveau_4 {
        public static final String              ID = "id";
        public static final String              FONCTION = "fonction";
        public static final String              ENSEMBLE = "ensemble";
        public static final String              NIVEAU_3 = "niveau_3";
        public static final String              NIVEAU_4 = "niveau_4";
        public static final String              CODE_SERIE = "code_serie";
        public static final String              LIBELLE_NIVEAU = "libelle_niveau";
    }

    public interface t_gr_niveau_5 {
        public static final String              ID = "id";
        public static final String              FONCTION = "fonction";
        public static final String              ENSEMBLE = "ensemble";
        public static final String              NIVEAU_3 = "niveau_3";
        public static final String              NIVEAU_4 = "niveau_4";
        public static final String              NIVEAU_5 = "niveau_5";
        public static final String              CODE_SERIE = "code_serie";
        public static final String              LIBELLE_NIVEAU = "libelle_niveau";
    }

    public interface t_gr_niveau_6 {
        public static final String              ID = "id";
        public static final String              FONCTION = "fonction";
        public static final String              ENSEMBLE = "ensemble";
        public static final String              NIVEAU_3 = "niveau_3";
        public static final String              NIVEAU_4 = "niveau_4";
        public static final String              NIVEAU_5 = "niveau_5";
        public static final String              NIVEAU_6 = "niveau_6";
        public static final String              CODE_SERIE = "code_serie";
        public static final String              LIBELLE_NIVEAU = "libelle_niveau";
    }

    public interface t_gr_anomalies {
        public static final String              ID = "id";
        public static final String              ANOM_CODE = "anom_code";
        public static final String              ANOM_NOM = "anom_nom";
        public static final String              VALIDE = "valide";
    }

    public interface t_gr_reparations {
        public static final String              ID = "id";
        public static final String              REPARATION_CODE = "reparation_code";
        public static final String              REPARATION_NOM = "reparation_nom";
        public static final String              VALIDE = "valide";
    }

    public interface t_gr_conclusions {
        public static final String              ID = "id";
        public static final String              CONCLUSION_CRI = "conclusion_cri";
        public static final String              VALIDE = "valide";
    }

    public interface t_gr_micro_dj {
        public static final String              ID = "id";
        public static final String              TYPE_MDJ = "type_mdj";
        public static final String              LIBELLE = "libelle";
        public static final String              ID_LOCALISATION = "id_localisation";
    }

    public interface t_gr_localisations_mdj {
        public static final String              ID = "id";
        public static final String              LOCALISATION_MDJ = "localisation_mdj";
    }

    public interface t_sncf_series {
        public static final String              ID = "id";
        public static final String              SERIE = "serie";

        public interface t_sncf_subseries
        {
            public static final String          T_SNCF_SUBSERIES = "t_sncf_subseries";
        }
    }

    public interface t_sncf_subserie
    {
        public static final String                ID = "id";
        public static final String                ID_SERIE = "id_serie";
        public static final String                SOUS_SERIE = "sous_serie";
        public static final String                CS_HL = "cs_hl";
        public static final String                CS_PRG = "cs_prg";
        public static final String                LCN = "lcn";
        public static final String                SOUS_SERIE_TANDEM = "sous_serie_tandem";
    }

    public interface t_gr_cri {
        public static final String              ID = "id";
        public static final String              NUM_CRI = "num_cri";
        public static final String              IND_CRI = "ind_cri";
        public static final String              DATE_INTERVENTION = "date_intervention";
        public static final String              INTERVENANT = "intervenant";
        public static final String              ID_RAME = "id_rame";
        public static final String              ID_RAME_UM = "id_rame_um";
        public static final String              ID_VEHICULE = "id_vehicule";
        public static final String              TYPE_OPERATION = "type_operation";
        public static final String              LIBELLE = "libelle";
        public static final String              DEPECHE = "depeche";
        public static final String              BS = "bs";
        public static final String              ID_CONCLUSION = "id_conclusion";
        public static final String              ID_FONCTION = "id_fonction";
        public static final String              ID_ENSEMBLE = "id_ensemble";
        public static final String              ID_NIVEAU_3 = "id_niveau_3";
        public static final String              ID_NIVEAU_4 = "id_niveau_4";
        public static final String              ID_NIVEAU_5 = "id_niveau_5";
        public static final String              ID_NIVEAU_6 = "id_niveau_6";
        public static final String              ID_ANOMALIE= "id_anomalie";
        public static final String              ID_REPARATION = "id_reparation";
        public static final String              COMMENTAIRE = "commentaire";
        public static final String              DATE_SIGNALEMENT = "date_signalement";
        public static final String              CS_GRIFFE = "cs_griffe";
        public static final String              SPECIALITE = "specialite";
        public static final String              NUM_CHANTIER = "num_chantier";
        public static final String              ID_RESTRICTION = "id_restriction";
        public static final String              SERVEUR_GRIFFE = "serveur_griffe";
        public static final String              MDJ1 = "mdj1";
        public static final String              MDJ2 = "mdj2";
        public static final String              MDJ3 = "mdj3";
        public static final String              MDJ4 = "mdj4";
        public static final String              MDJ5 = "mdj5";
        public static final String              MDJ6 = "mdj6";
        public static final String              MDJ7 = "mdj7";
        public static final String              MDJ8 = "mdj8";
        public static final String              MDJ9 = "mdj9";
        public static final String              MDJ10 = "mdj10";
        public static final String              MDJ11 = "mdj11";
        public static final String              MDJ12 = "mdj12";
    }

    public interface t_sem_controles {
        public static final String              ID = "id";
        public static final String              DATE_CTRL = "date_ctrl";
        public static final String              ID_RAME = "id_rame";
        public static final String              ID_INTERVENTION = "id_intervention";
        public static final String              ID_SITE = "id_site";
        public static final String              CONTROLER_NAME = "controler_name";
    }

    public interface t_sem_bogie {
        public static final String              ID = "id";
        public static final String              ID_CONTROL = "controle_id";
        public static final String              ID_COMPO = "compo_id";
        public static final String              BOGIE_IDENTIFIER = "bogie_id";
        public static final String              BOGIE_ISOLE = "bogie_isole";
        public static final String              BM_SOLE = "bm_isole";
    }

    public interface t_sem_data {
        public static final String              ID = "id";
        public static final String              ID_BOGIE = "bogie_id";
        public static final String              NUM_ROUE = "num_roue";
        public static final String              CONFORMITE = "conformite";
        public static final String              REMPLACEMENT = "remplacement";
        public static final String              CTRL_VISUEL = "ctrl_visuel";
        public static final String              MESURE = "mesure";
        public static final String              VALIDE_MESURE = "validite_mesure";
    }

    public interface t_gr_restriction_mvc {
        public static final String              ID = "id";
        public static final String              ID_REST = "id_rest";
        public static final String              NUM_REST = "num_rest";
        public static final String              SPECIFICITE = "specificite";
        public static final String              TYPE_REST = "type_rest";
        public static final String              LIB_REDUIT = "lib_reduit";
        public static final String              SITE_POSEUR = "site_poseur";
        public static final String              NUM_RAME = "num_rame";
        public static final String              DATE_POSE = "date_pose";
        public static final String              PRIORITE = "priorite";
        public static final String              COMMENTAIRE = "commentaire";
        public static final String              LOCALISATION = "localisation";
        public static final String              AMORTI = "amorti";
        public static final String              DATE_AMORTISSEMENT = "date_amortissement";
        public static final String              LOG_AMORTISSEMENT = "log_amortissement";
        public static final String              LOG_POSEUR = "log_poseur";
        public static final String              COMMENTAIRE_M = "commentaire_m";
        public static final String              DATE_JOURNALISATION = "date_journalisation";
        public static final String              SERIE_ID = "serie_id";
        public static final String              ID_RAME = "id_rame";
        public static final String              DATE_IMPORT = "date_import";
        public static final String              DATE_MAJ = "date_maj";
    }
}
