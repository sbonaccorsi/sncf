package com.sncf.android.data;

/**
 * Created by zzzwist on 15/01/14.
 */
public interface Constant {

    public interface STF {
        public static final String              STF_D_R = "STF D/R";
        public static final String              STF_H_K = "STF H/K";
        public static final String              STF_L_J = "STF L/J";
        public static final String              STF_E_P_T4 = "STF E/P/T4";
        public static final String              STF_C = "STF C";
        public static final String              STF_N_U = "STF N/U";
    }
}
