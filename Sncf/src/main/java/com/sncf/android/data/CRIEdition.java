package com.sncf.android.data;

import android.content.Context;
import android.util.SparseArray;

import com.sncf.android.entity.AnomalyEntity;
import com.sncf.android.entity.CRIEntity;
import com.sncf.android.entity.CompoEntity;
import com.sncf.android.entity.ConclusionEntity;
import com.sncf.android.entity.FunctionEntity;
import com.sncf.android.entity.LevelFiveEntity;
import com.sncf.android.entity.LevelFourEntity;
import com.sncf.android.entity.LevelSixEntity;
import com.sncf.android.entity.LevelThreeEntity;
import com.sncf.android.entity.RameEntity;
import com.sncf.android.entity.RepairEntity;
import com.sncf.android.entity.SetEntity;
import com.sncf.android.observer.Observable;
import com.sncf.android.orm.DAO;
import com.sncf.android.orm.Entity;
import com.sncf.android.orm.criteria.ICriteria;
import com.sncf.android.orm.criteria.SQLCriteria;
import com.sncf.android.orm.criteria.SQLPredicate;
import com.sncf.android.orm.criteria.operators.EqualSqlPredicate;
import com.sncf.android.orm.dao.AnomalyDAO;
import com.sncf.android.orm.dao.CompoDAO;
import com.sncf.android.orm.dao.ConclusionDAO;
import com.sncf.android.orm.dao.FunctionDAO;
import com.sncf.android.orm.dao.LevelFiveDAO;
import com.sncf.android.orm.dao.LevelFourDAO;
import com.sncf.android.orm.dao.LevelSixDAO;
import com.sncf.android.orm.dao.LevelThreeDAO;
import com.sncf.android.orm.dao.RameDAO;
import com.sncf.android.orm.dao.RepairDAO;
import com.sncf.android.orm.dao.SetDAO;
import com.sncf.android.provider.SncfContent;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by sbonaccorsi on 18/02/2014.
 */
public class CRIEdition extends Observable {

    private static CRIEdition                   mInstance;

    private Integer                              id_rame;
    private Integer                              id_um;
    private Integer                              id_caisse;
    private Integer                              id_conclusion;
    private Integer                              id_function;
    private Integer                              id_set;
    private Integer                              id_level3;
    private Integer                              id_level4;
    private Integer                              id_level5;
    private Integer                              id_level6;
    private Integer                              id_anomaly;
    private Integer                              id_repair;
    private Integer                              id_stf;

    private String                               name_intervenant;

    private String                               date_signalment;
    private String                               date_intervention;
    private Integer                              id_uo;
    private Integer                              id_site;
    private Integer                              id_speciality;
    private String                               name_speciality;
    private Integer                              id_series;
    private Integer                              id_subserie;
    private Integer                              id_intervention;
    private List<String>                         mListMDJ;
    private String                               signalment;
    private String                               intervention;

    private CRIEdition() {
        mListMDJ = new ArrayList<String>();
    }

    public static CRIEdition getInstance() {
        if (mInstance == null)  mInstance = new CRIEdition();
        return mInstance;
    }

    public void createListMDJ(SparseArray<String>... arrays) {
        List<String> list = new ArrayList<String>();
        for (int idx = 0; idx < arrays.length; ++idx) {
            SparseArray<String> array = arrays[idx];

            for (int index = 0; index < array.size(); index++) {
                int key = array.keyAt(index);
                String mdj = array.get(key);

                list.add(mdj);
            }
        }
        this.setmListMDJ(list);
    }

    //region SETTERS
    public void setId_rame(Integer id_rame) {
        this.id_rame = id_rame;
        this.notifyObserver();
    }

    public void setId_um(Integer id_um) {
        this.id_um = id_um;
        this.notifyObserver();
    }

    public void setId_caisse(Integer id_caisse) {
        this.id_caisse = id_caisse;
        this.notifyObserver();
    }

    public void setId_conclusion(Integer id_conclusion) {
        this.id_conclusion = id_conclusion;
        this.notifyObserver();
    }

    public void setId_function(Integer id_function) {
        this.id_function = id_function;
        this.notifyObserver();
    }

    public void setId_set(Integer id_set) {
        this.id_set = id_set;
        this.notifyObserver();
    }

    public void setId_level3(Integer id_level3) {
        this.id_level3 = id_level3;
        this.notifyObserver();
    }

    public void setId_level4(Integer id_level4) {
        this.id_level4 = id_level4;
        this.notifyObserver();
    }

    public void setId_level5(Integer id_level5) {
        this.id_level5 = id_level5;
        this.notifyObserver();
    }

    public void setId_level6(Integer id_level6) {
        this.id_level6 = id_level6;
        this.notifyObserver();
    }

    public void setId_anomaly(Integer id_anomaly) {
        this.id_anomaly = id_anomaly;
        this.notifyObserver();
    }

    public void setId_repair(Integer id_repair) {
        this.id_repair = id_repair;
        this.notifyObserver();
    }

    public void setId_subserie(Integer id_subserie) {
        this.id_subserie = id_subserie;
        this.notifyObserver();
    }

    public void setName_intervenant(String name_intervenant) {
        this.name_intervenant = name_intervenant;
        this.notifyObserver();
    }

    public void setId_stf(Integer id_stf) {
        this.id_stf = id_stf;
        this.notifyObserver();
    }

    public void setDate_signalment(String date_signalment) {
        this.date_signalment = date_signalment;
        this.notifyObserver();
    }

    public void setDate_intervention(String date_intervention) {
        this.date_intervention = date_intervention;
        this.notifyObserver();
    }

    public void setId_uo(Integer id_uo) {
        this.id_uo = id_uo;
        this.notifyObserver();
    }

    public void setId_site(Integer id_site) {
        this.id_site = id_site;
        this.notifyObserver();
    }

    public void setId_speciality(Integer id_speciality) {
        this.id_speciality = id_speciality;
        this.notifyObserver();
    }

    public void setName_speciality(String name_speciality) {
        this.name_speciality = name_speciality;
        this.notifyObserver();
    }

    public void setId_series(Integer id_series) {
        this.id_series = id_series;
        this.notifyObserver();
    }

    public void setId_intervention(Integer id_intervention) {
        this.id_intervention = id_intervention;
        this.notifyObserver();
    }

    public void setmListMDJ(List<String> mListMDJ) {
        this.mListMDJ = mListMDJ;
        this.notifyObserver();
    }

    public void setSignalment(String signalment) {
        this.signalment = signalment;
        this.notifyObserver();
    }

    public void setIntervention(String intervention) {
        this.intervention = intervention;
        this.notifyObserver();
    }
    //endregion

    //region GETTERS
    public Integer getId_rame() {
        return id_rame;
    }

    public Integer getId_um() {
        return id_um;
    }

    public Integer getId_caisse() {
        return id_caisse;
    }

    public Integer getId_conclusion() {
        return id_conclusion;
    }

    public Integer getId_function() {
        return id_function;
    }

    public Integer getId_set() {
        return id_set;
    }

    public Integer getId_level3() {
        return id_level3;
    }

    public Integer getId_level4() {
        return id_level4;
    }

    public Integer getId_level5() {
        return id_level5;
    }

    public Integer getId_level6() {
        return id_level6;
    }

    public Integer getId_anomaly() {
        return id_anomaly;
    }

    public Integer getId_repair() {
        return id_repair;
    }

    public String getName_intervenant() {
        return name_intervenant;
    }

    public Integer getId_stf() {
        return id_stf;
    }

    public String getDate_signalment() {
        return date_signalment;
    }

    public String getDate_intervention() {
        return date_intervention;
    }

    public Integer getId_uo() {
        return id_uo;
    }

    public Integer getId_site() {
        return id_site;
    }

    public Integer getId_speciality() {
        return id_speciality;
    }

    public String getName_speciality() {
        return name_speciality;
    }

    public Integer getId_series() {
        return id_series;
    }

    public Integer getId_subserie() { return id_subserie; }

    public Integer getId_intervention() {
        return id_intervention;
    }

    public List<String> getmListMDJ() {
        return mListMDJ;
    }

    public String getSignalment() {
        return signalment;
    }

    public String getIntervention() {
        return intervention;
    }
    //endregion

    //region BUILDS ENTITY CRI
    public void buildCRIEntity(Context context) {

        if (!checkFieldsValidities()) return;
        CRIEntity criEntity = new CRIEntity();

        //TODO create all fields
        this.createRameId(context, criEntity);
        this.createUMId(context, criEntity);
        this.createCompoId(context, criEntity);
        this.createConclusionId(context, criEntity);
        this.createFunctionId(context, criEntity);
        this.createSetId(context, criEntity);
        this.createLevel3Id(context, criEntity);
        this.createLevel4Id(context, criEntity);
        this.createLevel5Id(context, criEntity);
        this.createLevel6Id(context, criEntity);
        this.createAnomalyId(context, criEntity);
        this.createRepairId(context, criEntity);
    }

    private void createRameId(Context context, CRIEntity criEntity) {
        RameEntity rameEntity               = this.buildEntity(new RameDAO(context), new SQLPredicate(SncfContent.t_sncf_rames.Columns.ID.getName(), new EqualSqlPredicate(id_rame)));
        criEntity.rameId                    = rameEntity.idRame;
    }
    private void createUMId(Context context, CRIEntity criEntity) {
        RameEntity rameEntity               = this.buildEntity(new RameDAO(context), new SQLPredicate(SncfContent.t_sncf_rames.Columns.ID.getName(), new EqualSqlPredicate(id_um)));
        criEntity.rameUMId                  = rameEntity.idRame;
    }
    private void createCompoId(Context context, CRIEntity criEntity) {
        CompoEntity compoEntity             = this.buildEntity(new CompoDAO(context), new SQLPredicate(SncfContent.t_sncf_compos.Columns.ID.getName(), new EqualSqlPredicate(id_caisse)));
        criEntity.compoId                   = compoEntity.idCompo;
    }
    private void createConclusionId(Context context, CRIEntity criEntity) {
        ConclusionEntity conclusionEntity   = this.buildEntity(new ConclusionDAO(context), new SQLPredicate(SncfContent.t_gr_conclusions.Columns.ID.getName(), new EqualSqlPredicate(id_conclusion)));
        criEntity.conclusionId              = conclusionEntity.idConclusion;
    }
    private void createFunctionId(Context context, CRIEntity criEntity) {
        FunctionEntity functionEntity       = this.buildEntity(new FunctionDAO(context), new SQLPredicate(SncfContent.t_gr_fonctions.Columns.ID.getName(), new EqualSqlPredicate(id_function)));
        criEntity.functionId                = functionEntity.functionId;
    }
    private void createSetId(Context context, CRIEntity criEntity) {
        SetEntity setEntity                 = this.buildEntity(new SetDAO(context), new SQLPredicate(SncfContent.t_gr_ensembles.Columns.ID.getName(), new EqualSqlPredicate(id_set)));
        criEntity.setId                     = setEntity.idSet;
    }
    private void createLevel3Id(Context context, CRIEntity criEntity) {
        LevelThreeEntity levelThreeEntity   = this.buildEntity(new LevelThreeDAO(context), new SQLPredicate(SncfContent.t_gr_niveau_3.Columns.ID.getName(), new EqualSqlPredicate(id_level3)));
        criEntity.levelThreeId              = levelThreeEntity.idLevelThree;
    }
    private void createLevel4Id(Context context, CRIEntity criEntity) {
        LevelFourEntity levelFourEntity     = this.buildEntity(new LevelFourDAO(context), new SQLPredicate(SncfContent.t_gr_niveau_4.Columns.ID.getName(), new EqualSqlPredicate(id_level4)));
        criEntity.levelFourId               = levelFourEntity.idLevelFour;
    }
    private void createLevel5Id(Context context, CRIEntity criEntity) {
        LevelFiveEntity levelFiveEntity     = this.buildEntity(new LevelFiveDAO(context), new SQLPredicate(SncfContent.t_gr_niveau_5.Columns.ID.getName(), new EqualSqlPredicate(id_level5)));
        criEntity.levelFiveId               = levelFiveEntity.idLevelFive;
    }
    private void createLevel6Id(Context context, CRIEntity criEntity) {
        LevelSixEntity levelSixEntity       = this.buildEntity(new LevelSixDAO(context), new SQLPredicate(SncfContent.t_gr_niveau_6.Columns.ID.getName(), new EqualSqlPredicate(id_level6)));
        criEntity.levelSixId                = levelSixEntity.idLevelSix;
    }
    private void createAnomalyId(Context context, CRIEntity criEntity){
        AnomalyEntity anomalyEntity         = this.buildEntity(new AnomalyDAO(context), new SQLPredicate(SncfContent.t_gr_anomalies.Columns.ID.getName(), new EqualSqlPredicate(id_anomaly)));
        criEntity.anomalyId                 = anomalyEntity.code;
    }
    private void createRepairId(Context context, CRIEntity criEntity) {
        RepairEntity repairEntity           = this.buildEntity(new RepairDAO(context), new SQLPredicate(SncfContent.t_gr_reparations.Columns.ID.getName(), new EqualSqlPredicate(id_repair)));
        criEntity.repairId                  = repairEntity.code;
    }
    private <GenericDAO extends Entity> GenericDAO buildEntity(DAO<GenericDAO> dao, SQLPredicate predicate) {
        ICriteria criteria = new SQLCriteria();
        criteria.add(predicate);
        Collection<GenericDAO> entities = dao.read(criteria);
        if (entities == null || entities.size() == 0) return null;
        return (GenericDAO) entities.toArray()[0];
    }
    private boolean checkFieldsValidities() {
        return false;
    }
    //endregion
}
