package com.sncf.android.builder.tablebuilder;

import android.content.Context;
import android.net.Uri;

import com.sncf.android.builder.TableBuilder;
import com.sncf.android.entity.CRIEntity;
import com.sncf.android.provider.SncfContent;

import java.util.Collection;

/**
 * Created by favre on 01/02/2014.
 */
public class CRIBuilder extends TableBuilder<CRIEntity>
{
    public CRIBuilder(Context context, Collection<CRIEntity> entities) {
        super(context, SncfContent.t_gr_cri.CONTENT_URI, entities, CRIEntity.class);
    }
}
