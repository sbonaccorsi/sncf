package com.sncf.android.builder.tablebuilder;

import android.content.Context;
import android.net.Uri;

import com.sncf.android.builder.TableBuilder;
import com.sncf.android.entity.FunctionEntity;
import com.sncf.android.provider.SncfContent;

import java.util.Collection;

/**
 * Created by favre on 01/02/2014.
 */
public class FunctionsBuilder extends TableBuilder<FunctionEntity>
{
    public FunctionsBuilder(Context context, Collection<FunctionEntity> entities) {
        super(context, SncfContent.t_gr_fonctions.CONTENT_URI, entities, FunctionEntity.class);
    }
}
