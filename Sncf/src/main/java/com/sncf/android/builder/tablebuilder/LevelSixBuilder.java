package com.sncf.android.builder.tablebuilder;

import android.content.Context;

import com.sncf.android.builder.TableBuilder;
import com.sncf.android.entity.LevelFourEntity;
import com.sncf.android.entity.LevelSixEntity;
import com.sncf.android.provider.SncfContent;

import java.util.Collection;

/**
 * Created by favre on 01/02/2014.
 */
public class LevelSixBuilder extends TableBuilder<LevelSixEntity>
{
    public LevelSixBuilder(Context context, Collection<LevelSixEntity> entities) {
        super(context, SncfContent.t_gr_niveau_6.CONTENT_URI, entities, LevelSixEntity.class);
    }

}
