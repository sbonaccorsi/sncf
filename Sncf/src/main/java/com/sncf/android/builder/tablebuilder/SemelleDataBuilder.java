package com.sncf.android.builder.tablebuilder;

import android.content.Context;
import android.net.Uri;

import com.sncf.android.builder.TableBuilder;
import com.sncf.android.entity.SemelleDataEntity;
import com.sncf.android.provider.SncfContent;

import java.util.Collection;

/**
 * Created by favre on 01/02/2014.
 */
public class SemelleDataBuilder extends TableBuilder<SemelleDataEntity>
{
    public SemelleDataBuilder(Context context, Collection<SemelleDataEntity> entities) {
        super(context, SncfContent.t_sem_data.CONTENT_URI, entities, SemelleDataEntity.class);
    }
}
