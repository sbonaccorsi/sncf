package com.sncf.android.builder.tablebuilder;

import android.content.Context;
import android.net.Uri;

import com.sncf.android.builder.TableBuilder;
import com.sncf.android.entity.MicroDjEntity;
import com.sncf.android.provider.SncfContent;

import java.util.Collection;

/**
 * Created by favre on 01/02/2014.
 */
public class MicroDjBuilder extends TableBuilder<MicroDjEntity>
{
    public MicroDjBuilder(Context context, Collection<MicroDjEntity> entities) {
        super(context, SncfContent.t_gr_micro_dj.CONTENT_URI, entities, MicroDjEntity.class);
    }
}
