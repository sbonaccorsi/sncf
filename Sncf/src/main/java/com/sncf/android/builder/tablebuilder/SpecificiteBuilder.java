package com.sncf.android.builder.tablebuilder;

import android.content.Context;

import com.sncf.android.builder.TableBuilder;
import com.sncf.android.entity.STFEntity;
import com.sncf.android.entity.SpecificiteEntity;
import com.sncf.android.provider.SncfContent;

import java.util.Collection;

/**
 * Created by favre on 01/02/2014.
 */
public class SpecificiteBuilder extends TableBuilder<SpecificiteEntity>
{
    public SpecificiteBuilder(Context context, Collection<SpecificiteEntity> entities) {
        super(context, SncfContent.t_gr_specificite.CONTENT_URI, entities, SpecificiteEntity.class);
    }
}
