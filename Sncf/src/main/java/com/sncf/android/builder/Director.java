package com.sncf.android.builder;

import android.util.Log;

import com.sncf.android.utils.Parallel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.TimeUnit;

/**
 * Created by favre on 01/02/2014.
 */
public class Director implements IDirector
{
    private Collection<IBuilder> builders;

    public Director()                           { this.builders = new ArrayList<IBuilder>();    }
    public void addBuilder(IBuilder builder)    { this.builders.add(builder);                   }

    @Override
    public void build() {

        for(IBuilder builder : this.builders)
        {
            Log.i("Director", "Build...");
            builder.build();
        }
        /*Parallel.For(builders,
                // The operation to perform with each item
                new Parallel.Operation<IBuilder>() {
                    public void perform(IBuilder param) {
                        param.build();
                    };
                });*/

        Log.i("Director", "Build ended");
    }
}
