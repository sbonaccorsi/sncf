package com.sncf.android.builder.tablebuilder;

import android.content.Context;
import android.net.Uri;

import com.sncf.android.builder.TableBuilder;
import com.sncf.android.entity.LinkSpecialtyEntity;
import com.sncf.android.provider.SncfContent;

import java.util.Collection;

/**
 * Created by favre on 01/02/2014.
 */
public class LinkSpecialtiesBuilder extends TableBuilder<LinkSpecialtyEntity>
{
    public LinkSpecialtiesBuilder(Context context, Collection<LinkSpecialtyEntity> entities) {
        super(context, SncfContent.t_sncf_link_specialities.CONTENT_URI, entities, LinkSpecialtyEntity.class);
    }
}
