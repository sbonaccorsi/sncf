package com.sncf.android.builder.tablebuilder;

import android.content.Context;
import android.net.Uri;

import com.sncf.android.builder.TableBuilder;
import com.sncf.android.entity.SemelleBogieEntity;
import com.sncf.android.provider.SncfContent;

import java.util.Collection;

/**
 * Created by favre on 01/02/2014.
 */
public class SemelleBogieBuilder extends TableBuilder<SemelleBogieEntity>
{
    public SemelleBogieBuilder(Context context, Collection<SemelleBogieEntity> entities) {
        super(context, SncfContent.t_sem_bogie.CONTENT_URI, entities, SemelleBogieEntity.class);
    }
}
