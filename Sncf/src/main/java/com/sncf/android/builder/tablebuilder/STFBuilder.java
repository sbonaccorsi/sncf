package com.sncf.android.builder.tablebuilder;

import android.content.Context;
import android.net.Uri;

import com.sncf.android.builder.TableBuilder;
import com.sncf.android.entity.STFEntity;
import com.sncf.android.provider.SncfContent;

import java.util.Collection;

/**
 * Created by favre on 01/02/2014.
 */
public class STFBuilder extends TableBuilder<STFEntity>
{
    public STFBuilder(Context context, Collection<STFEntity> entities) {
        super(context, SncfContent.t_sncf_stf.CONTENT_URI, entities, STFEntity.class);
    }
}
