package com.sncf.android.builder.tablebuilder;

import android.content.Context;
import android.net.Uri;

import com.sncf.android.builder.TableBuilder;
import com.sncf.android.entity.RepairEntity;
import com.sncf.android.provider.SncfContent;

import java.util.Collection;

/**
 * Created by favre on 01/02/2014.
 */
public class RepairBuilder extends TableBuilder<RepairEntity>
{
    public RepairBuilder(Context context, Collection<RepairEntity> entities) {
        super(context, SncfContent.t_gr_reparations.CONTENT_URI, entities, RepairEntity.class);
    }
}
