package com.sncf.android.builder;

/**
 * Created by favre on 01/02/2014.
 */
public interface IDirector
{
    public void build();
}
