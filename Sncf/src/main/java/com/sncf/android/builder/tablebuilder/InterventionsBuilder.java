package com.sncf.android.builder.tablebuilder;

import android.content.Context;
import android.net.Uri;

import com.sncf.android.builder.TableBuilder;
import com.sncf.android.entity.InterventionEntity;
import com.sncf.android.provider.SncfContent;

import java.util.Collection;

/**
 * Created by favre on 01/02/2014.
 */
public class InterventionsBuilder extends TableBuilder<InterventionEntity>
{
    public InterventionsBuilder(Context context, Collection<InterventionEntity> entities) {
        super(context, SncfContent.t_gr_interventions.CONTENT_URI, entities, InterventionEntity.class);
    }
}
