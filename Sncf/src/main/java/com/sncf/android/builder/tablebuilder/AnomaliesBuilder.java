package com.sncf.android.builder.tablebuilder;

import android.content.Context;

import com.sncf.android.builder.TableBuilder;
import com.sncf.android.entity.AnomalyEntity;
import com.sncf.android.provider.SncfContent;

import java.util.Collection;

/**
 * Created by favre on 01/02/2014.
 */
public class AnomaliesBuilder extends TableBuilder<AnomalyEntity>
{
    public AnomaliesBuilder(Context context, Collection<AnomalyEntity> entities) {
        super(context, SncfContent.t_gr_anomalies.CONTENT_URI, entities, AnomalyEntity.class);
    }
}
