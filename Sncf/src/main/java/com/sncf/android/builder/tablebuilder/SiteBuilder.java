package com.sncf.android.builder.tablebuilder;

import android.content.Context;
import android.net.Uri;

import com.sncf.android.builder.TableBuilder;
import com.sncf.android.entity.SiteEntity;
import com.sncf.android.provider.SncfContent;

import java.util.Collection;

/**
 * Created by favre on 01/02/2014.
 */
public class SiteBuilder extends TableBuilder<SiteEntity>
{
    public SiteBuilder(Context context, Collection<SiteEntity> entities) {
        super(context, SncfContent.t_sncf_sites.CONTENT_URI, entities, SiteEntity.class);
    }
}
