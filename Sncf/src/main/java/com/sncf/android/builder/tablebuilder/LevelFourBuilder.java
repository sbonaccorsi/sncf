package com.sncf.android.builder.tablebuilder;

import android.content.Context;

import com.sncf.android.builder.TableBuilder;
import com.sncf.android.entity.LevelFiveEntity;
import com.sncf.android.entity.LevelFourEntity;
import com.sncf.android.provider.SncfContent;

import java.util.Collection;

/**
 * Created by favre on 01/02/2014.
 */
public class LevelFourBuilder extends TableBuilder<LevelFourEntity>
{
    public LevelFourBuilder(Context context, Collection<LevelFourEntity> entities) {
        super(context, SncfContent.t_gr_niveau_4.CONTENT_URI, entities, LevelFourEntity.class);
    }
}
