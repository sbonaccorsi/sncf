package com.sncf.android.builder.tablebuilder;

import android.content.Context;

import com.sncf.android.builder.TableBuilder;
import com.sncf.android.entity.SeriesEntity;
import com.sncf.android.entity.SubSeriesEntity;
import com.sncf.android.provider.SncfContent;

import java.util.Collection;

/**
 * Created by favre on 01/02/2014.
 */
public class SubSeriesBuilder extends TableBuilder<SubSeriesEntity>
{
    public SubSeriesBuilder(Context context, Collection<SubSeriesEntity> entities) {
        super(context, SncfContent.t_sncf_subseries.CONTENT_URI, entities, SubSeriesEntity.class);
    }
}
