package com.sncf.android.builder;

import android.content.Context;
import android.net.Uri;

import com.sncf.android.entity.AnomalyEntity;
import com.sncf.android.orm.Entity;
import com.sncf.android.orm.ORM;
import com.sncf.android.provider.SncfContent;
import com.sncf.android.utils.Parallel;
import com.sncf.android.utils.ValueHelper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by favre on 01/02/2014.
 */
public class TableBuilder<T extends Entity> implements IBuilder
{
    protected Uri           uri;
    protected Class<T>      clazz;
    protected Context       context;
    protected Collection<T> entities;

    public TableBuilder(Context context, Uri uri, Collection<T> entities, Class<T> clazz)
    {
        this.uri        = uri;
        this.clazz      = clazz;
        this.context    = context;
        this.entities   = entities;
    }

    @Override
    public void build()
    {
        //ArrayList list = new ArrayList(entities);
        //Collection<List> lists = ValueHelper.split(list, 20);//new ArrayList<List>();
         ORM.createAll(context, uri, entities, clazz);
    }
}
