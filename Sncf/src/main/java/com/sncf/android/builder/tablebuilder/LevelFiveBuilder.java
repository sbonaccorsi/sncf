package com.sncf.android.builder.tablebuilder;

import android.content.Context;
import android.net.Uri;

import com.sncf.android.builder.TableBuilder;
import com.sncf.android.entity.LevelFiveEntity;
import com.sncf.android.provider.SncfContent;

import java.util.Collection;

/**
 * Created by favre on 01/02/2014.
 */
public class LevelFiveBuilder extends TableBuilder<LevelFiveEntity>
{
    public LevelFiveBuilder(Context context, Collection<LevelFiveEntity> entities) {
        super(context, SncfContent.t_gr_niveau_5.CONTENT_URI, entities, LevelFiveEntity.class);
    }
}
