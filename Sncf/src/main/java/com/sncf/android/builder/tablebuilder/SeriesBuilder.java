package com.sncf.android.builder.tablebuilder;

import android.content.Context;
import android.net.Uri;

import com.sncf.android.builder.TableBuilder;
import com.sncf.android.entity.SeriesEntity;
import com.sncf.android.provider.SncfContent;

import java.util.Collection;

/**
 * Created by favre on 01/02/2014.
 */
public class SeriesBuilder extends TableBuilder<SeriesEntity>
{
    public SeriesBuilder(Context context, Collection<SeriesEntity> entities) {
        super(context, SncfContent.t_sncf_series.CONTENT_URI, entities, SeriesEntity.class);
    }
}
