package com.sncf.android.builder.tablebuilder;

import android.content.Context;
import android.net.Uri;

import com.sncf.android.builder.TableBuilder;
import com.sncf.android.entity.RameEntity;
import com.sncf.android.provider.SncfContent;

import java.util.Collection;

/**
 * Created by favre on 01/02/2014.
 */
public class RamesBuilder extends TableBuilder<RameEntity>
{
    public RamesBuilder(Context context, Collection<RameEntity> entities) {
        super(context, SncfContent.t_sncf_rames.CONTENT_URI, entities, RameEntity.class);
    }
}
