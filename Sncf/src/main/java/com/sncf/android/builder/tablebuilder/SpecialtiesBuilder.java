package com.sncf.android.builder.tablebuilder;

import android.content.Context;
import android.net.Uri;

import com.sncf.android.builder.TableBuilder;
import com.sncf.android.entity.SpecialtyEntity;
import com.sncf.android.provider.SncfContent;

import java.util.Collection;

/**
 * Created by favre on 01/02/2014.
 */
public class SpecialtiesBuilder extends TableBuilder<SpecialtyEntity>
{
    public SpecialtiesBuilder(Context context, Collection<SpecialtyEntity> entities) {
        super(context, SncfContent.t_gr_specialities.CONTENT_URI, entities, SpecialtyEntity.class);
    }
}
