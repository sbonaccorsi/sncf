package com.sncf.android.builder.tablebuilder;

import android.content.Context;

import com.sncf.android.builder.TableBuilder;
import com.sncf.android.entity.CompoEntity;
import com.sncf.android.provider.SncfContent;

import java.util.Collection;

/**
 * Created by favre on 01/02/2014.
 */
public class ComposBuilder extends TableBuilder<CompoEntity>
{
    public ComposBuilder(Context context, Collection<CompoEntity> entities) {
        super(context, SncfContent.t_sncf_compos.CONTENT_URI, entities, CompoEntity.class);
    }
}
