package com.sncf.android.builder.tablebuilder;

import android.content.Context;

import com.sncf.android.builder.TableBuilder;
import com.sncf.android.entity.LevelSixEntity;
import com.sncf.android.entity.LevelThreeEntity;
import com.sncf.android.provider.SncfContent;

import java.util.Collection;

/**
 * Created by favre on 01/02/2014.
 */
public class LevelThreeBuilder extends TableBuilder<LevelThreeEntity>
{
    public LevelThreeBuilder(Context context, Collection<LevelThreeEntity> entities) {
        super(context, SncfContent.t_gr_niveau_3.CONTENT_URI, entities, LevelThreeEntity.class);
    }

}
