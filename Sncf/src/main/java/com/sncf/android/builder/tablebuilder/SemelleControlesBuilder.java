package com.sncf.android.builder.tablebuilder;

import android.content.Context;
import android.net.Uri;

import com.sncf.android.builder.TableBuilder;
import com.sncf.android.entity.SemelleControlesEntity;
import com.sncf.android.provider.SncfContent;

import java.util.Collection;

/**
 * Created by favre on 01/02/2014.
 */
public class SemelleControlesBuilder extends TableBuilder<SemelleControlesEntity>
{
    public SemelleControlesBuilder(Context context, Collection<SemelleControlesEntity> entities) {
        super(context, SncfContent.t_sem_controles.CONTENT_URI, entities, SemelleControlesEntity.class);
    }
}
